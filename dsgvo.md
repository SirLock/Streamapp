- [DSGVO für Website-Betreiber -heise](#dsgvo-f%c3%bcr-website-betreiber--heise)
  - [Social-Media-Inhalte datenschutzfreundlich in die Website einbinden](#social-media-inhalte-datenschutzfreundlich-in-die-website-einbinden)

# Artikelsammlung  <!-- omit in toc -->
## [DSGVO für Website-Betreiber -heise](https://www.heise.de/select/ct-wissen/2019/1/softlinks/wnur?wt_mc=pred.red.ct.sh_wissen012019.084.softlink.softlink)
### [Social-Media-Inhalte datenschutzfreundlich in die Website einbinden](https://www.heise.de/select/ct/2018/12/1528066716941653)


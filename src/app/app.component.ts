import {Component} from '@angular/core';
import {AppManagerService} from './services/appmanager.service';
import {ResponsiveService} from './responsive.service';
import {Theme} from './models/theme.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private responsiveService: ResponsiveService, private appManager: AppManagerService){
  }

  get currentTheme(): Theme {
    return this.appManager.currentTheme;
  }

  onResize(event: Event){
    this.responsiveService.checkWidth();
  }

  resolveTheme(): string {
      return this.currentTheme.mode + " " + this.currentTheme.color;
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService } from './language.service';

@Pipe({
  name: 'tl8',
  pure: false
})
export class LanguagePipe implements PipeTransform {

  transform(key: string): string {

    return this.languageService.keyToValue(key);
  }

  constructor(private languageService : LanguageService)  {
  }

}

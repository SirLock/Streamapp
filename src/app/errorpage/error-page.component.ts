import { Component, OnInit } from '@angular/core';
import {CustomAnimations} from "../customanimations";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-errorpage',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss'],
  animations: [
    CustomAnimations.bluescreenSlideIn
  ]
})
export class ErrorPageComponent implements OnInit {
  newRoomLink: string;
  errorLink_: string;

  get errorLink():string {
    return this.errorLink_;
  }

  set errorLink(val: string) {
    this.errorLink_ = 'URL:' + document.URL;
  }

  constructor(private router: Router, private route: ActivatedRoute) {
    this.errorLink = document.URL;
    this.setInitalCreateNewRoomLink();
  }

  ngOnInit() {
  }

  createNewRoom() {
    window.location.href= this.newRoomLink;
  }

  setInitalCreateNewRoomLink() {
    const end: number = document.URL.lastIndexOf("/");
    this.newRoomLink = document.URL.substring(0, end);
  }
}

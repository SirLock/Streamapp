import {Injectable} from '@angular/core';
import {Constants} from "./helper/constants";

@Injectable({
  providedIn: 'root'
})
export class OverlayManagerService {

  private _overlayIndex: number = -1; // TODO scope?
  private _previousOverlayIndex: number = -1;

  get previousOverlayIndex(): number {
    return this._previousOverlayIndex;
  }

  set previousOverlayIndex(value: number) {
    this._previousOverlayIndex = value;
  }

  set overlayIndex(value: number) {
    this._overlayIndex = value;
  }

  get overlayIndex(): number {
    return this._overlayIndex;
  }

  constructor() {
  }

  isDsgvoDisplayed(): boolean {
    return Constants.OVERLAY_DSGVO_INDEX === this.overlayIndex;
  }

  isCookieDetailsDisplayed(): boolean {
    return Constants.OVERLAY_COOKIE_DETAILS_INDEX === this.overlayIndex;
  }

  isImpressumDisplayed(): boolean {
    return Constants.OVERLAY_IMPRESSUM_INDEX === this.overlayIndex;
  }

  isTermsOfUseDisplayed(): boolean {
    return Constants.OVERLAY_TERMS_OF_USE_INDEX === this.overlayIndex;
  }

  isPhilosophyDisplayed(): boolean {
    return Constants.OVERLAY_PHILOSOPHY_INDEX === this.overlayIndex;
  }

  isCookiePolicyDisplayed(): boolean {
    return Constants.OVERLAY_COOKIE_POLICY_INDEX === this.overlayIndex;
  }

  get hasPreviousOverlay(): boolean {
    return this.previousOverlayIndex > -1;
  }

  resetPreviousOverLayIndex() {
    this.previousOverlayIndex = -1;
  }

  closeCurrentOverlay() {
    if(this.hasPreviousOverlay) {
      this.overlayIndex = this.previousOverlayIndex;
      this.resetPreviousOverLayIndex();
    }else{
      this.overlayIndex = -1;
    }
  }

  openDsgvo() {
    this.openOverlayComponent(Constants.OVERLAY_DSGVO_INDEX);
  }

  openCookieDetails() {
    this.openOverlayComponent(Constants.OVERLAY_COOKIE_DETAILS_INDEX);
  }

  openImpressum() {
    this.openOverlayComponent(Constants.OVERLAY_IMPRESSUM_INDEX);
  }

  openTermsOfUse() {
    this.openOverlayComponent(Constants.OVERLAY_TERMS_OF_USE_INDEX);
  }

  openPhilosophy() {
    this.openOverlayComponent(Constants.OVERLAY_PHILOSOPHY_INDEX);
  }

  openCookiePolicy() {
    this.openOverlayComponent(Constants.OVERLAY_COOKIE_POLICY_INDEX);
  }

  openOverlayComponent(index: number) {
    this.updatePreviousOverlayIndex(this.overlayIndex);
    this.overlayIndex = index;
  }

  updatePreviousOverlayIndex(oldIndex: number){
    if(this.previousOverlayIndex === -1) {
      this.previousOverlayIndex = oldIndex;
    }
  }
}

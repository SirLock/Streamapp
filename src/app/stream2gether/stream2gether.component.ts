import {Component, OnInit, HostListener} from '@angular/core';
import {AppManagerService} from '../services/appmanager.service';
import {CookieService} from "ngx-cookie-service";
import {ActivatedRoute, Router} from '@angular/router';
import {Logger} from '../helper/logger';
import {Observable} from 'rxjs';
import {StoreService} from '../services/store.service';
import {WebsocketService} from '../services/websocket.service';
import {DataTransferContainer} from '../models/datatransfercontainer.model';
import {RestService} from '../services/rest.service';
import {Constants} from '../helper/constants';
import {User} from '../models/user.model';
import {Video} from '../models/video.model';
import {ResponsiveService} from '../responsive.service';
import {LanguageService} from '../language.service';
import {NextPageTokenObject} from '../models/nextpagetokenobject.model';
import {SearchResult} from '../models/searchresult.model';
import {ChatMessage} from '../models/chatmessage.model';
import {Cookie} from '../models/s2g.cookie';
import {CookieManagerService} from '../services/cookiemanager.service';
//import { gsap, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin } from "gsap/all";
import {gsap} from "gsap/dist/gsap";
import {DrawSVGPlugin} from "gsap/dist/DrawSVGPlugin";
import {MorphSVGPlugin} from "gsap/dist/MorphSVGPlugin";
import {CustomAnimations} from "../customanimations";
import {OverlayManagerService} from "../overlay-manager.service";
import * as UUID from 'uuid';
import { WebStorageService } from '../web-storage.service';

gsap.registerPlugin(DrawSVGPlugin, MorphSVGPlugin);


@Component({
  selector: 'app-stream2gether',
  templateUrl: './stream2gether.component.html',
  styleUrls: ['./stream2gether.component.scss']
})
export class Stream2getherComponent implements OnInit {


  configLoaded: boolean = false;

  private videoContainer_: HTMLElement;
  private screenMode_: string;

  set overlayIndex(value: number) {
    this.overlayManager.overlayIndex = value;
  }

  get overlayIndex(): number {
    return this.overlayManager.overlayIndex;
  }

  set displayScrollBackToVideo(val: boolean) {
    this.appManager.displayScrollBackToVideo = val;
  }

  get revealContent(): boolean {
    return this.appManager.revealContent;
  }

  set revealContent(val: boolean) {
    this.appManager.revealContent = val;
  }

  get videoContainer(): HTMLElement {
    return this.videoContainer_;
  }

  set videoContainer(val: HTMLElement) {
    this.videoContainer_ = val;
  }

  get screenMode(): string {
    return this.responsiveService.resolvedScreenMode;
  }

  get topbarDocked(): boolean {
    return this.appManager.topbarDocked;
  }

  set topbarDocked(val: boolean) {
    this.appManager.topbarDocked = val;
  }

  get chatToggle(): boolean {
    return this.appManager.chatToggle;
  }

  set chatToggle(val: boolean) {
    this.appManager.chatToggle = val;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  set roomId(val: string) {
    this.store.roomId = val;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  set currentlyPopularVideos(val: Video[]) {
    this.store.currentlyPopularVideos = val;
  }

  get searchResults(): SearchResult[] {
    return this.appManager.dataApiManager.searchResults || [];
  }

  set searchResults(val: SearchResult[]) {
    this.appManager.dataApiManager.searchResults = val;
  }

  get nextPageTokenObj(): NextPageTokenObject {
    return this.appManager.dataApiManager.nextPageTokenObj;
  }

  get pipActive(): boolean {
    return this.appManager.pipActive;
  }

  set pipActive(val: boolean) {
    this.appManager.pipActive = val;
  }

  get currentSearchResultsIndex(): number {
    return this.appManager.currentSearchResultsIndex;
  }

  set currentSearchResultsIndex(value: number) {
    this.appManager.currentSearchResultsIndex = value;
  }


  get MobileNavStatus(): string {
    return this.appManager.MobileNavStatus;
  }

  set MobileNavStatus(val: string) {
    this.appManager.MobileNavStatus = val;
  }


  constructor(private languageService: LanguageService,
              private cookieService: CookieService,
              private appManager: AppManagerService,
              private restService: RestService,
              private activatedRoute: ActivatedRoute,
              private store: StoreService,
              private websocket: WebsocketService,
              private responsiveService: ResponsiveService,
              private cookieManager: CookieManagerService,
              private router: Router,
              private overlayManager: OverlayManagerService,
              private webStorageManager: WebStorageService
  ) {

    //this.determineFullScreenModus();
    this.listenToSizeChange();

    this.appManager.registerS2GComponent(this);
    if (this.dsgvoCookieConfirmed) {
      this.startApp();
    }
  }

  get dsgvoCookieConfirmed(): boolean {
    return this.cookieManager.dsgvoCookieConfirmed;
  }

  startApp() {
    let that = this;
    const waitForConfig = setInterval(() => {
      if (Constants.configLoaded) {
        that.handleAppInit();
        clearInterval(waitForConfig);
      }
    }, 1);
  }

  handleAppInit() {

    let userName = null;
    let userId = null;
    let iconUrl = null;
    let schemaString = null;
    if (this.cookieService.check(Constants.USER_COOKIE_KEY)) {
      let cookieObj: Cookie = JSON.parse(this.cookieService.get(Constants.USER_COOKIE_KEY));
      let user: User = cookieObj.user;
      userName = user.name;
      userId = user.id;
      iconUrl = user.iconUrl;
      schemaString = user.schemaString;
      this.languageService.selectedLanguageKey = cookieObj.langKey;
      this.appManager.currentTheme = cookieObj.theme;
    }

    let roomId = this.getPathId();
    if (this.isValidId(roomId)) {
      // JOIN ROOM
      //Load Values From Room
      this.loadFromLocalStorage(roomId);
      this.sendJoinRoomRequest(roomId, userName, userId, iconUrl, schemaString)
        .subscribe(this.handleEnterRoomResponse, this.OnJoinRoomRequestError);
    } else {
      // CREATE ROOM
      this.sendCreateRoomRequest(userName, userId, iconUrl, schemaString)
        .subscribe(this.handleEnterRoomResponse, this.OnRequestError);
    }
  }

  loadFromLocalStorage(roomId: string) {
    this.webStorageManager.restoreRoomContainer(roomId);
  }

  listenToOrientationChange() {
    screen.orientation.addEventListener("change", (event) => {
      this.determineFullScreenModus();
    });
  }

  determineFullScreenModus() {
    if (this.appManager.videoService && screen.orientation.type === 'landscape-primary' && this.screenMode === this.responsiveService._mobile) {
      this.appManager.videoService.selectedPlayer.toFullScreen();
    } else {
      if (document.fullscreen) {
        document.exitFullscreen().then(v => console.log(v), err => console.log(err));
      }
    }

  }

  listenToSizeChange() {
    this.responsiveService.getMobileStatus().subscribe(screenMode => {
      this.resetChatContainerForDesktopMode(screenMode);
      this.responsiveService.resolvedScreenMode = screenMode;
    });
    this.responsiveService.checkWidth();
  }

  resetChatContainerForDesktopMode(screenMode: string) {
    if (screenMode === this.responsiveService._widescreen && screenMode !== this.screenMode) {
      if (this.chatToggle) {
        this.chatToggle = false;
        this.appManager.userListOpened = false;
        this.appManager.closed = true;
      }
    }
  }

  ngOnInit() {
    this.listenToOrientationChange();
    window.addEventListener('scroll', this.onScroll, true);
  }


  private isValidId(roomId: string): boolean {
    return roomId && roomId.match(/^\w{8}\-(\w{4}\-){3}\w{12}$/i) !== null;
  }

  private sendJoinRoomRequest(roomId: string, userName?: string, userId?: string, iconUrl?: string, schemaString?: string): Observable<any> {
    if (userId && userName && iconUrl && schemaString) {
      let dtc = new DataTransferContainer();
      dtc.user = new User();
      dtc.user.name = userName ? userName : 'new-user';
      dtc.user.id = userId ? userId : null;
      dtc.user.iconUrl = iconUrl;
      dtc.user.schemaString = schemaString;
      return this.restService.POST(Constants.REST_ENDPOINT_JOIN_ROOM + roomId, dtc);
    } else {
      return this.restService.GET(Constants.REST_ENDPOINT_JOIN_ROOM + roomId);
    }
  }

  public sendCreateRoomRequest(userName?: string, userId?: string, iconUrl?: string, schemaString?: string): Observable<any> {
    if (userId && userName && iconUrl && schemaString) {
      let dtc = new DataTransferContainer();
      dtc.user = new User();
      dtc.user.name = userName ? userName : 'new-user';
      dtc.user.id = userId ? userId : null;
      dtc.user.iconUrl = iconUrl;
      dtc.user.schemaString = schemaString;
      return this.restService.POST(Constants.REST_ENDPOINT_CREATE_ROOM, dtc);
    } else {
      return this.restService.GET(Constants.REST_ENDPOINT_CREATE_ROOM);
    }
  }



  private getPathId(): string {
    return this.activatedRoute.snapshot.paramMap.get('id');
  }

  private updateAtRoomInit(response: DataTransferContainer) {
    let user = response.user;
    this.store.localUser = user;
    this.store.roomId = response.roomId;
    //this.store.users.set(user.id, user); // TODO refresh/update whole list
  }

  handleEnterRoomResponse = (response: DataTransferContainer) => {
    if (response.user && response.from && response.roomId) {
      this.replaceUrl(response.roomId);
      this.handleRoomInitResponse(response);
      Logger.success((response.purpose == 0 ? 'Created Room' : 'Joined Room') + ' -> finalizing')
    }
  };

  handleRoomInitResponse(response: DataTransferContainer) {
    this.appManager.addLocalUserWelcomeNotification(response.user.name);
    this.updateAtRoomInit(response);
    this.cookieManager.setUserCookieInital(response, this.appManager.currentTheme)
    //this.cookieManager.refreshCookieContinuously(this.appManager);
    this.requestInitalValues();
    this.finalizeJoinAttempt();
  }

  /**
   * playlist, users, currentlymostpopular videos
   */
  requestInitalValues() {
    this.restService.GET("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/userlist")
      .subscribe((users: User[]) => {
        this.store.updatedUsers(users)
      });

    this.restService.GET("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/playlist")
      .subscribe((playlist: Video[]) => {
        this.store.playlist = playlist;
      });

    //TODO alternative like news from youtube
    let that = this;
    const waitForDataApi = setInterval(function () {
      if (that.appManager.dataApiManager && that.store) {
        that.searchResults = that.store.searchResultsHistory.get(that.store.searchResultsHistoryKeys[that.currentSearchResultsIndex])
        clearInterval(waitForDataApi);
      }
    }, 2);
  }

  private finalizeJoinAttempt() {
    this.websocket.connect();
  }


  private replaceUrl(roomId: string) {
    //SOLLTE VON CONFIG ERMITTELT WERDEN
    //let url: string = Constants.FRONTEND_URL_POSTFIX + roomId;
    window.history.replaceState({}, "", this.getReplaceRoomLink(roomId));
  }

  getReplaceRoomLink(roomId: string) {
    const end: number = document.URL.lastIndexOf("/");
    return document.URL.substring(0, end + 1) + roomId;
  }


  // Error Section
  private OnJoinRoomRequestError = (error) => {
    Logger.error("join room request has failed");
    this.router.navigateByUrl('errorPage');
  };

  OnRequestError = (error) => {
    Logger.error("create room request has failed");
    this.router.navigateByUrl('errorPage')
  };


  @HostListener("window:beforeunload", ["$event"])
  beforeunloadHandler($event: any) {
    this.disconnect();
  }

  disconnect() {
    if (this.localUser && this.localUser.id && this.roomId) {
      this.sendDisconnectMessage();
      this.closeWebsocket();
      this.webStorageManager.storeRoomContainer();
    }
  }

  private sendDisconnectMessage() {
    let dtc: DataTransferContainer = new DataTransferContainer();
    dtc.from = this.localUser.id;
    dtc.roomId = this.roomId;
    this.websocket.sendWebsocketRequest("disconnect-client", dtc);
  }

  private closeWebsocket() {
    this.websocket.closeWebsocket();
  }

  limit = 400;
  lastScrollY = window.scrollY;
  onScroll = (event) => {
    const target: any = event.target;
    if (target.id === 'playlist_container') {
      return;
    }
    if (!this.videoContainer) {
      this.videoContainer = document.getElementById('videocontainer');
    }
    //Picture In Picture
    this.handlePictureInPicture();
    //control behaviour of scroll back to video button
    this.handleDisplayScrollBackToVideo();
    // control docking behaviour of topbar
    this.handleTopBarDocking();
  }


  handlePictureInPicture() {
    if (this.appManager && this.appManager.videoService) {
      let previousLastScrollY = this.lastScrollY;
      this.lastScrollY = window.scrollY;
      if (window.scrollY > this.limit && !this.pipActive && this.appManager.videoService.selectedPlayer) {
        this.appManager.videoService.selectedPlayer.enablePiP('pip');
        this.pipActive = true;
        return;
      }
      if (window.scrollY <= this.limit && this.pipActive && window.scrollY < previousLastScrollY && this.appManager.videoService.selectedPlayer) {
        this.appManager.videoService.selectedPlayer.disablePiP('nopip');
        this.pipActive = false;
        return;
      }
    }
  }

  handleTopBarDocking() {
    if (window.scrollY >= Constants.topBarDockingThreshold) {
      this.topbarDocked = true;
    } else {
      this.topbarDocked = false;
    }
  }

  handleDisplayScrollBackToVideo() {
    if (this.videoContainer.clientHeight * 2 / 3 < window.scrollY) {
      this.displayScrollBackToVideo = true;
    } else {
      this.displayScrollBackToVideo = false;
    }
  }

  scrollBackToVideoContainer() {
    window.scrollTo({top: 0, behavior: 'smooth'})
  }

  closeSideNav() {
    this.appManager.closeSideNav()
  }

}

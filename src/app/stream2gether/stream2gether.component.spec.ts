import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Stream2getherComponent } from './stream2gether.component';

describe('Stream2getherComponent', () => {
  let component: Stream2getherComponent;
  let fixture: ComponentFixture<Stream2getherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Stream2getherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Stream2getherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

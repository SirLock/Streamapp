import {animate, AnimationMetadata, style, transition, trigger} from "@angular/animations";

export class CustomAnimations {
  public static opInOut: AnimationMetadata =
    trigger('opInOut', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('200ms ease-in-out', style({opacity: '1'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in-out', style({opacity: '0'}))
      ])
    ]);

  public static opInOutSlow: AnimationMetadata =
    trigger('opInOutSlow', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('400ms ease-in-out', style({opacity: '1'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in-out', style({opacity: '0'}))
      ])
    ]);

  public static opInSlideOut: AnimationMetadata =
    trigger('opInSlideOut', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('200ms ease-in-out', style({opacity: '1'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in-out', style({opacity: '0', transform: 'translate(0, 30px)'}))
      ])
    ]);

  public static opIn: AnimationMetadata =
    trigger('opIn', [
      transition(':enter', [
        //style({opacity: '0'}),
        animate('200ms ease-in-out', style({opacity: '1'}))
      ])
    ]);

  public static opInSlow: AnimationMetadata =
    trigger('opInSlow', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('400ms ease-in-out', style({opacity: '1'}))
      ])
    ]);

  public static opOut: AnimationMetadata =
    trigger('opOut', [
      transition(':leave', [
        animate('200ms ease-in-out', style({opacity: '0'}))
      ])
    ]);

  public static opOutMedium: AnimationMetadata =
    trigger('opOutMedium', [
      transition(':leave', [
        animate('300ms ease-in-out', style({opacity: '0'}))
      ])
    ]);

  public static opOutSlow: AnimationMetadata =
    trigger('opOutSlow', [
      transition(':leave', [
        animate('400ms ease-in-out', style({opacity: '0'}))
      ])
    ]);


  public static bluescreenSlideIn: AnimationMetadata =
    trigger('bluescreenSlideIn', [
      transition(':enter', [
        style({opacity: '0', height: '400px', width: '0'}),
        animate('500ms ease-in-out', style({opacity: '1', width: '70%', height: '400px'}))
      ])
    ]);


  //SLIDER FOR PAGES

  /*public static slidePageInRight_: AnimationMetadata =
    trigger('slidePageInRight', [
      transition(':enter', [
        style({
          opacity: '1', position: 'absolute',
          right: '0px',
          top: '0px'
        }),
        animate('500ms ease-in-out', style({opacity: '1', transform: 'translate(-100vw, 0)'}))
      ]),
      transition(':leave', [
        animate('500ms ease-in-out', style({opacity: '0'}))
      ])
    ]);*/


  public static slidePageInFromRight: AnimationMetadata =
    trigger('slidePageInRight', [
      transition('* => fromRight', [
        style({
          position: 'absolute',
          right: '0px',
          top: '0px'
        }),
        animate('350ms ease-in-out', style({transform: 'translate(-100vw, 0)'}))
      ])
    ]);

  public static slidePageInFromLeft: AnimationMetadata =
    trigger('slidePageInLeft', [
      transition('* => fromLeft', [
        style({
          position: 'absolute',
          left: '-100vw',
          top: '0px'
        }),
        animate('350ms ease-in-out', style({transform: 'translate(100vw, 0)'}))
      ])
    ]);


}

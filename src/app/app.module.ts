import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponentContainer } from './chat-container/chat-container.component';
import { PlaylistHistoryContainerComponent } from './playlist-history-container/playlist-history-container.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { HistoryComponent } from './history/history.component';
import { VideoComponent } from './video/video.component';
import { SearchResultComponent } from './searchresult/search-result.component';
import { TopbarComponent } from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { DatePipe } from '@angular/common';
import { Stream2getherComponent } from './stream2gether/stream2gether.component';
import { RepeatPipe } from './repeat.pipe';
import { OrderPipe } from './order.pipe';
import { FormsModule } from '@angular/forms';
import { UsernamePipe } from './username.pipe';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ClipboardModule } from 'ngx-clipboard';
import { ResponsiveService } from './responsive.service';

import { ClickOutsideModule } from 'ng-click-outside';
import { LanguagePipe } from './language.pipe';
import { S2gDatePipe } from './s2g-date.pipe';
import { IconEditorComponent } from './iconeditor/iconeditor.component';
import { ErrorPageComponent } from './errorpage/error-page.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { S9toastrComponent } from './s9toastr/s9toastr.component';
import { ChatComponent } from './chat/chat.component';
import { UserlistComponent } from './userlist/userlist.component';
import { MobileContainerComponent } from './mobile-container/mobile-container.component';
import { TopbarMobileComponent } from './topbar-mobile/topbar-mobile.component';
import { PlaylistControlsComponent } from './playlist-controls/playlist-controls.component';
import { DsgvoComponent } from './dsgvo/dsgvo.component';
import { PhilosophyComponent } from './philosophy/philosophy.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { CookieDetailsComponent } from './cookie-details/cookie-details.component';
import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';
import { OverlayContainerComponent } from './overlay-container/overlay-container.component';
import { SpinnerComponent } from './spinner/spinner.component';
import {ScrollingModule} from "@angular/cdk/scrolling";
import { SafePipe } from './safe.pipe';
import { SearchControlsComponent } from './search-controls/search-controls.component';
import { VideoLinkPipe } from './video-link.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponentContainer,
    PlaylistHistoryContainerComponent,
    PlaylistComponent,
    HistoryComponent,
    VideoComponent,
    SearchResultComponent,
    TopbarComponent,
    FooterComponent,
    Stream2getherComponent,
    RepeatPipe,
    OrderPipe,
    UsernamePipe,
    LanguagePipe,
    S2gDatePipe,
    IconEditorComponent,
    ErrorPageComponent,
    CookiePolicyComponent,
    S9toastrComponent,
    ChatComponent,
    UserlistComponent,
    MobileContainerComponent,
    TopbarMobileComponent,
    PlaylistControlsComponent,
    DsgvoComponent,
    PhilosophyComponent,
    ImpressumComponent,
    TermsOfUseComponent,
    CookieDetailsComponent,
    MobileMenuComponent,
    OverlayContainerComponent,
    SpinnerComponent,
    SafePipe,
    SearchControlsComponent,
    VideoLinkPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    DragDropModule,
    ClipboardModule,
    ClickOutsideModule,
    ScrollingModule
  ],
  providers: [CookieService, DatePipe, ResponsiveService],
  bootstrap: [AppComponent]
})
export class AppModule { }

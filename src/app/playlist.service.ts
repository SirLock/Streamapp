import {Injectable} from '@angular/core';
import {StoreService} from './services/store.service';
import {SearchResult} from './models/searchresult.model';
import {Video} from './models/video.model';
import {Logger} from './helper/logger';
import {RestService} from './services/rest.service';
import {User} from './models/user.model';
import {PlaylistCommand} from './models/playlistcommand.model';
import {Constants} from './helper/constants';
import {AppManagerService} from './services/appmanager.service';
import {IService} from './services/iservice.model';
import {PlaylistState} from './models/playliststate.model';
import {moveItemInArray, transferArrayItem,} from '@angular/cdk/drag-drop';


@Injectable({
  providedIn: 'root'
})
export class PlaylistService implements IService {
  register() {
    this.appManager.playlistService = this;
  }

  private playlistState_: PlaylistState;

  get playlistState(): PlaylistState {
    return this.playlistState_;
  }

  set playlistState(val: PlaylistState) {
    this.playlistState_ = val;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  get playlist(): Video[] {
    return this.store.playlist;
  }

  set playlist(val: Video[]) {
    this.store.playlist = val;
  }

  constructor(private appManager: AppManagerService, private store: StoreService, private restService: RestService) {
    this.register();
  }

  public requestAndHandleInitilPlaylistState() {
    this.restService.GET("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/playlist-state")
      .subscribe(this.handleInitialPlaylistState)
  }

  sendPlaylistCommand(action: string, video: Video, index: number, previousIndex?: number) {
    let playlistCommand = this.buildPlaylistCommand(action, video, index, previousIndex);
    this.restService.POST("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/playlist-command", playlistCommand)
      .subscribe(this.handleRestResponse, this.handleRestError)
  }


  sendImportPlaylistCommand(playlist: Video[]) {
    let playlistCommand = this.buildPlaylistCommand(Constants.PL_CMD_IMPORT_PLAYLIST, null, -1, -1, playlist);
    this.restService.POST("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/playlist-command", playlistCommand)
      .subscribe(this.handleRestResponse, this.handleRestError)

  }

  handleInitialPlaylistState = (response) => {
    this.playlistState = <PlaylistState>response;
  };

  handleRestResponse = (response) => {
    Logger.log("REST_RESPONSE: Playlist Command erfolgreich ausgeführt")
  };

  handleRestError = (error) => {
    this.appManager.displaySearchResultOverlay = false;
    Logger.error("REST_ERROR: Fehler beim Ausführen des Playlist Command")
  };


  getVideoWithoutIdIfExists(): Video {
    let noIdVids: Video[] = this.playlist.filter(vid => !vid.id);
    return noIdVids && noIdVids.length > 0 ? noIdVids[0] : null;
  }

  handlePlaylistCommandResponse = (serverResponse) => {
    let playlistCmd: PlaylistCommand = JSON.parse(serverResponse.body);
    this.appManager.fallbackMode = playlistCmd.fallbackMode;

    switch (playlistCmd.action) {
      case Constants.PL_CMD_APPEND: {
        this.playlist = [...this.playlist, playlistCmd.video];
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_INSERT_AT: {
        transferArrayItem([playlistCmd.video], this.playlist, 0, playlistCmd.index);
        //this.playlist.splice(playlistCmd.index, 0, playlistCmd.video);
        this.playlist = [...this.playlist];

        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_DELETE: {
        this.playlist.splice(playlistCmd.index, 1);
        this.playlist = [...this.playlist];
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_CHANGE_POSITION: {
        moveItemInArray(this.playlist, playlistCmd.previousIndex, playlistCmd.index);
        this.playlist = [...this.playlist];
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_START_IMMEDIATELY: {
        if (this.appManager.fallbackMode) {
          this.appManager.updateCurrentVideoDataIfNeeded(playlistCmd.video, playlistCmd.from);
        }
        this.appManager.videoService.nextVideo = null;
      }
        break;
      case Constants.PL_CMD_START_NEXT: {

      }
        break;
      case Constants.PL_CMD_REPEAT_ALL: {
        this.playlistState.repeat = Constants.PL_CMD_REPEAT_ALL;
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_REPEAT_ONE: {
        this.playlistState.repeat = Constants.PL_CMD_REPEAT_ONE
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_NO_REPEAT: {
        this.playlistState.repeat = Constants.PL_CMD_NO_REPEAT;
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_SEQUENCE_ORDER: {
        this.playlistState.order = Constants.PL_CMD_SEQUENCE_ORDER;
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_RANDOM_ORDER: {
        this.playlistState.order = Constants.PL_CMD_RANDOM_ORDER;
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
      case Constants.PL_CMD_IMPORT_PLAYLIST: {
        this.playlist = [...this.playlist, ...playlistCmd.playlist]
        this.appManager.videoService.requestNextVideoFromServer();
        this.appManager.displaySearchResultOverlay = false;
        this.appManager.displayLatestSearchResults();
        this.appManager.dataApiManager.displayPlaylistImportOptions = false;
      }
        break;
      case Constants.PL_CMD_CLEAR_LIST: {
        this.playlist = [];
        this.appManager.videoService.requestNextVideoFromServer();
      }
        break;
    }
  }

  buildPlaylistCommand(action: string, video: Video, index: number, previousIndex: number, playlist?: Video[]): PlaylistCommand {
    let playlistCmd = new PlaylistCommand();
    playlistCmd.from = this.localUser.id;
    playlistCmd.action = action;
    playlistCmd.video = video;
    playlistCmd.playlist = playlist;
    playlistCmd.index = index;
    playlistCmd.previousIndex = previousIndex;
    return playlistCmd;
  }

}

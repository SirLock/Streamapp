import { Pipe, PipeTransform } from '@angular/core';
import { StoreService } from './services/store.service';
import { User } from './models/user.model';

@Pipe({
  name: 'id2name',
  pure: false
})
export class UsernamePipe implements PipeTransform {

  transform(id: string): string {
    return this.store.resolveUserIdToName(id);
  }

  constructor(private store: StoreService) {

  }

}

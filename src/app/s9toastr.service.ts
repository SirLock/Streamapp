import { Injectable } from '@angular/core';
import {S9Toastr} from "./s9toastr/s9toastr.model";
import * as UUID from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class S9toastrService {


  toastrs: Map<UUID, S9Toastr> = new Map<UUID, S9Toastr>();

  constructor() { }


  public toast(title: string, body: string, duration: number, css: string) {
    const toastr = new S9Toastr();
    toastr.id = UUID.v4();
    toastr.body = body;
    toastr.title = title;
    toastr.duration = duration;
    toastr.css = css;
    this.addToastr(toastr);
    this.removeAfterDuration(toastr);
  }

  removeAfterDuration(toastr: S9Toastr) {
    let that = this;
    setTimeout(function() {
      that.removeToastr(toastr.id);
    }, toastr.duration)
  }

  addToastr(toastr: S9Toastr) {
    this.toastrs.set(toastr.id, toastr);
  }

  removeToastr(id: UUID) {
    this.toastrs.delete(id);
  }

}

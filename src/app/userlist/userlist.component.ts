import { Component, OnInit } from '@angular/core';
import {User} from "../models/user.model";
import {StoreService} from "../services/store.service";
import {AppManagerService} from "../services/appmanager.service";
import {DataTransferContainer} from "../models/datatransfercontainer.model";
import {Util} from "../helper/util";
import {WebsocketService} from "../services/websocket.service";
import {CustomAnimations} from "../customanimations";

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss'],
  animations:[CustomAnimations.opInSlow]
})
export class UserlistComponent implements OnInit {

  public userNameInputFieldIsRevealed: boolean;
  public localUserNameCopy: string;


  get localUser(): User {
    return this.store.localUser;
  }
  set localUser(val: User) {
    this.store.localUser = val;
  }

  get sortedUsers(): User[] {
    return this.store.sortedUsers;
  }

  get displayIconEditor(): boolean {
    return this.appManager.displayIconEditor;
  }

  set displayIconEditor(val: boolean) {
    this.appManager.displayIconEditor = val;
  }

  constructor(private store: StoreService,
              private appManager: AppManagerService,
              private websocket: WebsocketService) { }

  ngOnInit(): void {
  }

  toggleDisplayIconEditor() {
    this.displayIconEditor = !this.displayIconEditor;
  }


  private delayedFocusOnInputField() {
    let that = this;
    const waitForInputField = setTimeout(() => {
      const inputField: HTMLInputElement = <HTMLInputElement>document.getElementById("changeUsernameInputField");
      if (inputField) {
        inputField.value = that.localUser.name;
        that.localUserNameCopy = that.localUser.name;
        inputField.focus();
        clearInterval(waitForInputField);
      }
    }, 25);
  }


  public triggerChangeUserName() {
    this.displayUserNameInputField(false)
    this.sendRequestChangeUserName();
  }

  public displayUserNameInputField(value: boolean) {
    if(value) {
      this.delayedFocusOnInputField();
    }
    this.userNameInputFieldIsRevealed = value;
  }

  private sendRequestChangeUserName() {
    let dtc: DataTransferContainer = new DataTransferContainer();
    dtc.from = this.localUser.id;
    dtc.user = Util.cloneUser(this.localUser);
    dtc.user.name = this.localUserNameCopy;
    this.websocket.sendWebsocketRequest("roomId/" + this.store.roomId + "/change-user-name", dtc);
  }

  clickOutsideUserNameInputField(event: Event) {
    let target: any = event.target;
    if (target.id !== 'editUserName' && target.id !== 'userNameInputField' && target.id !== 'readOnlyUserName') {
      this.triggerChangeUserName();
    }

  }


}

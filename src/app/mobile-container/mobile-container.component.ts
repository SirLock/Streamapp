import {Component, OnInit} from '@angular/core';
import {User} from "../models/user.model";
import {StoreService} from "../services/store.service";
import {AppManagerService} from "../services/appmanager.service";
import {Video} from "../models/video.model";
import {gsap, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin, TimelineLite} from "gsap/all";
import {DataApiManagerService} from '../services/data-api-manager.service';
import {IDataApi} from '../models/idataapi';
import {CustomAnimations} from "../customanimations";
import {Observable, of} from "rxjs";
import {ChatMessage} from "../models/chatmessage.model";

gsap.registerPlugin(MorphSVGPlugin, MotionPathPlugin);

@Component({
  selector: 'app-mobile-container',
  templateUrl: './mobile-container.component.html',
  styleUrls: ['./mobile-container.component.scss'],
  animations: [CustomAnimations.slidePageInFromLeft, CustomAnimations.slidePageInFromRight]
})
export class MobileContainerComponent implements OnInit {

  //STATES
  searchTabState: number = 0;
  playlistTabState: number = 1;
  chatTabState: number = 2;
  userlistTabState: number = 3;
  historyTabState: number = 4;

  isFirefox: boolean = false;
  slideDirection: string;


  updateSlideDirection(index: number) {
    if (index > this.currentTabIndex) {
      this.slideDirection = 'fromLeft';
      return;
    }

    if (index < this.currentTabIndex) {
      this.slideDirection = 'fromRight';
    }
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  get previousControlStateIndex(): number {
    return this.appManager.previousControlStateIndex;
  }

  set previousControlStateIndex(value: number) {
    this.appManager.previousControlStateIndex = value;
  }

  get selectedDataApiId(): string {
    return this.dataApiManager.selectedDataApiId;
  }

  set selectedDataApiId(val: string) {
    this.dataApiManager.selectedDataApiId = val;
  }

  get selectedDataApi(): IDataApi {
    return this.dataApiManager.selectedDataApi;
  }

  get chatMessages(): ChatMessage[] {
    return this.store.chatMessages;
  }

  get history(): Video[] {
    return this.store.history;
  }

  constructor(private store: StoreService, private appManager: AppManagerService, private dataApiManager: DataApiManagerService) {
    //this.initKeyBoardHandling();
  }

  tlHistory = gsap.timeline({paused: true});
  tlPlaylist = gsap.timeline({paused: true, defaults: {duration: 0.2, ease: "bounce.out"}});
  tlChat = gsap.timeline({paused: true});
  tlUser = gsap.timeline({paused: true, defaults: {duration: 0.2}});

  ngOnInit(): void {
    this.isFirefox = window.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;


    this.tlHistory.fromTo("#hours", 1, {rotation: 0, transformOrigin: '0% 100%'}, {rotation: 1440}, 0)
      .fromTo("#minutes", 1, {rotation: 0}, {rotation: 360}, 0)
      .fromTo("#circle", 0.8, {rotation: 0, transformOrigin: '15px 11px'}, {rotation: -360}, 0);


    this.tlPlaylist.to("#path11", {y: -4, ease: "power1.out"}, 0.1)
      .to("#path11", {y: 0, ease: "power1.out"}, 0.3)
      .to("#text", {y: -4, ease: "power1.out"}, 0.1)
      .to("#text", {y: 0, ease: "power1.out"}, 0.3)
      .to("#path12", {morphSVG: "#path22"}, 0.2)
      .to("#path12", {morphSVG: "#path12"}, 0.4)
      .to("#path13", {morphSVG: "#path23"}, 0.3)
      .to("#path13", {morphSVG: "#path13"}, 0.5);


    this.tlChat.to("#circle1", 0.3, {opacity: "0.5"}, 0.0);
    this.tlChat.to("#circle1", 0.3, {opacity: "1"}, 0.3);
    this.tlChat.to("#circle1", 0.3, {opacity: "0.5"}, 0.6);
    this.tlChat.to("#circle1", 0.3, {opacity: "0.5"}, 0.9);
    this.tlChat.to("#circle1", 0.3, {opacity: "1"}, 1.2);
    this.tlChat.to("#circle1", 0.3, {opacity: "0.5"}, 1.5);
    this.tlChat.to("#circle1", 0.3, {opacity: "0.7"}, 1.8);

    this.tlChat.to("#circle2", 0.3, {opacity: "0.5"}, 0.1);
    this.tlChat.to("#circle2", 0.3, {opacity: "1"}, 0.4);
    this.tlChat.to("#circle2", 0.3, {opacity: "0.5"}, 0.7);
    this.tlChat.to("#circle2", 0.3, {opacity: "0.5"}, 0.9);
    this.tlChat.to("#circle2", 0.3, {opacity: "1"}, 1.2);
    this.tlChat.to("#circle2", 0.3, {opacity: "0.5"}, 1.5);
    this.tlChat.to("#circle2", 0.3, {opacity: "1"}, 1.8);

    this.tlChat.to("#circle3", 0.3, {opacity: "0.5"}, 0.2);
    this.tlChat.to("#circle3", 0.3, {opacity: "1"}, 0.5);
    this.tlChat.to("#circle3", 0.3, {opacity: "0.5"}, 0.8);
    this.tlChat.to("#circle3", 0.3, {opacity: "0.5"}, 1.1);
    this.tlChat.to("#circle3", 0.3, {opacity: "1"}, 1.4);
    this.tlChat.to("#circle3", 0.3, {opacity: "0.7"}, 1.7);
    this.tlChat.to("#circle3", 0.3, {opacity: "0.7"}, 2);

    this.tlChat.to("#circle4", 0.3, {opacity: "0.5"}, 0.3);
    this.tlChat.to("#circle4", 0.3, {opacity: "1"}, 0.6);
    this.tlChat.to("#circle4", 0.3, {opacity: "0.5"}, 0.9);
    this.tlChat.to("#circle4", 0.3, {opacity: "0.5"}, 1.2);
    this.tlChat.to("#circle4", 0.3, {opacity: "1"}, 1.5);
    this.tlChat.to("#circle4", 0.3, {opacity: "0.5"}, 1.8);


    this.tlUser.to("#body11", 0.2, {morphSVG: "#body12"}, 0);
    this.tlUser.to("#body12", 0.2, {morphSVG: "#body13"}, 0);
    this.tlUser.to("#body13", 0.2, {morphSVG: "#body11"}, 0);
    this.tlUser.to("#head11", 0.2, {morphSVG: "#head12"}, 0);
    this.tlUser.to("#head12", 0.2, {morphSVG: "#head13"}, 0);
    this.tlUser.to("#head13", 0.2, {morphSVG: "#head11"}, 0);

    this.tlUser.to("#body11", 0.4, {morphSVG: "#body12"}, 0.2);
    this.tlUser.to("#body12", 0.4, {morphSVG: "#body13"}, 0.2);
    this.tlUser.to("#body13", 0.4, {morphSVG: "#body11"}, 0.2);
    this.tlUser.to("#head11", 0.4, {morphSVG: "#head12"}, 0.2);
    this.tlUser.to("#head12", 0.4, {morphSVG: "#head13"}, 0.2);
    this.tlUser.to("#head13", 0.4, {morphSVG: "#head11"}, 0.2);
  }

  get unseenMsgCounterString(): any {
    return this.appManager.unseenMsgCounter <= 9 ? this.appManager.unseenMsgCounter : '9+';
  }

  get unseenMsgCounter(): number {
    return this.appManager.unseenMsgCounter;
  }

  set unseenMsgCounter(val: number) {
    this.appManager.unseenMsgCounter = val;
  }

  get sortedUsers(): User[] {
    return this.store.sortedUsers;
  }

  get controlState(): string {
    return this.appManager.controlState;
  }

  get currentTabIndex(): number {
    return this.appManager.currentTabIndex;
  }

  set currentTabIndex(value: number) {
    this.appManager.currentTabIndex = value;
  }

  get playlist(): Video[] {
    return this.store.playlist;
  }

  set playlist(val: Video[]) {
    this.store.playlist = val;
  }

  openSearchTab() {
    this.switchTab(this.searchTabState);
  }

  openPlaylistTab() {
    this.switchTab(this.playlistTabState);
  }

  openChatTab() {
    this.switchTab(this.chatTabState);
      this.unseenMsgCounter = 0;
      this.appManager.scrollToChatElementOrBottom();
  }

  openUserListTab() {
    this.switchTab(this.userlistTabState);
  }

  openHistoryTab() {
    this.switchTab(this.historyTabState);
  }


  triggerAnimation(tabIndex: number) {
    switch (tabIndex) {
      case this.searchTabState :
        break;
      case this.playlistTabState :
        this.tlPlaylist.play(0);
        break;
      case this.chatTabState :
        this.tlChat.play(0);
        break;
      case this.userlistTabState :
        this.tlUser.play(0);
        break;
      case this.historyTabState :
        this.tlHistory.play(0);
        break;
    }
  }

  switchTab(tabIndex: number) {
    if (tabIndex !== this.currentTabIndex) {
      this.updateSlideDirection(tabIndex);
      this.currentTabIndex = tabIndex;
      this.appManager.scrollToAppBottom();
      this.triggerAnimation(tabIndex);
    }
  }

  searchInput: string = '';

  triggerSearchInput(value: any, event: any) {
    event.target.blur();
    if (value !== this.searchInput) {
      this.searchInput = value;
      const asObservable: Observable<any> = of(this.searchInput);
      this.dataApiManager.handleMobileKeyboardInput(asObservable);
    }
  }

  enteredPlaylistTabWhileDragging(event: any) {
    this.openPlaylistTab();
  }

  enteredSearchTabWhileDragging(event: any) {
    this.openSearchTab();
  }

}

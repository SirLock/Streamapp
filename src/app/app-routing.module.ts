import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { Stream2getherComponent } from './stream2gether/stream2gether.component';
import {ErrorPageComponent} from "./errorpage/error-page.component";


interface Route {
  path?: string;
  component?: Object |string;

}

const routes: Routes = [
  { path: '', component: Stream2getherComponent },
  { path: 'errorPage', component: ErrorPageComponent },
  { path: ':id', component: Stream2getherComponent },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

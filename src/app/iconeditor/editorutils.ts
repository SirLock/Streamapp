import { Cell } from '../models/cell.model';
import { RGB } from '../models/rgb.model';
import { Constants } from '../helper/constants';
import { IconEditorComponent } from './iconeditor.component';
import { Util } from '../helper/util';

export class EditorUtils {


    public static usesTwoColors(schema: Cell[][]): boolean {
        let initalColor: RGB = schema[0][0].color;
        for (let row of schema) {
          for (let cell of row) {
            if (!cell.color.equals(initalColor)) {
              return true;
            }
          }
        }
        return false;
      }

      public static schemaToString(schema: Cell[][], iconEditor: IconEditorComponent): string {

        let tileColorString: string = Constants.COLOR_SEPARATOR + iconEditor.selectedColors.get(0).stripped;
        let bgColorString: string = Constants.COLOR_SEPARATOR + iconEditor.selectedColors.get(1).stripped;
        let cellStatesString: string = "";
        schema.forEach(row => {
          row.forEach((cell: Cell) => {
            if (iconEditor.color1.equals(cell.color)) {
              cellStatesString += Constants.TILE_ICON_CODE + Constants.CELL_SEPARATOR;
            } else {
              cellStatesString += Constants.BG_ICON_CODE + Constants.CELL_SEPARATOR;
            }
          })
          cellStatesString = cellStatesString.substring(0, cellStatesString.length - 1); //CUT LAST SEPARATOR PER ROW
          cellStatesString += Constants.ROW_SEPARATOR;
        })

        return tileColorString + bgColorString + '>' + cellStatesString;
      }

      public static buildSchema(schemaString: string, iconEditor: IconEditorComponent) {

        let colorsAndRows: string[] = schemaString.split(Constants.START_SEPARATOR);
        let colors: string[] = colorsAndRows[0].split(Constants.COLOR_SEPARATOR);
        let tileColorRGBArr: string[] = colors[1].split(Constants.VALUE_SEPARATOR);
        let bgColorRGBArr: string[] = colors[2].split(Constants.VALUE_SEPARATOR);

        let tileColorRGB: RGB = Util.toRGB(tileColorRGBArr);
        let bgColorRGB: RGB = Util.toRGB(bgColorRGBArr);
        let states: string[] = colorsAndRows[1].split(Constants.ROW_SEPARATOR);
        let rows: any[] = [];
        for (let i = 0; i < states.length; i++) {
          rows.push(states[i].split(Constants.CELL_SEPARATOR));
        }

        iconEditor.schema = [];
        for (let y = 0; y < Constants.ICON_SIZE / Constants.TILE_SIZE; y++) {
          let row: Cell[] = [];
          for (let x = 0; x < Constants.ICON_SIZE / Constants.TILE_SIZE; x++) {
            let cell: Cell = new Cell();
            let iconCode = rows[y][x];
            let isTile: boolean = parseInt(iconCode) === Constants.TILE_ICON_CODE;
            cell.color = isTile ? tileColorRGB : bgColorRGB;
            cell.x = x;
            cell.y = y;
            cell.size = Constants.DISPLAYED_EDITOR_TILE_SIZE;
            cell.leftClicked = isTile;
            row.push(cell)
          }
          iconEditor.schema.push(row);
        }
        iconEditor.selectedColors.set(0, tileColorRGB);
        iconEditor.selectedColors.set(1, bgColorRGB);
        iconEditor.currentlyPaintingColorKey = 0;
      }
}

import {Component, OnInit} from '@angular/core';
import {AppManagerService} from '../services/appmanager.service';
import {Cell} from '../models/cell.model';
import {StoreService} from '../services/store.service';
import {User} from '../models/user.model';
import {WebsocketService} from '../services/websocket.service';
import {DataTransferContainer} from '../models/datatransfercontainer.model';
import {RGB} from '../models/rgb.model';
import {EditorUtils} from './editorutils';

@Component({
  selector: 'app-iconeditor',
  templateUrl: './iconeditor.component.html',
  styleUrls: ['./iconeditor.component.scss']
})
export class IconEditorComponent implements OnInit {

  private COLORS: RGB[] = [
    new RGB(11, 122, 117),
    new RGB(42, 42, 114),
    new RGB(82, 25, 69),
    new RGB(131, 151, 136),
    new RGB(186, 168, 152),
    new RGB(200, 173, 85)
  ];
  private selectedColors_: Map<number, RGB> = new Map<number, RGB>();
  private currentlyPaintingColorKey_: number;
  private schema_: Cell[][];

  get selectedColors(): Map<number, RGB> {
    return this.selectedColors_;
  }

  get colors() {
    return this.COLORS;
  }

  get displayGrid(): boolean {
    return this.appManager.displayGrid;
  }

  set displayGrid(val: boolean) {
    this.appManager.displayGrid = val;
  }

  set displayIconEditor(val: boolean) {
    this.appManager.displayIconEditor = val;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }


  get schema(): Cell[][] {
    return this.schema_;
  }

  set schema(val: Cell[][]) {
    this.schema_ = val;
  }

  get currentlyPaintingColorKey(): number {
    return this.currentlyPaintingColorKey_;
  }

  set currentlyPaintingColorKey(val: number) {
    this.currentlyPaintingColorKey_ = val;
  }

  get currentlyPaintingColor(): RGB {
    return this.selectedColors.get(this.currentlyPaintingColorKey);
  }

  set currentlyPaintingColor(val: RGB) {
    this.selectedColors.set(this.currentlyPaintingColorKey, val);
  }

  get color1(): RGB {
    return this.selectedColors.get(0);
  }

  get color2(): RGB {
    return this.selectedColors.get(1);
  }

  constructor(private websocket: WebsocketService,
              private store: StoreService,
              private appManager: AppManagerService) {
  }

  ngOnInit() {
    EditorUtils.buildSchema(this.localUser.schemaString, this);
  }

  closeEditor() {
    this.displayIconEditor = false;
  }

  leftClickedCell(cell: Cell) {
    cell.color = this.currentlyPaintingColor;
  }

  chooseColor(color: RGB) {
    if (!color.equals(this.color1) && !color.equals(this.color2)) {
      this.updateAllCells(this.currentlyPaintingColor, color);
      this.currentlyPaintingColor = color;
    }
  }

  toggleSelectingColor() {
    if (this.currentlyPaintingColorKey === 0) {
      this.currentlyPaintingColorKey = 1;
    } else {
      this.currentlyPaintingColorKey = 0;
    }
  }

  updateAllCells(oldColor: RGB, replacingColor: RGB) {
    this.schema.forEach((row: Cell[]) => {
      row.forEach(cell => {
        if (cell.color.equals(oldColor)) {
          cell.color = replacingColor;
        }
      });
    });
  }

  saveIcon() {
    let dtc = new DataTransferContainer();
    dtc.from = this.localUser.id;
    dtc.user = new User();
    dtc.user.id = this.localUser.id;
    dtc.user.name = this.localUser.name;
    dtc.user.schemaString = EditorUtils.schemaToString(this.schema, this);
    if (this.localUser.schemaString !== dtc.user.schemaString) {
      this.websocket.sendWebsocketRequest('roomId/' + this.store.roomId + '/change-user-icon', dtc);
    }
    this.closeEditor();
  }

  clearWithSelectedColor() {
    this.schema.forEach(row => {
      row.forEach(cell => {
        cell.color = this.currentlyPaintingColor;
      });
    });
  }

  valid(): boolean {
    return EditorUtils.usesTwoColors(this.schema);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconEditorComponent } from './iconeditor.component';

describe('IconeditorComponent', () => {
  let component: IconEditorComponent;
  let fixture: ComponentFixture<IconEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

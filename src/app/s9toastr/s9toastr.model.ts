import * as UUID from 'uuid';

export class S9Toastr {

  id: UUID;
  title: string;
  body: string;
  duration: number;
  css: string;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S9toastrComponent } from './s9toastr.component';

describe('S9toastrComponent', () => {
  let component: S9toastrComponent;
  let fixture: ComponentFixture<S9toastrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S9toastrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S9toastrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {S9toastrService} from "../s9toastr.service";
import {S9Toastr} from "./s9toastr.model";
import * as UUID from 'uuid';
import {CustomAnimations} from "../customanimations";

@Component({
  selector: 'app-s9toastr',
  templateUrl:'./s9toastr.component.html',
  styleUrls: ['./s9toastr.component.scss'],
  animations:[CustomAnimations.opInOut]
})
export class S9toastrComponent implements OnInit {


  get toastrs() : S9Toastr[] {
    return [...this.toastrService.toastrs.values()];
  }

  constructor(private toastrService: S9toastrService) { }

  removeToastr(id: UUID) {
    this.toastrService.removeToastr(id)
  }

  ngOnInit(): void {
  }

}

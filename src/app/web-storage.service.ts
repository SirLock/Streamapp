import {Injectable} from '@angular/core';
import {Constants} from './helper/constants';
import {Logger} from './helper/logger';
import {split} from 'ts-node';
import {RoomStorageContainer} from './models/roomstoragecontainer';
import {ChatMessage} from './models/chatmessage.model';
import {StoreService} from './services/store.service';
import {AppManagerService} from './services/appmanager.service';
import {Util} from './helper/util';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  private capacity: number;
  private keyPrefix: string;
  private freeKeys: string[];
  private keyInUse: string;

  constructor(private store: StoreService, private appManager: AppManagerService) {
    this.keyPrefix = Constants.LOCALSTORAGE_KEY_PREFIX;
    this.capacity = Constants.LOCALSTORAGE_KEYS_LIMIT;
    this.initFreeKeys();
  }

  public restoreRoomContainer(roomId: string) {
    const container: RoomStorageContainer = this.getRoomContainerFromStorage(roomId);
    if (container) {
      if (container.chatMessages) {
        this.store.chatMessages = container.chatMessages;
      }
      if (container.history) {
        this.store.history = container.history;
      }
      if (container.searchResultsHistory) {
        const searchResultHistoryObj: any = JSON.parse(container.searchResultsHistory);
        this.store.searchResultsHistory = new Map(searchResultHistoryObj);
        this.store.searchResultsHistoryKeys = [...this.store.searchResultsHistory.keys()];
        this.appManager.currentSearchResultsIndex = this.store.searchResultsHistoryKeys.length - 1 || 0;
      }
    }
  }

  private getRoomContainerFromStorage(roomId: string): RoomStorageContainer {
    for (let i = 0; i < this.capacity; i++) {
      const key = this.keyPrefix + i;
      const itemString = localStorage.getItem(key);
      if (itemString) {
        if (this.extractRoomId(itemString) === roomId) {
          this.keyInUse = key;
          return JSON.parse(this.extractStoredItem(itemString));
        }
      }
    }
    return null;
  }


  public storeRoomContainer() {
    const container: RoomStorageContainer = this.packRoomContainer();
    const itemString: string = `${container.roomId}#${Date.now().toString()}#${JSON.stringify(container)}`;
    if (this.keyInUse) {
      localStorage.setItem(this.keyInUse, itemString);
    } else if (this.freeKeys.length > 0) {
      this.keyInUse = this.freeKeys.pop();
      localStorage.setItem(this.keyInUse, itemString);
    } else {
      this.keyInUse = this.getOldestRoomContainerKey();
      localStorage.setItem(this.keyInUse, itemString);
    }

  }

  private getOldestRoomContainerKey(): string {
    let keyOfOldestRC: string = null;
    let date = Number.MAX_VALUE;
    for (let i = 0; i < this.capacity; i++) {
      const key = this.keyPrefix + i;
      const storeItem = localStorage.getItem(key);
      if (storeItem) {
        // const rc: RoomStorageContainer = JSON.parse(storeItem);
        const itemDate: number = Number.parseInt(this.extractDate(storeItem));
        if (itemDate < date) {
          date = itemDate;
          keyOfOldestRC = key;
        }
      }
    }
    return keyOfOldestRC;
  }

  // Konvention roomId#date#storedobject
  private extractStoredItem(itemString: string): string {
    return this.itemField(itemString, Constants.LOCALSTORAGE_ITEM_INDEX);
  }

  private extractDate(itemString: string): string {
    return this.itemField(itemString, Constants.LOCALSTORAGE_DATE_INDEX);
  }

  private extractRoomId(itemString: string) {
    return this.itemField(itemString, Constants.LOCALSTORAGE_ROOM_ID_INDEX);
  }

  private itemField(itemString: string, index: number): string {
    return itemString.split(Constants.LOCALSTORAGE_ITEM_STRING_DELIMITER)[index];
  }

  private getAllStoredRooms(): RoomStorageContainer[] {
    let allRooms = [];
    for (let i = 0; i < this.capacity; i++) {
      const key = this.keyPrefix + i;
      const storeItem = localStorage.getItem(key);
      if (storeItem) {
        allRooms.push(JSON.parse(storeItem));
      }
    }
    return allRooms;
  }

  private initFreeKeys() {
    this.freeKeys = [];
    for (let i = this.capacity - 1; 0 <= i; i--) {
      const key = this.keyPrefix + i;
      const itemString = localStorage.getItem(key);
      if (!itemString) {
        this.freeKeys.push(key);
      }
    }
  }

  // TODO IMPLEMENT
  public clearRoomStorage() {
    for (let i = 0; i < this.capacity; i++) {
      const key = this.keyPrefix + i;
      localStorage.removeItem(key);
    }
  }

  // TODO IMPLEMENT
  public clearCompletely() {
    localStorage.clear();
  }

  public packRoomContainer(): RoomStorageContainer {
    const roomContainer: RoomStorageContainer = new RoomStorageContainer();

    roomContainer.roomId = this.store.roomId;
    roomContainer.chatMessages = this.packMessages(this.store.chatMessages);
    roomContainer.history = this.store.history;
    roomContainer.searchResultsHistory = JSON.stringify(Array.from(this.store.searchResultsHistory.entries()));
    return roomContainer;
  }


  public packMessages(chatMessages: ChatMessage[]) {
    return [...chatMessages.map(message => {
      if (message.from === this.store.localUser.id) {
        return message;
      }
      let newChatMessage = new ChatMessage();
      newChatMessage.id = message.id;
      newChatMessage.from = this.store.resolveUserIdToName(message.from);
      newChatMessage.body = message.body;
      newChatMessage.createdAt = message.createdAt;
      return newChatMessage;
    }), Util.getDelimiterChatMessage()];
  }
}

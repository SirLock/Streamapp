import { TestBed } from '@angular/core/testing';

import { TextgeneratorService } from './textgenerator.service';

describe('TextgeneratorService', () => {
  let service: TextgeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextgeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

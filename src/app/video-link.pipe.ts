import {Pipe, PipeTransform} from '@angular/core';
import {Video} from "./models/video.model";
import {SafeUrl} from "@angular/platform-browser";
import {Constants} from './helper/constants';

@Pipe({
  name: 'videoLink'
})
export class VideoLinkPipe implements PipeTransform {

  constructor() {
  }

  transform(hVideo: Video): string {
    return this.getUrlByApi(hVideo);
  }

  getUrlByApi(hVideo: Video): string {
    switch (hVideo.api) {
      case 'youtube' :
        return 'https://www.youtube.com/watch?v=' + hVideo.videoId;
      case Constants.DIRECT_VIDEO_ID :
        return hVideo.videoId;
      default:
        return '';
    }
  }

}

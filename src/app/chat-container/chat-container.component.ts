import { Component, OnInit } from '@angular/core';
import { StoreService } from '../services/store.service';
import { AppManagerService } from '../services/appmanager.service';
import { User } from '../models/user.model';
import { ChatMessage } from '../models/chatmessage.model';
import { WebsocketService } from '../services/websocket.service';
import { DataTransferContainer } from '../models/datatransfercontainer.model';
import { Logger } from '../helper/logger';
import { CustomAnimations } from "../customanimations";
import { Util } from "../helper/util";
import { gsap, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin, DrawSVGPlugin, TimelineLite } from "gsap/all";
gsap.registerPlugin(MorphSVGPlugin, MotionPathPlugin, DrawSVGPlugin);

@Component({
  selector: 'app-chat-container',
  templateUrl: './chat-container.component.html',
  styleUrls: ['./chat-container.component.scss'],
  animations: [
    CustomAnimations.opInSlow,
    CustomAnimations.opOut
  ]
})
export class ChatComponentContainer implements OnInit {

  private chatMessageInputText_: string;
  public userNameInputFieldIsRevealed: boolean;
  public localUserNameCopy: string;

  get closed(): boolean {
    return this.appManager.closed;
  }

  set closed(value: boolean) {
    this.appManager.closed = value;
  }

  get userListOpened(): boolean {
    return this.appManager.userListOpened;
  }

  set userListOpened(value: boolean) {
    this.appManager.userListOpened = value;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  get sortedUsers(): User[] {
    return this.store.sortedUsers;
  }

  get unseenMsgCounterString(): any {
    return this.appManager.unseenMsgCounter <= 9 ? this.appManager.unseenMsgCounter : '9+';
  }

  get unseenMsgCounter(): number {
    return this.appManager.unseenMsgCounter;
  }

  set unseenMsgCounter(val: number) {
    this.appManager.unseenMsgCounter = val;
  }

  get chatToggle(): boolean {
    return this.appManager.chatToggle;
  }

  set chatToggle(val: boolean) {
    this.appManager.chatToggle = val;
  }

  get chatMessages(): ChatMessage[] {
    return this.store.chatMessages;
  }

  set chatMessages(val: ChatMessage[]) {
    this.store.chatMessages = val;
  }

  constructor(private appManager: AppManagerService, private store: StoreService, private webSocket: WebsocketService) {
  }

  public triggerChangeUserName() {
    this.setUserNameInputField(false);
    this.sendRequestChangeUserName();
  }

  private sendRequestChangeUserName() {
    let dtc: DataTransferContainer = new DataTransferContainer();
    dtc.from = this.localUser.id;
    dtc.user = Util.cloneUser(this.localUser);
    dtc.user.name = this.localUserNameCopy;
    this.webSocket.sendWebsocketRequest("roomId/" + this.store.roomId + "/change-user-name", dtc);
  }

  public setUserNameInputField(reveal: boolean, event?: Event) {
    this.userNameInputFieldIsRevealed = reveal;
    this.delayedFocusOnInputField();

  }

  private delayedFocusOnInputField() {
    const waitForInputField = setInterval(() => {
      const inputField: HTMLInputElement = <HTMLInputElement>document.getElementById("changeUsernameInputField");
      if (inputField) {
        inputField.focus();
        clearInterval(waitForInputField);
      }
    }, 10);
  }

  tlChat = gsap.timeline({ paused: true });
  tlUser = gsap.timeline({ paused: true, defaults: { duration: 0.2 } })

  ngOnInit() {
    let that = this;
    let waitForLocalUser = setInterval(
      function () {
        if (that.localUser) {
          that.localUserNameCopy = that.localUser.name;
          clearInterval(waitForLocalUser);
        }
      }, 500);

    this.tlChat.to("#circle1", 0.3, { opacity: "0.5" }, 0.0);
    this.tlChat.to("#circle1", 0.3, { opacity: "1" }, 0.3);
    this.tlChat.to("#circle1", 0.3, { opacity: "0.5" }, 0.6);
    this.tlChat.to("#circle1", 0.3, { opacity: "0.5" }, 0.9);
    this.tlChat.to("#circle1", 0.3, { opacity: "1" }, 1.2);
    this.tlChat.to("#circle1", 0.3, { opacity: "0.5" }, 1.5);
    this.tlChat.to("#circle1", 0.3, { opacity: "0.7" }, 1.8);

    this.tlChat.to("#circle2", 0.3, { opacity: "0.5" }, 0.1);
    this.tlChat.to("#circle2", 0.3, { opacity: "1" }, 0.4);
    this.tlChat.to("#circle2", 0.3, { opacity: "0.5" }, 0.7);
    this.tlChat.to("#circle2", 0.3, { opacity: "0.5" }, 0.9);
    this.tlChat.to("#circle2", 0.3, { opacity: "1" }, 1.2);
    this.tlChat.to("#circle2", 0.3, { opacity: "0.5" }, 1.5);
    this.tlChat.to("#circle2", 0.3, { opacity: "1" }, 1.8);

    this.tlChat.to("#circle3", 0.3, { opacity: "0.5" }, 0.2);
    this.tlChat.to("#circle3", 0.3, { opacity: "1" }, 0.5);
    this.tlChat.to("#circle3", 0.3, { opacity: "0.5" }, 0.8);
    this.tlChat.to("#circle3", 0.3, { opacity: "0.5" }, 1.1);
    this.tlChat.to("#circle3", 0.3, { opacity: "1" }, 1.4);
    this.tlChat.to("#circle3", 0.3, { opacity: "0.7" }, 1.7);
    this.tlChat.to("#circle3", 0.3, { opacity: "0.7" }, 2);

    this.tlChat.to("#circle4", 0.3, { opacity: "0.5" }, 0.3);
    this.tlChat.to("#circle4", 0.3, { opacity: "1" }, 0.6);
    this.tlChat.to("#circle4", 0.3, { opacity: "0.5" }, 0.9);
    this.tlChat.to("#circle4", 0.3, { opacity: "0.5" }, 1.2);
    this.tlChat.to("#circle4", 0.3, { opacity: "1" }, 1.5);
    this.tlChat.to("#circle4", 0.3, { opacity: "0.5" }, 1.8);




    this.tlUser.to("#body11", 0.2, { morphSVG: "#body12" }, 0);
    this.tlUser.to("#body12", 0.2, { morphSVG: "#body13" }, 0);
    this.tlUser.to("#body13", 0.2, { morphSVG: "#body11" }, 0);
    this.tlUser.to("#head11", 0.2, { morphSVG: "#head12" }, 0);
    this.tlUser.to("#head12", 0.2, { morphSVG: "#head13" }, 0);
    this.tlUser.to("#head13", 0.2, { morphSVG: "#head11" }, 0);

    this.tlUser.to("#body11", 0.4, { morphSVG: "#body12" }, 0.2);
    this.tlUser.to("#body12", 0.4, { morphSVG: "#body13" }, 0.2);
    this.tlUser.to("#body13", 0.4, { morphSVG: "#body11" }, 0.2);
    this.tlUser.to("#head11", 0.4, { morphSVG: "#head12" }, 0.2);
    this.tlUser.to("#head12", 0.4, { morphSVG: "#head13" }, 0.2);
    this.tlUser.to("#head13", 0.4, { morphSVG: "#head11" }, 0.2);
  }

  chatToggleBtnClose() {
    this.chatToggle = false;
    this.userListOpened = false;
    this.closed = true;
    this.tlChat.play(0);
  }

  chatToggleBtnOpen() {
    this.chatToggle = true;
    this.userListOpened = false;
    this.closed = false;
    this.unseenMsgCounter = 0;
    this.tlChat.play(0);
    this.appManager.scrollToChatElementOrBottom();
  }

  userListToggle() {
    this.userListOpened = true;
    this.tlUser.play(0);
  }

}

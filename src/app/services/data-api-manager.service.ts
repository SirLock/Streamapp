import {Injectable} from '@angular/core';
import {IDataApi} from '../models/idataapi';
import {ConfigService} from './config.service';
import {Logger} from '../helper/logger';
import {YouTubeDataApi} from '../models/youtubedataapi';
import {Constants} from '../helper/constants';
import {Observable, of} from 'rxjs';
import {RestService} from './rest.service';
import {debounceTime, distinctUntilChanged, switchMap, catchError, map} from 'rxjs/operators';
import {SearchResult} from '../models/searchresult.model';
import {IService} from './iservice.model';
import {AppManagerService} from './appmanager.service';
import {DirectVideoDataApi} from '../models/directvideodataapi.model';
import {NextPageTokenObject} from '../models/nextpagetokenobject.model';
import {DataServiceFactory} from '../models/dataservicefactory';
import {StoreService} from './store.service';
import {User} from '../models/user.model';
import {Video} from '../models/video.model';
import {DataTransferContainer} from '../models/datatransfercontainer.model';
import {S9toastrService} from '../s9toastr.service';
import {LanguagePipe} from '../language.pipe';
import {LanguageService} from '../language.service';

@Injectable({
  providedIn: 'root'
})
export class DataApiManagerService implements IService {

  register() {
    this.appManager.dataApiManager = this;
  }

  get RestService(): RestService {
    return this.restService;
  }

  get fallbackMode(): boolean {
    return this.appManager.fallbackMode;
  }

  set fallbackMode(value: boolean) {
    this.appManager.fallbackMode = value;
  }

  waitForScrollToSearchResult: any;
  private requestingNextSearchResults_: boolean = false;
  private nextPageTokenObj_: NextPageTokenObject;
  private importFromPreview_: boolean = false;
  private displayPlaylistImportOptions_: boolean = false;
  private dataApis_: Map<string, IDataApi>;
  private selectedDataApiId_: string;
  private searchResults_: SearchResult[];

  get displaySearchResultOverlay(): boolean {
    return this.appManager.displaySearchResultOverlay;
  }

  set displaySearchResultOverlay(val: boolean) {
    this.appManager.displaySearchResultOverlay = val;
  }

  get currentSearchInput(): string {
    return this.appManager.currentSearchInput;
  }

  set currentSearchInput(value: string) {
    this.appManager.currentSearchInput = value;
  }

  set requestingNextSearchResults(val: boolean) {
    this.requestingNextSearchResults_ = val;
  }

  get requestingNextSearchResults(): boolean {
    return this.requestingNextSearchResults_;
  }

  set nextPageTokenObj(val: NextPageTokenObject) {
    this.nextPageTokenObj_ = val;
  }

  get nextPageTokenObj(): NextPageTokenObject {
    return this.nextPageTokenObj_;
  }

  set importFromPreview(val: boolean) {
    this.importFromPreview_ = val;
  }

  get importFromPreview(): boolean {
    return this.importFromPreview_;
  }

  set displayPlaylistImportOptions(val: boolean) {
    this.displayPlaylistImportOptions_ = val;
  }

  get displayPlaylistImportOptions(): boolean {
    return this.displayPlaylistImportOptions_;
  }

  get searchResults(): SearchResult[] {
    return this.searchResults_;
  }

  set searchResults(val: SearchResult[]) {
    this.searchResults_ = val;
  }

  get dataApis(): Map<string, IDataApi> {
    return this.dataApis_;
  }

  set dataApis(val: Map<string, IDataApi>) {
    this.dataApis_ = val;
  }

  get selectedDataApiId(): string {
    return this.selectedDataApiId_;
  }

  set selectedDataApiId(val: string) {
    this.selectedDataApiId_ = val;
  }

  get selectedDataApi(): IDataApi {
    return this.dataApis ? this.dataApis.get(this.selectedDataApiId) : null;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  get searchResultsHistory(): Map<string, SearchResult[]> {
    return this.store.searchResultsHistory;
  }

  set searchResultsHistory(val: Map<string, SearchResult[]>) {
    this.store.searchResultsHistory = val;
  }

  get hasPreviousSearchResults(): boolean {
    return this.currentSearchResultsIndex > 0;
  }

  get hasNextSearchResults(): boolean {
    return this.currentSearchResultsIndex < this.searchResultsHistoryKeys.length - 1;
  }

  get currentSearchResultsIndex(): number {
    return this.appManager.currentSearchResultsIndex;
  }

  set currentSearchResultsIndex(value: number) {
    this.appManager.currentSearchResultsIndex = value;
  }

  get searchResultsHistoryKeys(): string[] {
    return this.store.searchResultsHistoryKeys;
  }

  set searchResultsHistoryKeys(val: string[]) {
    this.store.searchResultsHistoryKeys = val;
  }


  constructor(private appManager: AppManagerService,
              private configService: ConfigService,
              private restService: RestService,
              private store: StoreService,
              private toastr: S9toastrService,
              private languageService : LanguageService) {
    this.register();
    this.initDataApis();
  }

  initDataApis() {
    let that = this;
    let waitForConfig = setInterval(function() {
      if (that.configService.CONFIG) {
        DataServiceFactory.buildDataApis(that.configService.CONFIG.apis, that);
        clearInterval(waitForConfig);
        Logger.success('clearInterval: waitForConfig -> building dataApis');
      }
    }, 2);
  }

  detectApiAndSendRequest(searchInput: string) {
    if (searchInput.length <= Constants.MIN_QUERY_LENGTH || searchInput.length >= Constants.MAX_QUERY_LENGTH) {
      return of([]);
    }

    this.nextPageTokenObj = null;
    const ytParams = this.getValidYoutubeParams(searchInput);
    if (ytParams) {
      return this.tryYoutubeApi(ytParams, searchInput);
    }

    const response = this.tryDirectVideoApi(searchInput);
    if (response) {
      return response;
    }

    //FallBack: if could not be validated as link -> search, only for dataservices with search function
    if (!this.fallbackMode) {
      if (this.selectedDataApiId !== Constants.DIRECT_VIDEO_ID && searchInput.length <= Constants.MAX_DIRECT_VIDEO_QUERY_LENGTH) {
        this.appManager.displaySearchResultOverlay = true;
        return this.selectedDataApi.sendSearchRequestToBackend(searchInput);
      } else {
        // searchInput to long
        this.displaySearchInputTooLongToastr(searchInput);
        return of([]);
      }
    } else {
      //SUCHE NICHT ERLAUBT
      this.displaySearchDisabledToastr();
      return of(null);
    }
  }

  getValidYoutubeParams(searchInput: string): Map<string, string> {
    let ytApi = <YouTubeDataApi> this.dataApis.get(Constants.YOUTUBE_ID);
    return ytApi.getValidYouTubeParams(searchInput);

  }

  private tryDirectVideoApi(searchInput) {
    let directVideoDataApi = <DirectVideoDataApi> this.dataApis.get(Constants.DIRECT_VIDEO_ID);
    if (directVideoDataApi) {
      let directVideoSearchResult: SearchResult[] = directVideoDataApi.handleVideoLinkFallback(searchInput);
      if (directVideoSearchResult) {
        this.selectedDataApiId = directVideoDataApi.id;
        return of(directVideoSearchResult);
      } else {
        this.selectedDataApiId = Constants.YOUTUBE_ID;
      }
    }
    return null;
  }

  private tryYoutubeApi(ytParams: Map<string, string>, searchInput: string) {
    const ytApi = <YouTubeDataApi> this.dataApis.get(Constants.YOUTUBE_ID);
    if (ytApi) {
      this.selectedDataApiId = ytApi.id;
      if (this.fallbackMode) {
        const videoId: string = ytParams.get(Constants.YOUTUBE_VIDEO_ID_KEY);
        return of(ytApi.handleVideoLinkFallback(videoId));
      }

      return ytApi.sendRequest(ytParams);
    }
    return of(null);
  }

  private tryYoutubeApiFallBack(searchInput: string) {
    const ytApi = <YouTubeDataApi> this.dataApis.get(Constants.YOUTUBE_ID);
    let ytParams: Map<string, string> = ytApi.getValidYouTubeParams(searchInput);
    if (ytParams) {
      const videoId: string = ytParams.get(Constants.YOUTUBE_VIDEO_ID_KEY);
      if (videoId) {
        const searchResults: SearchResult[] = ytApi.handleVideoLinkFallback(videoId); //can only be one
        return searchResults;
      }
    }
    return null;
  }

  handleMobileKeyboardInput(mobileKeyboardInput: Observable<any>) {
    mobileKeyboardInput.pipe(
      switchMap((searchInput: string) => this.detectApiAndSendRequest(searchInput)
        .pipe(
          catchError(() => this.preProcessErroneousResponse(searchInput)),
          map((response) => this.preProcessResponse(response, searchInput))
        )))
      .subscribe(this.processSearchResponse);
  }

  handleKeyboardInput(keyboardInput_: Observable<any>) {
    keyboardInput_.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap((searchInput: string) => this.detectApiAndSendRequest(searchInput)
        .pipe(
          catchError(() => this.preProcessErroneousResponse(searchInput)),
          map((response) => this.preProcessResponse(response, searchInput))
        )))
      .subscribe(this.processSearchResponse);
  }

  preProcessErroneousResponse(searchInput) {
    return of({error: true, searchResults: null, searchInput: searchInput});
  }

  preProcessResponse(response, searchInput) {
    //Response is null? there's a problem
    if (!response) {
      return {error: true, searchResults: null, searchInput: searchInput};
    }
    //Response is erroneous -> forward it and let the error be handled
    if (response.error) {
      return response;
    }
    //nothing to worry about has happend so be build our final response object
    return {error: false, searchResults: response, searchInput: searchInput};
  }


  public processSearchResponse = (response: any) => {
    if (response) {

      //there's nothing to show let abort
      if (!response.error && response.searchResults.length === 0) {
        return;
      }

      //let handle the error here
      if (response.error) {
        this.onDataApiReponseError();
        this.switchToFallBackModeIfNeeded();
        response.searchResults = this.tryYoutubeApiFallBack(response.searchInput);
        if (!response) { //youtubefallback back was unsuccessful so we're done here
          this.displaySearchDisabledToastr();
          return;
        }
      }//after error handling let's try to process the result as if nothing happened

      //process response according To Mode
      if (this.fallbackMode) {
        this.processValidFallbackResponse(response);
      } else {
        this.processValidResponse(response);
      }
    }
  };

  private displaySearchDisabledToastr() {
    this.toastr.toast('', 'Suche & Playlist Import sind für ' + this.selectedDataApiId + ' momentan deaktiviert. Entschuldige.', 3000, 'info');
  }

  private displaySearchInputTooLongToastr(searchInput: string) {
    this.toastr.toast('', this.languageService.keyToValue('search_input_too_long') + searchInput, 3000, 'info');
  }



  processValidResponse(response: any) {
    this.searchResults = [];
    this.selectedDataApi.handleResponse(response.searchResults, response.searchInput);
    this.scrollToFirstSearchResult();
  }

  processValidFallbackResponse(response: any) {
    this.searchResults = [];
    this.selectedDataApi.handleResponseFallback(response.searchResults, response.searchInput);
    this.scrollToFirstSearchResult();
  }

  scrollToFirstSearchResult() {
    let that = this;
    this.waitForScrollToSearchResult = setTimeout(function() {
      if (that.searchResults.length > 0) {
        let elements = document.getElementsByClassName('searchresult-elem');
        if (elements && elements.length) {
          elements[0].scrollIntoView({behavior: 'smooth', block: 'end', inline: 'nearest'});
          clearTimeout(that.waitForScrollToSearchResult);
        }
      }
    }, 250);
  }

  addSearchResultsToHistory(results: SearchResult[], searchInput: string) {
    if (this.searchResultsHistory.size >= Constants.SEARCH_RESULTS_HISTORY_LIMIT) {
      this.addSearchResultToFullHistory(results, searchInput);
    } else {
      this.currentSearchResultsIndex += 1;
      this.searchResultsHistoryKeys.push(searchInput);
      this.updateSearchResultsInHistory(results, searchInput);
    }
    this.currentSearchInput = searchInput;
  }

  addSearchResultToFullHistory(results: SearchResult[], searchInput: string) {
    if (this.searchResultsHistoryKeys.indexOf(searchInput) > -1) {
      this.searchResultsHistoryKeys.push(searchInput);
      this.searchResultsHistory.set(searchInput, results);
    }
    const oldestKey = this.searchResultsHistoryKeys[0];
    this.searchResultsHistoryKeys = this.searchResultsHistoryKeys.filter((key) => key != oldestKey);
    this.searchResultsHistoryKeys.push(searchInput);
    //const newSearchResultsHistory: Map<string, SearchResult[]> = new Map();
    this.searchResultsHistory.forEach((value, key) => {
      if (key === oldestKey) {
        this.searchResultsHistory.delete(key);
      }
    });
    this.searchResultsHistory.set(searchInput, results);
  }

  updateSearchResultsInHistory(searchResults: SearchResult[], searchInput: string) {
    //this.searchResultsHistoryKeys.push(searchInput);

    this.currentSearchInput = null;
    this.searchResultsHistory.set(this.searchResultsHistoryKeys[this.searchResultsHistoryKeys.length - 1], searchResults);
  }

  getSearchResultsFromHistory(key: string) {
    return this.searchResultsHistory.get(key);
  }

  toPreviousSearchResults() {
    if (this.hasPreviousSearchResults) {
      this.nextPageTokenObj = null;
      this.currentSearchResultsIndex -= 1;
      const key = this.searchResultsHistoryKeys[this.currentSearchResultsIndex];
      this.searchResults = this.getSearchResultsFromHistory(key);
    }
  }

  toNextSearchResults() {
    if (this.hasNextSearchResults) {
      this.nextPageTokenObj = null;
      this.currentSearchResultsIndex += 1;
      this.searchResults = this.getSearchResultsFromHistory(this.searchResultsHistoryKeys[this.currentSearchResultsIndex]);
    }
  }

  toLatestSearchResults() {
    this.nextPageTokenObj = null;
    this.currentSearchResultsIndex = this.searchResultsHistoryKeys.length - 1;
    this.searchResults = this.getSearchResultsFromHistory(this.getHistoryKey(this.currentSearchResultsIndex));
  }

  getHistoryKey(index: number): string {
    return this.searchResultsHistoryKeys[index];
  }

  onDataApiReponseError() {
    this.toastr.toast('', 'Fehlerhaftes Verhalten von ' + this.selectedDataApiId + '. Versuche Fallback Strategie.', 3000, 'error');
    this.displaySearchResultOverlay = false;
    this.displayPlaylistImportOptions = false;
    this.nextPageTokenObj = null;
  }

  switchToFallBackModeIfNeeded() {
    if (!this.fallbackMode) {
      this.appManager.fallbackMode = true;
      this.notifyUsersAboutFallBackMode();
    }
  }

  notifyUsersAboutFallBackMode() {
    this.restService.GET('set-fallback-mode/' + this.appManager.fallbackMode)
      .subscribe(response => {
          Logger.log('fallbackMode: ' + response);
          this.fallbackMode = <boolean> response;
        }
      );
  }

  clearSearchResults() {
    this.searchResults = [];
  }

  updateSearchResultDataFromPlayer(video: Video) {
    this.searchResults
      .filter(searchResult => searchResult.videoId === video.videoId)
      .forEach(searchResult => {
        searchResult.title = video.title;
        searchResult.channelTitle = video.channelTitle;
      });

  }

  notifyBackendAboutVideoDataUpdate(video: Video) {
    this.restService.POST('roomId/' + this.roomId + '/userId/' + this.localUser.id + '/update-video-data/', video)
      .subscribe(Logger.log, Logger.error);
  }
}

import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {Video} from '../models/video.model';
import {ChatMessage} from '../models/chatmessage.model';
import {SearchResult} from "../models/searchresult.model";
import * as UUID from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private localUser_: User;
  private playlist_: Video[] = [];
  private users_: Map<string, User> = new Map<string, User>();
  private chatMessages_: ChatMessage[] = [];
  private roomId_: string;
  private oldUsers_: Map<string, User> = new Map<string, User>();
  private history_: Video[] = [];
  private currentlyPopularVideos_: Video[] = [];
  private searchResultsHistoryKeys_: string[] = [];
  searchResultsHistory_: Map<string, SearchResult[]> = new Map();


  get searchResultsHistory(): Map<string, SearchResult[]> {
    return this.searchResultsHistory_;
  }

  set searchResultsHistory(val: Map<string, SearchResult[]>) {
    this.searchResultsHistory_ = val;
  }

  get searchResultsHistoryKeys(): string[] {
    return this.searchResultsHistoryKeys_;
  }

  set searchResultsHistoryKeys(val: string[]) {
    this.searchResultsHistoryKeys_ = val;
  }

  get currentlyPopularVideos(): Video[] {
    return this.currentlyPopularVideos_;
  }

  set currentlyPopularVideos(val: Video[]) {
    this.currentlyPopularVideos_ = val;
  }

  get oldUsers(): Map<string, User> {
    return this.oldUsers_;
  }

  set oldUsers(val: Map<string, User>) {
    this.oldUsers_ = val;
  }

  get roomId(): string {
    return this.roomId_;
  }

  set roomId(val: string) {
    this.roomId_ = val;
  }

  get localUser(): User {
    return this.localUser_;
  }

  set localUser(val: User) {
    this.localUser_ = val;
  }



  get playlist(): Video[] {
    /*return Array.from({length: 100000}).map((_, i) => {
        const vid =  new Video();
        vid.id = UUID.v4();
        vid.title = `${i}`;
        vid.iconUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARQAAAC2CAMAAAAvDYIaAAAAkFBMVEXMGB7////QMjbJAADKAAvfhojLERjLCxTMFBvKAAfLCRLVT1Lyz9D34OHLAADKAA3no6T66ur99fb88PHtvL3TQ0bRNjr129vwxcbcdXj01dbNHyX45OXUSEzjkpTkmZvZYWTXWl3st7jaam3PKzDccnTqra7SPUHdfX7abG/qsLHklJbOIynmnqDgiovYYmW2ZAZGAAAFg0lEQVR4nO2diXqiShBGu2yG3VbRaFBjXKJJ5k6c93+7iwQN+yJMyFdV5wma89E/RVWjAkoZzp7fts7DAA0PzvbteTYsv2pRImTsKKks29BNROiGbQWX5YxLxBRJWX/MpTIEWgwl5x/rRlLcrVJ63+v+1+hKbd3aUhY7afe94u/BlttFLSnTIxUlF2x59KqlzHSt74V+L5o5q5LiS7PvVX43pvRLpUxeVd9L7AP1OimWstAQP4TLMLRFkRSX3ta5Yko3X8ojXScXK495UgjfJxfi98pNykSRdhJYUZOMlAHRjP3CmKel+CSfxUnUMSllL/te0U9A7uNSPIN4oHxi6l5Mik/sfacIy/+SsuTNEyGXNykO+SfPFcO5SnH5RrkRlnAXKTtCTaUq7N2nlCHfKDHkMJRytvpeyE/COodSVlyjxDBXFykcs0mCqBWw4d2TwNoEUgbop17N0AcgPN49KaQnuMRPI5dizI2UFOokNvyCnELbiL/8MpjCOIgnLt1S6E/ihaWkMF/Equ81/DxYCcMwDMOQw+ChQQbj/V1yPyuFdgZ3Tvu0VBbtFwCMDe4IxwmlgPdG6ZB7JZ9SAIY7jpYbVykAjy8cLRFfUoJoUdwEDYlL4WiJSEgJv7TiaElLCaJlwNGSkQLwMaIeLTlSYOpL2hOWPCkAS4f0HsqXAjBbER7cFkkBeFajvhfXF8VSYPpONVpKpATR8kAzWkqlAOwFxWipkAKwIRgtlVJgciAXLdVSAFxq0VJHCsBvWtFSTwp4Z0WoqVBTShAtWzpNhdpSgmgh01RoIAXgpGiMQhpJAfhFol/ZUAqsKURLUylAYcraXArA2EYeLfdIAe8Nd9VylxTsU9Y7peAehdwtJYgWiXUU0kIKeFhHIW2kACyeUO6hdlLCUQg+LW2lAHzgO8DRXgrCKWsHUvBNWTuRgi1aOpIC8J+FZxTSmRSY4JmydicF0ZS1SykAex3FHupWCsABQ0+hWynTI4qhWadSsJzz6VDK7A+WWqUzKQsHTy+uIym43n+6kYKsv9+FFHTt2vZSEDb220rxME6XW0rBeQ6hlRQX6Rd2LaTgPYBwtxTMp+DulYL6vOR9UpCfrL1HCqLGYz53SMF/Wr+xlD2qYUY+DaUscU7UUzSSMj0iD5OIJlKeJfYwiagvZWYirkyS1JWyQDZDL6WelOkbjTCJqCUFWbexkhpS0HUbK6mUgrDbWEmFFJTdxkrKpZxGtMIkokwKgY9Y8imWsiYYJhGFUkiGSUSBlN825X9IyZXivhINk4gcKQR/8SFFRoqHv9tYSVoKhW5jJUkpNLqNlcSlUOk2VhKTguVsY3tuUmaY56ANiaQg/RjwTkIpuM42tuci5cNC9xVgO0ZnrMeR2vBHUW0QMAzDMAzDdAb/u2mGlRhw5Z3CHIgnrr1T6E/iwK/yKYwD/zF9Bm0jTtwbTKFOYkl5lJuLXAqPpaSQnoABP34S6HMQsCF5rqoYaxNI4VBJIpeBFFhxTRvDXMFFypn3TwzrHEpZc6USQ61DKbCjexAvg/0XPqW4HLU3pBtJAYdfCiMMB65S+Kl8JXgeX6WAz6/KIZYPX1I8nWuVANPwYlJgzxsoQO4hLgV8LlaE8iEpBV7JFyv2K6SlTA3iLQTdmGakwJD2MSNTDiErJahWCFsxPyuUjJTACtkdpMedJKTAwiR6EnokFlAkBTyHZL0iHQ+KpQCc6W0hU55TEtJS4JHaZzhq9Zh2kJFy+a05Qq+Hmjp7GQVZKQCToxyRuFtMTR0nOQLypASFnK/wH4/WlfKHuZefLyWo+sdzqQy094tpKDkfTwsuvkjK5XYZ70ZSKs3WTUTotqakHO3G+TdJlZQLE/e08XcPAyzMH3b+5uTmBUmM/wFya1PjhQ+Q5gAAAABJRU5ErkJggg=='
        return vid;
    });*/
    return this.playlist_;
  }

  set playlist(val: Video[]) {
    this.playlist_ = val;
  }

  get users(): Map<string, User> {
    return this.users_;
  }

  set users(val: Map<string, User>) {
    this.users_ = val;
  }

  get sortedUsers() {
    return [this.localUser, ...this.users.values()];
  }

  get chatMessages(): ChatMessage[] {
    return this.chatMessages_;
  }

  set chatMessages(val: ChatMessage[]) {
    this.chatMessages_ = val;
  }

  get history(): Video[] {
    return this.history_;
  }

  set history(val: Video[]) {
    this.history_ = val;
  }

  constructor() {
    this.chatMessages_ = [];
    this.users_ = new Map<string, User>();
  }

  /**
   * Removes user from users and puts it to the oldUsers for names resolution in chat messages.
   * @param id user id
   */
  removeUser(id: string) {
    let removingUser = this.users.get(id);
    this.users.delete(id);
    this.oldUsers.set(id, removingUser);
  }

  updatedUsers(userList: User[]) {
    this.users = new Map<string, User>();
    userList
      .filter(user => user.id !== this.localUser.id)
      .forEach(user => this.users.set(user.id, user));
  }

  addSingleChatMessage(chatMessage: ChatMessage) {
    this.chatMessages = [...this.chatMessages, chatMessage];
  }

  resetStore() {
    this.localUser = null;
    this.playlist = [];
    this.users = new Map<string, User>();
    this.chatMessages = [];
    this.roomId = null;
    this.oldUsers = new Map<string, User>();
  }

  resolveUserIdToName(id: string): string {
    if(this.localUser && this.localUser.id === id)
      return this.localUser.name;
    if (this.users.has(id))
      return this.users.get(id).name;
    if (this.oldUsers.has(id))
      return this.oldUsers.get(id).name;
    return id;
  }

  updateLocalPlaylistDataFromVideoPlayer(video: Video) {
    this.playlist
      .filter(playlistVideo => playlistVideo.videoId === video.videoId)
      .forEach(playlistVideo => {
        playlistVideo.title = video.title;
        playlistVideo.channelTitle = video.channelTitle;
      })

  }
}

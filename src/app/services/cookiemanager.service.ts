import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { StoreService } from './store.service';
import { User } from '../models/user.model';
import { Cookie } from '../models/s2g.cookie';
import { Constants } from '../helper/constants';
import { Theme } from '../models/theme.model';
import {DataTransferContainer} from "../models/datatransfercontainer.model";
import {Util} from "../helper/util";
import {Logger} from "../helper/logger";
import {AppManagerService} from "./appmanager.service";
import {LanguageService} from "../language.service";
import {OverlayManagerService} from "../overlay-manager.service";

@Injectable({
  providedIn: 'root'
})
export class CookieManagerService {


  private _dsgvoCookieConfirmed: boolean = false;
  domain: string = 'http://localhost:4200'
  private listenForCookieId_: any;

  get dsgvoCookieConfirmed(): boolean {
    return this._dsgvoCookieConfirmed;
  }

  set dsgvoCookieConfirmed(value: boolean) {
    this._dsgvoCookieConfirmed = value;
  }

  get listenForCookieId(): any {
    return this.listenForCookieId_;
  }

  set listenForCookieId(val: any) {
    this.listenForCookieId_ = val;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  initCookieManager() {
    this.domain = Util.getCookieDomain(document.URL);
    Logger.info("c_domain: " + this.domain);
  }

  constructor(private overlayManager: OverlayManagerService, private store: StoreService, private cookieService: CookieService, private languageService: LanguageService) {
    this.initCookieManager();
    this.checkAndUpdateDsgvoCookieStatus();
    this.openCookiePolicy();
    }

  openCookiePolicy() {
    if(!this.dsgvoCookieConfirmed) {
      this.overlayManager.openCookiePolicy();
    }
  }

  checkAndUpdateDsgvoCookieStatus() {
    if(this.hasDsgvoCookie()) {
      this._dsgvoCookieConfirmed = true;
    }else{
      this._dsgvoCookieConfirmed = false;
    }
  }

  updateCookie(theme: Theme) {
    let cookie = new Cookie();
    cookie.user = new User();
    cookie.user.id = this.localUser.id;
    cookie.user.name = this.localUser.name;
    cookie.user.iconUrl = this.localUser.iconUrl;
    cookie.user.schemaString = this.localUser.schemaString;
    cookie.langKey = this.languageService.selectedLanguageKey;
    cookie.theme = theme;
    this.setCookie(Constants.USER_COOKIE_KEY, cookie, Constants.USER_COOKIE_REFRESH_DELAY);
  }

  setCookie(roomId: string, cookie: Cookie, duration: number) {
    this.cookieService.set(roomId, JSON.stringify(cookie), duration, '', this.domain);
  }

  setDSGVOCookie() {
    this.cookieService.set(Constants.DSGVO_COOKIE_KEY, 'true', 365, '', this.domain);
  }

  setUserCookieInital(response: DataTransferContainer, currentTheme?: Theme, langKey?: string) {
    let cookie = new Cookie();
    cookie.user = new User();
    cookie.user.id = response.user.id;
    cookie.user.name = response.user.name;
    cookie.user.iconUrl = response.user.iconUrl;
    cookie.user.schemaString = response.user.schemaString;
    cookie.theme = currentTheme || Util.DEFAULT_THEME();
    cookie.langKey = langKey || 'english';
    this.setCookie(Constants.USER_COOKIE_KEY, cookie, Constants.USER_COOKIE_REFRESH_DELAY);
  }

  /*
  refreshCookieContinuously(appManager: AppManagerService) {
    clearInterval(this.listenForCookieId);
    let that = this;
    this.listenForCookieId = setInterval(that.listenForCookieContinuously.bind(this, appManager), 10000)
  }*/

  /*
  listenForCookieContinuously = (appManager: AppManagerService) => {

    if (appManager) {
      this.updateCookie(appManager.currentTheme);
    } else {
      clearInterval(this.listenForCookieId);
    }
  }*/

  deleteAll(url: string) {
    this.cookieService.deleteAll(url);
  }

  private hasDsgvoCookie(): boolean {
    return this.cookieService.check(Constants.DSGVO_COOKIE_KEY)
  }

  public setConfirmDSGVO() {
    this.setDSGVOCookie();
  }

  public setDiscardDSGVO() {
    this.cookieService.delete(Constants.DSGVO_COOKIE_KEY);

  }

}

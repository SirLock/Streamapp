import {VideoService} from './video.service';
import {Logger} from '../helper/logger';
import {ConfigService} from './config.service';
import {StoreService} from './store.service';
import {DataTransferContainer} from '../models/datatransfercontainer.model';
import {DataApiManagerService} from './data-api-manager.service';
import {PlaylistService} from '../playlist.service';
import {Constants} from '../helper/constants';
import {SearchResult} from '../models/searchresult.model';
import {IDataApi} from '../models/idataapi';
import {Util} from '../helper/util';
import {Stream2getherComponent} from '../stream2gether/stream2gether.component';
import {User} from '../models/user.model';
import {IVideoPlayerApi} from '../videoapis/ivideoplayerapi';
import {ChatMessage} from '../models/chatmessage.model';
import {Theme} from '../models/theme.model';
import {Video} from '../models/video.model';
import {VideoPlayerAction} from '../models/videoplayeraction.model';
import {CookieManagerService} from './cookiemanager.service';
import {LanguageService} from "../language.service";
import {S9toastrService} from "../s9toastr.service";
import {TextgeneratorService} from "../textgenerator.service";
import {gsap, TweenMax, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin, TimelineLite} from "gsap/all";
import {ResponsiveService} from "../responsive.service";
import {Injectable} from "@angular/core";

gsap.registerPlugin(MorphSVGPlugin, MotionPathPlugin);

@Injectable({
  providedIn: 'root'
})
export class AppManagerService {

  private revealContent_: boolean = false;

  get revealContent(): boolean {
    return this.revealContent_;
  }

  set revealContent(val: boolean) {
    this.revealContent_ = val;
  }

  private _fallbackMode: boolean = false;

  get fallbackMode(): boolean {
    return this._fallbackMode;
  }

  set fallbackMode(value: boolean) {
    this._fallbackMode = value;
  }

  private _currentSearchInput: string;
  private _currentDisplayPlaylistName: string;

  get currentSearchInput(): string {
    return this._currentSearchInput;
  }

  set currentSearchInput(value: string) {
    this._currentSearchInput = value;
  }

  get currentDisplayPlaylistName(): string {
    return this._currentDisplayPlaylistName;
  }

  set currentDisplayPlaylistName(value: string) {
    this._currentDisplayPlaylistName = value;
  }

  private _currentSearchResultsIndex: number = 0;

  get currentSearchResultsIndex(): number {
    return this._currentSearchResultsIndex;
  }

  set currentSearchResultsIndex(value: number) {
    this._currentSearchResultsIndex = value;
  }


  public mobileControlStates = ['search', 'playlist', 'chat', 'userlist', 'history'];
  private _previousControlStateIndex = -1;
  private _currentTabIndex = 0;

  get previousControlStateIndex(): number {
    return this._previousControlStateIndex;
  }

  set previousControlStateIndex(value: number) {
    this._previousControlStateIndex = value;
  }

  get currentTabIndex(): number {
    return this._currentTabIndex;
  }

  set currentTabIndex(value: number) {
    this._currentTabIndex = value;
  }

  get controlState(): string {
    return this.mobileControlStates[this.currentTabIndex];
  }

  // Icon Editor
  private displayIconEditor_: boolean = false;
  private displayGrid_: boolean = false;

  get displayGrid(): boolean {
    return this.displayGrid_;
  }

  set displayGrid(val: boolean) {
    this.displayGrid_ = val;
  }

  get displayIconEditor(): boolean {
    return this.displayIconEditor_;
  }

  set displayIconEditor(val: boolean) {
    this.displayIconEditor_ = val;
  }

  /*SERVICES */
  private videoService_: VideoService;
  private dataApiManager_: DataApiManagerService;
  private playlistService_: PlaylistService;

  s2gComponent: Stream2getherComponent;

  private pipActive_: boolean = false;

  private chatToggle_: boolean = false;
  private leftTime_: number;
  private initLeftTime_: number;

  private unseenMsgCounter_: number = 0;

  private nextVideoToastrFired_: boolean = false;
  private displayNextVideoPreview_: boolean = false;

  private topbarDocked_: boolean = false;

  private displayScrollBackToVideo_: boolean = false;
  private displaySearchResultOverlay_: boolean = false;

  private _userListOpened: boolean = false;
  _closed: boolean = true;


  currentTheme_: Theme = Util.DEFAULT_THEME();

  get closed(): boolean {
    return this._closed;
  }

  set closed(value: boolean) {
    this._closed = value;
  }

  get userListOpened(): boolean {
    return this._userListOpened;
  }

  set userListOpened(value: boolean) {
    this._userListOpened = value;
  }

  get displaySearchResultOverlay(): boolean {
    return this.displaySearchResultOverlay_;
  }

  set displaySearchResultOverlay(val: boolean) {
    this.displaySearchResultOverlay_ = val;
  }

  get pipActive(): boolean {
    return this.pipActive_;
  }

  set pipActive(val: boolean) {
    this.pipActive_ = val;
  }

  get displayScrollBackToVideo(): boolean {
    return this.displayScrollBackToVideo_;
  }

  set displayScrollBackToVideo(val: boolean) {
    this.displayScrollBackToVideo_ = val;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  set roomId(val: string) {
    this.store.roomId = val;
  }

  get currentTheme(): Theme {
    return this.currentTheme_;
  }

  set currentTheme(val: Theme) {
    this.currentTheme_ = val;
  }

  get topbarDocked(): boolean {
    return this.topbarDocked_;
  }

  set topbarDocked(val: boolean) {
    this.topbarDocked_ = val;
  }

  get leftTime(): number {
    return this.leftTime_;
  }

  set leftTime(val: number) {
    this.leftTime_ = val;
  }

  get initLeftTime(): number {
    return this.initLeftTime_;
  }

  set initLeftTime(val: number) {
    this.initLeftTime_ = val;
  }

  get chatToggle(): boolean {
    return this.chatToggle_;
  }

  set chatToggle(val: boolean) {
    this.chatToggle_ = val;
  }

  get unseenMsgCounter(): number {
    return this.unseenMsgCounter_;
  }

  set unseenMsgCounter(val: number) {
    this.unseenMsgCounter_ = val;
  }

  get videoService(): VideoService {
    return this.videoService_;
  }

  set videoService(val: VideoService) {
    this.videoService_ = val;
  }

  get dataApiManager(): DataApiManagerService {
    return this.dataApiManager_;
  }

  set dataApiManager(val: DataApiManagerService) {
    this.dataApiManager_ = val;
  }

  get playlistService(): PlaylistService {
    return this.playlistService_;
  }

  set playlistService(val: PlaylistService) {
    this.playlistService_ = val;
  }

  get displayNextVideoPreview(): boolean {
    return this.displayNextVideoPreview_;
  }

  set displayNextVideoPreview(val: boolean) {
    this.displayNextVideoPreview_ = val;
  }

  get nextVideoToastrFired(): boolean {
    return this.nextVideoToastrFired_;
  }

  set nextVideoToastrFired(val: boolean) {
    this.nextVideoToastrFired_ = val;
  }

  get displayPlaylistImportOptions(): boolean {
    return this.dataApiManager.displayPlaylistImportOptions;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  get selectedPlayer(): IVideoPlayerApi {
    return this.videoService ? this.videoService.selectedPlayer : null;
  }

  get resolvedScreenMode(): string {
    return this.responsiveService.resolvedScreenMode;
  }

  latestUnseenChatMessageId: string;


  registerS2GComponent(s2gComponent: Stream2getherComponent) {
    this.s2gComponent = s2gComponent;
  }

  constructor(private configService: ConfigService,
              private store: StoreService,
              private languageService: LanguageService,
              private cookieManager: CookieManagerService,
              private toastr: S9toastrService,
              private textGenerator: TextgeneratorService,
              private responsiveService: ResponsiveService) {

    this.initListenForVideoEnd();
  }

  initApp() {
    this.videoService.requestAndHandleInitalVideoPlayerSettings();
    this.playlistService.requestAndHandleInitilPlaylistState();
  }

  handleWebsocketResponse = (serverResponse) => {
    let dtc: DataTransferContainer = JSON.parse(serverResponse.body);
    this.decideWhatToDo(dtc);
  };

  addLocalUserWelcomeNotification(name: string) {
    const chatMessage: ChatMessage = this.textGenerator.welcomeLocalUserChatMessage(name);
    this.toastr.toast('', chatMessage.body, 7000, 'welcome');
    this.processNewChatMessage(chatMessage);
  }

  addUserJoinedNotification(name: string) {
    const chatMessage: ChatMessage = this.textGenerator.userJoinedChatMessage(name);
    this.toastr.toast('', chatMessage.body, 7000, 'welcome');
    this.processNewChatMessage(chatMessage);
  }


  addDisconnectMessage(name: string) {
    //TODO USE TEXTgenerator ?
    let chatMessage: ChatMessage = Util.generateLeftRoomChatMessage(name);
    //this.scrollToChatBottom(); //maybe insert the goodbyechatmessage here
  }

  chatIsClosed_Desktop(): boolean {
    return (this.resolvedScreenMode !== this.responsiveService._mobile && (!this.chatToggle || (this.chatToggle && this.userListOpened)));
  }

  chatIsClosed_Mobile(): boolean {
    return (this.currentTabIndex !== 2 && this.resolvedScreenMode === this.responsiveService._mobile);
  }


  processNewChatMessage(chatMessage: ChatMessage) {
    this.store.addSingleChatMessage(chatMessage);
    if (this.chatIsClosed_Desktop() || this.chatIsClosed_Mobile()) {
      this.unseenMsgCounter += 1;
      if (!this.latestUnseenChatMessageId) {
        this.latestUnseenChatMessageId = chatMessage.id;
      }
    } else {
      this.latestUnseenChatMessageId = null;
      this.scrollToChatElementOrBottom();
    }
  }

  decideWhatToDo(dtc: DataTransferContainer) {
    switch (dtc.purpose) {
      case Constants.PURPOSE_DISCONNECTED_CLIENT: {
        let userName = this.store.resolveUserIdToName(dtc.from);
        this.addDisconnectMessage(userName);
        this.store.removeUser(dtc.from);
      }
        break;
      case Constants.PURPOSE_SEND_CHATMESSAGE: {
        this.processNewChatMessage(dtc.chatMessage);
      }
        break;
      case Constants.PURPOSE_USER_JOINED: {
        this.addUserJoinedNotification(dtc.user.name);
        this.store.updatedUsers(dtc.users);
        // this.store.users.set(dtc.user.id, dtc.user); // TODO
      }
        break;
      case Constants.PURPOSE_ASSIGN_TO_DESIGNATED: {
        if (this.localUser.id === dtc.user.id) {
          this.localUser = dtc.user;
        }
      }
        break;
      case Constants.PURPOSE_UPDATE_USERNAME: {
        if (this.localUser.id === dtc.user.id) {
          this.localUser.name = dtc.user.name;
          this.cookieManager.updateCookie(this.currentTheme);
        } else {
          let user = this.store.users.get(dtc.user.id);
          user.name = dtc.user.name;
        }
      }
        break;
      case Constants.PURPOSE_UPDATE_USERICON: {
        if (this.localUser.id === dtc.user.id) {
          this.localUser.iconUrl = dtc.user.iconUrl;
          this.localUser.schemaString = dtc.user.schemaString;
          this.cookieManager.updateCookie(this.currentTheme);
        } else {
          let user = this.store.users.get(dtc.user.id);
          user.iconUrl = dtc.user.iconUrl;
        }
      }
        break;
    }

  }

  scrollToChatElement() {
    let that = this;
    setTimeout(function () {
      let chatMessageElem: HTMLElement = document.getElementById(that.latestUnseenChatMessageId);
      if (chatMessageElem) {
        that.latestUnseenChatMessageId = null;
        chatMessageElem.scrollIntoView({block: 'nearest', inline: 'nearest'});
      }
    }, 250);
    return;
  }

  scrollToChatBottom() {
    setTimeout(function () {
      let chatBoxElem = document.getElementById('chatBox');
      if (chatBoxElem) {
        chatBoxElem.scrollTop = chatBoxElem.scrollHeight;
      }
    }, 250);
  }

  scrollToChatElementOrBottom() {
    if (this.latestUnseenChatMessageId) {
      this.scrollToChatElement();
    } else {
      this.scrollToChatBottom();
    }
  }

  importPlaylistFromPreview(searchResult: SearchResult) {
    if (!this.displayPlaylistImportOptions) {
      this.displaySearchResultOverlay = true;
      this.dataApiManager.importFromPreview = true;
      let neededDataApi: IDataApi = this.dataApiManager.dataApis.get(searchResult.api);
      neededDataApi.sendPlaylistRequestToBackend(searchResult.playlistId)
        .subscribe(response => this.handleImportFromPreviewReponse(response, neededDataApi));
    }
  }

  tempImportingPlaylist: SearchResult[] = [];

  handleImportFromPreviewReponse(previousReponse, neededDataApi: IDataApi) {
    {
      //add latest received playlist video to list
      this.tempImportingPlaylist = [
        ...this.tempImportingPlaylist, ...previousReponse.items
          .map(item => neededDataApi.mapRequestedPlaylistItemToSearchResult(item))];

      //if there is a nextPageToken we request the next batch of videos until there is not nextPageToken
      if (previousReponse.nextPageToken) {
        neededDataApi.sendPlaylistRequestToBackend(previousReponse.items[0].snippet.playlistId, previousReponse.nextPageToken)
          .subscribe(nextResponse => this.handleImportFromPreviewReponse(nextResponse, neededDataApi));
        return;
      }

      //Playlist loaded -> trigger import
      this.playlistService.sendImportPlaylistCommand(this.tempImportingPlaylist.map(searchRes => Util.mapToVideo(searchRes)));
      this.tempImportingPlaylist = [];
    }
  }

  handleAdditionalAction(videoPlayerAction: VideoPlayerAction) {
    switch (videoPlayerAction.additionalAction) {
      case Constants.ACTION_ADD_TO_HISTORY:
        this.addVideoToHistory(videoPlayerAction.videoPlayerSettings.video);
        break;
    }
  }

  /**
   * add video to history
   */
  addVideoToHistory(video: Video) {
    this.store.history = [video, ...this.store.history];
  }


  listenForVideoEnd = () => {

    if (this.selectedPlayer && this.selectedPlayer.getPlayerState() === Constants.PLAYING && this.selectedPlayer.getVideoDuration() > 0) {
      this.leftTime = this.selectedPlayer.getVideoDuration() - this.selectedPlayer.getCurrentTime();
      if (this.leftTime <= Constants.NEXT_VIDEO_TOASTR_TIME_THRESHOLD_MAX && !this.nextVideoToastrFired && this.videoService.nextVideo) {
        // fire toastr
        this.initLeftTime = this.leftTime;
        this.displayNextVideoPreview = true;
        this.nextVideoToastrFired = true;
      } else if (this.leftTime > Constants.NEXT_VIDEO_TOASTR_TIME_THRESHOLD_MAX) {
        this.nextVideoToastrFired = false;
        this.displayNextVideoPreview = false;
      }
    }
  }

  initListenForVideoEnd() {
    setInterval(this.listenForVideoEnd, 100);
  }

  displayLatestSearchResults() {
    //this.dataApiManager.displayLatestSearchResults()
  }

  updateCurrentVideoDataIfNeeded(video: Video, from: string) {
    let that = this;
    const waitForVideoData = setInterval(() => {
      const data = this.selectedPlayer.getVideoData();
      if (video && data && data.title && data.author) {
        video.title = data.title;
        video.channelTitle = data.author;
        clearInterval(waitForVideoData);
        console.warn("CLEARED WAIT VIDEO DATA")
        that.updateLocalPlaylistDataFromVideoPlayerData(video);
        if (from === that.localUser.id) {
          that.updateLocalSearchResultDataFromVideoPlayerData(video)
          that.notifyBackendAboutVideoDataUpdate(video);
        }
      }
    }, 10)
  }

  notifyBackendAboutVideoDataUpdate(video: Video) {
    this.dataApiManager.notifyBackendAboutVideoDataUpdate(video);
  }

  updateLocalSearchResultDataFromVideoPlayerData(video: Video) {
    this.dataApiManager.updateSearchResultDataFromPlayer(video);
  }

  updateLocalPlaylistDataFromVideoPlayerData(video: Video) {
    this.store.updateLocalPlaylistDataFromVideoPlayer(video);
  }

  updateFallbackMode(fallbackMode: boolean) {
    if (this.fallbackMode !== fallbackMode) {
      this.fallbackMode = fallbackMode;
    }
  }

  /**
   * function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

   function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
   </script>
   */
  MobileNavStatus;

  openSideNav() {
    gsap.to("#mobileSidenav", 0.4, {ease: "power2.out", x: "-50vw"});
    gsap.to("#app-main", 0.4, {ease: "power2.out", x: "-50vw"});
    gsap.fromTo("#close", 0.3, {opacity: "0"}, {delay: 0.2, opacity: "1"});
    gsap.fromTo("#opennav", 0.2, {opacity: "1"}, {opacity: "0"});
    gsap.to("#logo", 0.4, {ease: "power2.out", x: "50vw"});
    this.MobileNavStatus = "open";
  }

  closeSideNav() {
    gsap.to("#mobileSidenav", 0.3, {ease: "power2.in", x: "0"});
    gsap.to("#app-main", 0.3, {ease: "power2.in", x: "0"});
    gsap.fromTo("#close", 0.3, {opacity: "1"}, {opacity: "0"});
    gsap.fromTo("#opennav", 0.2, {opacity: "0"}, {delay: 0.2, opacity: "1"});
    gsap.to("#logo", 0.3, {ease: "power2.in", x: "0"});
    this.MobileNavStatus = "closed";
  }

  elementInViewport(elem) {
    const bounding = elem.getBoundingClientRect();
    return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  scrollToAppBottom() {
    document.documentElement.scroll({top: document.documentElement.scrollHeight})
  }

}

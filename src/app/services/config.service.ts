import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { Util } from '../helper/util';
import { Logger } from '../helper/logger';
import {Constants} from "../helper/constants";
import { isDevMode } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  CONFIG_: any;

  get CONFIG(): any {
    return this.CONFIG_;
  }
  set CONFIG(val: any) {
    this.CONFIG_ = val;
  }

  constructor(private restService: RestService) {
    this.readConfig();
  }

  readConfig() {
    if(isDevMode()) {
      //uses devConfig if found otherwise defaultConfig
      this.tryLoadDevConfig();
    }else{
      this.loadDefaultConfig();
    }
  }

  addConfig = config => {
    Logger.success("config been loaded -> " + config.name);
    this.CONFIG = config;
    Util.loggingActive = config.app.loggingActive;
    this.setConstants(config);
  }

  noDevConfigFound = error => {
    Logger.warn("no dev config found. load default config.");
    this.loadDefaultConfig();
  }

  tryLoadDevConfig() {
    this.restService.fromFile("assets/config/devConfig.json")
      .subscribe(this.addConfig, this.noDevConfigFound)
  }

  loadDefaultConfig() {
    this.restService.fromFile("assets/config/defaultConfig.json")
      .subscribe(this.addConfig, error => Logger.error('no config found at all. bad.'))
  }

  setConstants(config: any) {
    Constants.BACKEND_BASE_URL = config.app.backendBaseUrl;

    //for saftey because in devMode there is only localhost for frontend
    if(isDevMode()) {
      Constants.FRONTEND_URL_POSTFIX = Constants.LOCALHOST_FRONTEND_POSTFIX;
    }else{
      Constants.FRONTEND_URL_POSTFIX = config.app.urlPostFix;
    }

    Constants.CONFIG = config;
    Constants.configLoaded = true;
  }

}

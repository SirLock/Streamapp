import { TestBed } from '@angular/core/testing';

import { DataApiManagerService } from './data-api-manager.service';

describe('DataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataApiManagerService = TestBed.get(DataApiManagerService);
    expect(service).toBeTruthy();
  });
});

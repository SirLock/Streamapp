import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from "@angular/common/http";
import { Observable } from 'rxjs';
import { Constants } from '../helper/constants';
import { DataTransferContainer } from '../models/datatransfercontainer.model';
import { PlaylistCommand } from '../models/playlistcommand.model';
import { RequestParam } from '../models/requestparam.model';
import {Video} from "../models/video.model";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private httpHeaders: HttpHeaders = new HttpHeaders().set("Content-Type", "application/json");

  constructor(private http: HttpClient) { }

  GET(urlPostFix: string): Observable<any> {
    return this.http.get(Constants.REST_BASE_URL + urlPostFix, { headers: this.httpHeaders });
  }

  GET_PUBLIC(url: string): Observable<any> {
    return this.http.get(url, { headers: this.httpHeaders });
  }

  POST(urlPostFix: string, requestContainer: DataTransferContainer | PlaylistCommand | RequestParam[] | Video): Observable<any> {
    return this.http.post(Constants.REST_BASE_URL + urlPostFix, requestContainer, { headers: this.httpHeaders });
  }

  fromFile(path: string): Observable<any> {
    return this.http.get(path);
  }

}

import { Injectable } from '@angular/core';
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";
import { Constants } from '../helper/constants';
import { DataTransferContainer } from '../models/datatransfercontainer.model';
import { Logger } from '../helper/logger';
import { AppManagerService } from './appmanager.service';
import { StoreService } from './store.service';
import { User } from '../models/user.model';
import { VideoPlayerAction } from '../models/videoplayeraction.model';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {


  private ws: SockJS;
  private stompClient;

  get localUser(): User {
    return this.store.localUser;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  constructor(private appManager: AppManagerService, private store: StoreService) { }

  connect() {
    this.ws = new SockJS(Constants.BACKEND_BASE_URL + "/socket");
    this.stompClient = Stomp.over(this.ws);
    let that = this;
    this.stompClient.connect({}, function () {
      Logger.success("Websocket successfully connected");
      that.subscribeToWebSocketEndpoints();
      that.appManager.initApp();
      that.appManager.revealContent = true;
      if(that.appManager.resolvedScreenMode === 'mobile') {
        that.appManager.scrollToAppBottom();
      }
    });
  }

  private subscribeToWebSocketEndpoints() {
    let that = this;
    let waitForServices = setInterval(function () {
      if (that.appManager.playlistService && that.appManager.videoService && that.localUser) {
        that.stompClient.subscribe("/client/" + that.localUser.id, that.appManager.handleWebsocketResponse);
        that.stompClient.subscribe("/client/" + that.localUser.id + "/playlist", that.appManager.playlistService.handlePlaylistCommandResponse);
        that.stompClient.subscribe("/client/" + that.localUser.id + "/video-player-command", that.appManager.videoService.handleVideoPlayerActionResponse);
        clearInterval(waitForServices);
      }
    }, 1);
  }

  sendWebsocketRequest(urlPostFix: string, requestContainer: DataTransferContainer) {
    this.stompClient.send(
      Constants.WEBSOCKET_URL_POSTFIX + urlPostFix,
      {},
      JSON.stringify(requestContainer)
    );
  }


  sendWebsocketSyncRequest(urlPostFix: string, videoPlayerAction: VideoPlayerAction) {
    this.stompClient.send(
      Constants.WEBSOCKET_URL_POSTFIX + urlPostFix,
      {},
      JSON.stringify(videoPlayerAction)
    );
  }

  closeWebsocket() {
    this.stompClient.disconnect();
    this.ws.close();
  }
}

import { TestBed } from '@angular/core/testing';

import { CookieManagerService } from './cookiemanager.service';

describe('CookiemanagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CookieManagerService = TestBed.get(CookieManagerService);
    expect(service).toBeTruthy();
  });
});

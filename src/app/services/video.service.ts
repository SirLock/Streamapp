import {Injectable} from '@angular/core';
import {AppManagerService} from './appmanager.service';
import {IVideoPlayerApi} from '../videoapis/ivideoplayerapi';
import {RestService} from './rest.service';
import {YoutubeVideoPlayer} from '../videoapis/youtube.videoplayerapi';
import {Logger} from '../helper/logger';
import {ConfigService} from './config.service';
import {Util} from '../helper/util';
import {WebsocketService} from './websocket.service';
import {IService} from './iservice.model';
import {StoreService} from './store.service';
import {User} from '../models/user.model';
import {VideoPlayerSettings} from '../models/videoplayersettings.model';
import {VideoPlayerAction} from '../models/videoplayeraction.model';
import {Constants} from '../helper/constants';
import {Observable} from 'rxjs';
import {DirectVideoPlayer} from '../models/directvideoplayer.model';
import {Video} from '../models/video.model';
import {VideoPlayerFactory} from '../models/videoplayerfactory';


@Injectable({
  providedIn: 'root'
})
export class VideoService implements IService {

  public currentVideoPlayerSettings: VideoPlayerSettings;
  private playersLoaded: Map<string, boolean> = new Map<string, boolean>();
  private players_: Map<string, IVideoPlayerApi> = new Map<string, IVideoPlayerApi>();
  private selectedPlayerId_: string;
  private nextVideo_: Video;

  register() {
    this.appManager.videoService = this;
  }

  get nextVideo(): Video {
    return this.nextVideo_;
  }

  set nextVideo(val: Video) {
    this.nextVideo_ = val;
  }

  get roomId(): string {
    return this.store.roomId;
  }

  get localUser(): User {
    return this.store.localUser;
  }

  get history(): Video[] {
    return this.store.history;
  }

  set history(val: Video[]) {
    this.store.history = val;
  }

  get users(): User[] {
    return [...this.store.users.values()];
  }

  get players(): Map<string, IVideoPlayerApi> {
    return this.players_;
  }

  set players(val: Map<string, IVideoPlayerApi>) {
    this.players_ = val;
  }

  get selectedPlayerId(): string {
    return this.selectedPlayerId_;
  }

  set selectedPlayerId(val: string) {
    this.selectedPlayerId_ = val;
  }

  get selectedPlayer(): IVideoPlayerApi {
    return this.players.get(this.selectedPlayerId);
  }

  get playlist(): Video[] {
    return this.store.playlist;
  }

  isANewVideo(previousVideoPlayerSettings: VideoPlayerSettings, receivedVideoPlayerSettings: VideoPlayerSettings) {
    return !previousVideoPlayerSettings || !previousVideoPlayerSettings.video || previousVideoPlayerSettings.video.id !== receivedVideoPlayerSettings.video.id
  }

  isReady(apiId: string) {
    this.playersLoaded.set(apiId, true);
  }

  /**
   * returns true or false if video players expected to be built are initialized
   */
  get areAllPlayersReady(): boolean {
    for (let apiId of this.playersLoaded.keys()) {
      let isNotReady = !this.playersLoaded.get(apiId);
      if (isNotReady) {
        return false;
      }
    }
    return true;
  }


  constructor(private appManager: AppManagerService, private restService: RestService, private websocket: WebsocketService, private configService: ConfigService, private store: StoreService) {
    this.register();


  }

  public requestAndHandleInitalVideoPlayerSettings() {
    this.requestInitialVideoPlayerSettings()
      .subscribe(this.handleInitialVideoPlayerSettings);
  }

  /**
   * request & handle initial VideoPlayerSettings
   */
  requestInitialVideoPlayerSettings(): Observable<any> {
    return this.restService.GET("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/player-settings")
  }

  /**
   * handles the initially received VideoPlayerSettings
   */
  handleInitialVideoPlayerSettings = (videoPlayerSettings: VideoPlayerSettings) => {
    let that = this;
    let waitForVideoPlayers = setInterval(function () {
      Logger.log('waiting for video players to be ready...');
      if (that.areAllPlayersReady) {
        Logger.success("number of generated video apis: " + that.players.size);
        let initialVideoPlayerAction = new VideoPlayerAction();
        initialVideoPlayerAction.from = "SYSTEM";
        initialVideoPlayerAction.videoPlayerSettings = videoPlayerSettings;
        that.processVideoPlayerSettings(initialVideoPlayerAction);
        that.requestInitialSyncIfPossible();
        clearInterval(waitForVideoPlayers);
      }
    }, 10)
  };

  /**
   * handle VideoPlayerAction responses and process incoming VideoPlayerSettings
   */
  handleVideoPlayerActionResponse = (response: any) => {
    let videoPlayerAction: VideoPlayerAction = JSON.parse(response.body);
    Logger.success("received VideoPlayerAction: " + videoPlayerAction.videoPlayerSettings.state)
    if (videoPlayerAction.videoPlayerSettings.state === Constants.REQUEST_SYNC) {
      this.respondToJoinSyncRequest();
    } else {
      this.appManager.updateFallbackMode(videoPlayerAction.fallbackMode);
      this.processVideoPlayerSettings(videoPlayerAction);
    }
  };

  /**
   * sets currentVideoPlayerSettings, updates selectedPlayer and its visibility
   * @param receivedVideoPlayerSettings
   */
  updateCurrentVideoApiAndVideoSettings(receivedVideoPlayerSettings: VideoPlayerSettings) {
    this.hideCurrentPlayerIfNeeded(receivedVideoPlayerSettings);
    this.setCurrentVideoPlayerSettings(receivedVideoPlayerSettings);
    this.revealCurrentPlayer();
  }

  setCurrentVideoPlayerSettings(receivedVideoPlayerSettings: VideoPlayerSettings) {
    this.currentVideoPlayerSettings = receivedVideoPlayerSettings;
    this.selectedPlayerId = receivedVideoPlayerSettings.api;
  }

  hideCurrentPlayerIfNeeded(receivedVideoPlayerSettings: VideoPlayerSettings) {
    if (this.currentVideoPlayerSettings && this.currentVideoPlayerSettings.api != receivedVideoPlayerSettings.api) {
      this.selectedPlayer.setVisible(false);
    }
  }

  revealCurrentPlayer() {
    if (this.selectedPlayer) {
      this.selectedPlayer.setVisible(true);
    }
  }

  /**
   * if the current video is switched to another the function loads the video in the correct state
   * @param receivedVideoPlayerSettings
   */
  handleNewVideo(receivedVideoPlayerSettings: VideoPlayerSettings) {
    if (receivedVideoPlayerSettings.state == Constants.PAUSED) {
      this.selectedPlayer.cueVideoById(receivedVideoPlayerSettings.video.videoId, receivedVideoPlayerSettings.timestamp);
    } else {
      this.selectedPlayer.loadVideoById(receivedVideoPlayerSettings.video.videoId, receivedVideoPlayerSettings.timestamp);
    }
  }

  /**
   * consumes received videoplayersettings and decides how to process these
   * @param receivedVideoPlayerSettings
   */
  processVideoPlayerSettings(receivedVideoPlayerAction: VideoPlayerAction) {

    this.handleAdditionalAction(receivedVideoPlayerAction);

    let receivedVideoPlayerSettings: VideoPlayerSettings = receivedVideoPlayerAction.videoPlayerSettings;
    let previousVideoPlayerSettings: VideoPlayerSettings = this.currentVideoPlayerSettings;
    this.updateCurrentVideoApiAndVideoSettings(receivedVideoPlayerSettings)

    if (receivedVideoPlayerSettings.timestamp === 0) {
      this.appManager.nextVideoToastrFired = false;
    }

    if(receivedVideoPlayerSettings.api === 'youtube') {
      this.processYoutubeSettings(receivedVideoPlayerSettings, previousVideoPlayerSettings)
    }

    if(receivedVideoPlayerSettings.api === 'direct-video') {
      this.processDirectVideoSettings(receivedVideoPlayerSettings, previousVideoPlayerSettings);
    }
  }

  processYoutubeSettings(receivedVideoPlayerSettings: VideoPlayerSettings, previousVideoPlayerSettings: VideoPlayerSettings) {
    // --> NEW VIDEO
    if (this.isANewVideo(previousVideoPlayerSettings, receivedVideoPlayerSettings)) {
      this.handleNewVideo(receivedVideoPlayerSettings);
      this.requestNextVideoFromServer();
      if (!previousVideoPlayerSettings || receivedVideoPlayerSettings.playbackRate != previousVideoPlayerSettings.playbackRate) {
        this.processPlaybackRateDelayed(receivedVideoPlayerSettings);
      }
      return;
    }

    // --> PLAYBACKRATE CHANGE
    if (receivedVideoPlayerSettings.playbackRate != previousVideoPlayerSettings.playbackRate) {
      this.selectedPlayer.setCurrentPlaybackRate(receivedVideoPlayerSettings.playbackRate);
      return;
    }

    // --> PAUSE OR PLAY
    if (receivedVideoPlayerSettings.state == Constants.PLAYING) {
      if (receivedVideoPlayerSettings.state === previousVideoPlayerSettings.state) {
        this.handleNewVideo(receivedVideoPlayerSettings);
      } else {
        this.selectedPlayer.playVideo();
        this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
      }
    } else if (receivedVideoPlayerSettings.state == Constants.PAUSED) {
      if (receivedVideoPlayerSettings.state === previousVideoPlayerSettings.state) {
        this.handleNewVideo(receivedVideoPlayerSettings);
      } else {
        this.selectedPlayer.pauseVideo();
        this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
      }
    } else {
      Logger.warn("EXOTIC PLAYER STATE RECEIVED: " + receivedVideoPlayerSettings.state);
      this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
    }
  }

  processDirectVideoSettings(receivedVideoPlayerSettings: VideoPlayerSettings, previousVideoPlayerSettings: VideoPlayerSettings) {
    if (this.isANewVideo(previousVideoPlayerSettings, receivedVideoPlayerSettings)) {
      this.selectedPlayer.loadVideoById(receivedVideoPlayerSettings.video.videoId, receivedVideoPlayerSettings.timestamp);
      if (!previousVideoPlayerSettings || receivedVideoPlayerSettings.playbackRate != previousVideoPlayerSettings.playbackRate) {
        this.processPlaybackRateDelayed(receivedVideoPlayerSettings);
      }
      if(receivedVideoPlayerSettings.state === Constants.PLAYING) {
        this.selectedPlayer.playVideo();
      }else if(receivedVideoPlayerSettings.state == Constants.PAUSED){
        this.selectedPlayer.pauseVideo();
      }
      return;
    }

    // --> PLAYBACKRATE CHANGE
    if (receivedVideoPlayerSettings.playbackRate != previousVideoPlayerSettings.playbackRate) {
      this.selectedPlayer.setCurrentPlaybackRate(receivedVideoPlayerSettings.playbackRate);
      return;
    }

    // --> PAUSE OR PLAY
    if (receivedVideoPlayerSettings.state == Constants.PLAYING) {
      this.selectedPlayer.playVideo();
      this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
    } else if (receivedVideoPlayerSettings.state == Constants.PAUSED) {
      this.selectedPlayer.pauseVideo();
      this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
    } else {
      Logger.warn("EXOTIC PLAYER STATE RECEIVED: " + receivedVideoPlayerSettings.state);
      this.selectedPlayer.seekTo(receivedVideoPlayerSettings.timestamp);
    }
  }

  processPlaybackRateDelayed(receivedVideoPlayerSettings: VideoPlayerSettings) {
    let that = this;
    setTimeout(function () {
      that.selectedPlayer.setCurrentPlaybackRate(receivedVideoPlayerSettings.playbackRate);
    }, 250);
  }

  handleAdditionalAction(videoPlayerAction: VideoPlayerAction) {
    this.appManager.handleAdditionalAction(videoPlayerAction);
  }

  requestNextVideoFromServer() {
    Logger.success("[Request Next Video]");
    this.restService.GET("roomId/" + this.roomId + "/userId/" + this.localUser.id + "/next-Video")
      .subscribe(nextVideoResponse => {
        this.nextVideo = nextVideoResponse;
        Logger.success("[Received Next Video]: " + (this.nextVideo ? this.nextVideo.title : 'Empty'));
      });
  }

  /**
   * handles the change of state in the local player
   */
  handlePlayerEvent = (data) => {
    let currentState: number = data;
    Logger.success("PLAYER EVENT: " + currentState);
    // EXIT ON USELESS STATES
    if (currentState === Constants.BUFFERING || currentState === Constants.PLACED || currentState === Constants.NOTSTARTED) {
      return;
    }

    // REACT ON VIDEO FINISHED
    if (currentState == Constants.FINISHED) {
      this.appManager.displayNextVideoPreview = false;
      if (this.localUser.designated && this.nextVideo) {
        this.fireNextVideoRequest();
      }
      return;
    }

    // REACT ON LOCALLY TRIGGERED PLAYERSTATE CHANGE
    if (currentState != this.currentVideoPlayerSettings.state) {
      //this.updateCurrentVideoDataIfNeeded();
      this.sendSyncRequest(currentState, this.currentVideoPlayerSettings.playbackRate);
      this.currentVideoPlayerSettings.state = currentState;
      return;
    }
  };


  /**
   * requests next video from backend for all clients
   */
  fireNextVideoRequest() {
    Logger.success("SYNC UPDATE CURRENT VIDEO");
    this.appManager.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_NEXT, null, -1)
  }

  /**
   * handles a change of playback rate in the local player
   */
  handlePlaybackRateChange = (data) => {
    let changedPlaybackRate: number = data;
    Logger.warn('changedPlaybackRate: ' + changedPlaybackRate);
    if (this.currentVideoPlayerSettings.playbackRate !== changedPlaybackRate) {
      this.currentVideoPlayerSettings.playbackRate = changedPlaybackRate;
      this.sendSyncRequest(this.currentVideoPlayerSettings.state, this.currentVideoPlayerSettings.playbackRate);
    }
  };

  handlePlayerError(event) {
    //TODO
    /*2 – Die Anfrage enthält einen ungültigen Parameterwert. Dieser Fehler tritt beispielsweise auf, wenn du eine Video-ID angibst, die nicht aus elf Zeichen besteht, oder wenn die Video-ID ungültige Zeichen wie z. B. Ausrufezeichen oder Sternchen enthält.
      5 – Der angeforderte Inhalt kann nicht mit einem HTML5-Player wiedergegeben werden oder es ist ein anderer Fehler im Zusammenhang mit dem HTML5-Player aufgetreten.
      100 – Der angeforderte Video wurde nicht gefunden. Dieser Fehler tritt auf, wenn ein Video aus irgendeinem Grund entfernt oder als privat markiert wurde.
      101 – Der Rechteinhaber des angeforderten Videos untersagt die Wiedergabe des Videos in eingebetteten Playern.
      150 – Dieser Fehler ist mit 101 identisch. Es handelt sich eigentlich um einen 101-Fehler.*/
  }

  /**
   * sends sync requests
   */
  sendSyncRequest(state: number, playbackRate: number) {
    if (this.selectedPlayer) {
      let videoPlayerAction: VideoPlayerAction = VideoPlayerFactory.buildVideoPlayerAction(state, this.selectedPlayer.getCurrentTime(), playbackRate, this.localUser, this.selectedPlayerId, this.currentVideoPlayerSettings);
      this.websocket.sendWebsocketSyncRequest("roomId/" + this.roomId + "/requesting-sync", videoPlayerAction);
      Logger.warn("Sending Sync: state ->" + state  + " | sentPlaybackRate: " + playbackRate);
    }
  }

  /**
   * responding to sync request by joining user
   */
  respondToJoinSyncRequest() {
    if (this.selectedPlayer) {
      let videoPlayerAction: VideoPlayerAction = VideoPlayerFactory.buildVideoPlayerAction(this.currentVideoPlayerSettings.state, this.selectedPlayer.getCurrentTime(), this.selectedPlayer.getCurrentPlaybackRate(), this.localUser, this.selectedPlayerId, this.currentVideoPlayerSettings);
      this.websocket.sendWebsocketSyncRequest("roomId/" + this.roomId + "/responding-to-join-sync-request", videoPlayerAction);
      Logger.success("responding to sync request by joining user")
    }
  }

  setApisInitalToNotReady(apis: any[]) {
    apis.forEach(api => this.playersLoaded.set(api.id, false));
  }

  /**
   * initialization functions
   */
  initVideoPlayers() {
    this.players = new Map<string, IVideoPlayerApi>();
    let that = this;
    //Setze alle Apis auf unintialisiert
    let waitForConfig = setInterval(function () {
      if (that.configService.CONFIG) {
        that.setApisInitalToNotReady(that.configService.CONFIG.apis);
        VideoPlayerFactory.buildVideoPlayers(that.configService.CONFIG.apis, that);
        clearInterval(waitForConfig);
        Logger.success('clearInterval: waitForConfig -> building players')
      }
    }, 2)
  }

  /**
   * if there are other users in the room request additional sync data from them
   */
  requestInitialSyncIfPossible() {
    let that = this;
    let waitForPlayers = setInterval(function () {
      if (that.selectedPlayer) {
        if (that.store.sortedUsers.length > 1) {
          Logger.success("REQUEST ADDITIONAL SYNC DATA AFTER JOINING");
          let initialSyncVideoPlayerAction = VideoPlayerFactory.buildInitialSyncVideoPlayerAction(that.localUser);
          that.websocket.sendWebsocketSyncRequest("roomId/" + that.roomId + "/requesting-sync", initialSyncVideoPlayerAction);
        }
        clearInterval(waitForPlayers)
      }
    }, 1);
  }
}

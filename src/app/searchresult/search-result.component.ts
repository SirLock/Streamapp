import { Component, OnInit } from '@angular/core';
import { DataApiManagerService } from '../services/data-api-manager.service';
import { SearchResult } from '../models/searchresult.model';
import { StoreService } from '../services/store.service';
import { AppManagerService } from '../services/appmanager.service';
import { PlaylistService } from '../playlist.service';
import { Constants } from '../helper/constants';
import { Util } from '../helper/util';
import { Logger } from '../helper/logger';
import { IDataApi } from '../models/idataapi';
import { Video } from '../models/video.model';
import { NextPageTokenObject } from '../models/nextpagetokenobject.model';
import {CustomAnimations} from "../customanimations";
import {ResponsiveService} from "../responsive.service";

@Component({
  selector: 'app-searchresult',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: [CustomAnimations.opOut]
})
export class SearchResultComponent implements OnInit {

  searchResultEleHeight:number;

  get dragDelay(): number {
    return this.responsiveService.dragDelay;
  }

  get screenMode(): string {
    return this.responsiveService.resolvedScreenMode;
  }

  get searchResults(): SearchResult[] {
    return this.dataApiManager.searchResults;
  }


  get displaySearchResultOverlay(): boolean {
    return this.appManager.displaySearchResultOverlay;
  }

  set currentDisplayPlaylistName(value: string) {
    this.appManager.currentDisplayPlaylistName = value;
  }

  constructor(private responsiveService: ResponsiveService,
              private appManager: AppManagerService,
              private store: StoreService,
              private dataApiManager: DataApiManagerService) {

  }

  isFirefox: boolean = false;
  ngOnInit() {
    this.isFirefox = window.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

  }

  triggerAppendToPlaylist(searchResult: SearchResult) {
    this.appManager.playlistService.sendPlaylistCommand(Constants.PL_CMD_APPEND, Util.mapToVideo(searchResult), -1);
  }

  triggerStartImmediately(searchResult: SearchResult) {
    this.scrollBackToVideoContainer();
    this.appManager.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_IMMEDIATELY, Util.mapToVideo(searchResult), -1);
  }

  triggerImportPlaylistFromPreview(searchResult: SearchResult) {
    this.appManager.importPlaylistFromPreview(searchResult);
  }

  triggerDisplayPlaylist(searchResult: SearchResult) {
    this.dataApiManager.clearSearchResults();
    this.currentDisplayPlaylistName = searchResult.title;
    let neededDataApi: IDataApi = this.dataApiManager.dataApis.get(searchResult.api);
    neededDataApi.sendPlaylistRequestToBackend(searchResult.playlistId)
      .subscribe(neededDataApi.handleResponse.bind(neededDataApi))
  }

  scrollBackToVideoContainer() {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  startVideo(searchResult: SearchResult) {
    if (searchResult.playlistId) {
      this.triggerDisplayPlaylist(searchResult);
    } else {
      this.triggerStartImmediately(searchResult);
    }
  }

}

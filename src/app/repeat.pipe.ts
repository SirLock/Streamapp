import { Pipe, PipeTransform } from '@angular/core';
import { Constants } from './helper/constants';

@Pipe({
  name: 'repeatControlImg',
  pure: false
})
export class RepeatPipe implements PipeTransform {


  transform(repeat: string): string {

    let iconUrl: string = "assets/playlisticons/repeatno.png";

    if (repeat === Constants.PL_CMD_REPEAT_ALL) {
      iconUrl = "assets/playlisticons/repeat.svg"
    }
    if (repeat === Constants.PL_CMD_REPEAT_ONE) {
      iconUrl = "assets/playlisticons/repeat1.png"
    }
    return iconUrl;
  }

}

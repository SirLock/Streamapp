import { Injectable } from '@angular/core';
import { RestService } from './services/rest.service';
import { Language } from './models/language.model';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  languageTexts: Map<string, any>;
  selectedLanguageKey: string = 'english';

  get selectedLanguageText(): any {
    return this.languageTexts.get(this.selectedLanguageKey);
  }

  get selectedLanguageSelect(): Language {
    if(this.selectedLanguageKey) {
      return this.LANGUAGES.filter(lang => this.selectedLanguageKey === lang.id)[0];
    }
    return this.LANGUAGES[0];
  }

  private LANGUAGES: Language[] = [new Language('english', 'en'), new Language('german', 'de')]

  get Languages() : Language[] {
    return this.LANGUAGES;
  }


  init() {
    this.languageTexts = new Map<string, any>();
    for (let language of this.Languages) {
      this.restService.fromFile("assets/languagetext/" + language.id + ".json")
        .subscribe(jsonObject => this.languageTexts.set(language.id, jsonObject));
    }

  }

  keyToValue(textKey: string) {
    return this.selectedLanguageText && this.selectedLanguageText[textKey] ? this.selectedLanguageText[textKey] : textKey;
  }

  constructor(private restService: RestService) {
    this.init();
  }

}

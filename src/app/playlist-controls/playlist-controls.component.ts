import { Component, OnInit } from '@angular/core';
import {Constants} from "../helper/constants";
import {Video} from "../models/video.model";
import {PlaylistState} from "../models/playliststate.model";
import {StoreService} from "../services/store.service";
import {AppManagerService} from "../services/appmanager.service";
import {PlaylistService} from "../playlist.service";

@Component({
  selector: 'app-playlist-controls',
  templateUrl: './playlist-controls.component.html',
  styleUrls: ['./playlist-controls.component.scss']
})
export class PlaylistControlsComponent implements OnInit {

  tooltip: boolean = false;

  get playlist(): Video[] {
    return this.store.playlist;
  }
  set playlist(val: Video[]) {
    this.store.playlist = val;
  }

  get playlistState(): PlaylistState {
    return this.playlistService.playlistState;
  }

  constructor(private store: StoreService, private appManager: AppManagerService, private playlistService: PlaylistService) { }

  ngOnInit(): void {
  }

  triggerTogglePlaylistRepetition() {
    if (this.playlistState.repeat === Constants.PL_CMD_NO_REPEAT) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_REPEAT_ALL, null, -1);
      return;
    }

    if (this.playlistState.repeat === Constants.PL_CMD_REPEAT_ALL) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_REPEAT_ONE, null, -1);
      return;
    }

    if (this.playlistState.repeat === Constants.PL_CMD_REPEAT_ONE) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_NO_REPEAT, null, -1);
      return;
    }
  }

  triggerClearPlaylist() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_CLEAR_LIST, null, -1);
  }

  triggerStartNextVideo() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_NEXT, null, -1);
  }

  triggerStartPreviousVideo() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_PREVIOUS, null, -1);
  }

  triggerTogglePlaylistOrder() {
    if (this.playlistState.order === Constants.PL_CMD_SEQUENCE_ORDER) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_RANDOM_ORDER, null, -1);
    } else {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_SEQUENCE_ORDER, null, -1);
    }
  }

  agreePlaylistClearing() {
    this.triggerClearPlaylist();
    this.tooltip = false;
  }

  disagreePlaylistClearing() {
    this.tooltip = false;
  }

  toggleTooltip() {
    if (this.playlist.length > 0)
      this.tooltip = !this.tooltip;
  }


  clickOutsideToolTip(event) {
    if (event.target.id !== "delete-button-id") {
      this.tooltip = false;
    }
  }

}

import {Injectable} from '@angular/core';
import {LanguageService} from "./language.service";
import {ChatMessage} from "./models/chatmessage.model";

@Injectable({
  providedIn: 'root'
})
export class TextgeneratorService {

  welcomeKey: string = "welcome_text_";
  welcomeKey2: string = "welcome_phrase_";
  welcomeTextsAmount: number =  25;
  welcomeTextsPhraseAmount: number = 15;
  constructor(private languageService: LanguageService) {

  }

  welcomeLocalUserChatMessage(name: string): ChatMessage {
    const randomWelcomeTextIndex = this.randomWelcomeIndex(this.welcomeTextsAmount);
    const randomWelcomePhraseIndex = this.randomWelcomeIndex(this.welcomeTextsPhraseAmount);
    const welcomeTextKey = this.welcomeKey + randomWelcomeTextIndex;
    const welcomePhraseKey = this.welcomeKey2 + randomWelcomePhraseIndex;
    const chatMessage: ChatMessage = new ChatMessage();
    chatMessage.from = 'system';
    let welcomeText: string = this.languageService.keyToValue(welcomeTextKey);
    let welcomePhrase: string = this.languageService.keyToValue(welcomePhraseKey)
    welcomePhrase = welcomePhrase.replace('$name',name);
    chatMessage.body = `<i>${welcomeText}</i><br> ${welcomePhrase}</br>`;
    chatMessage.createdAt = new Date().toString();
    return chatMessage;
  }

  userJoinedChatMessage(name: string): ChatMessage {
    const chatMessage: ChatMessage = new ChatMessage();
    chatMessage.from = 'system';
    const body = this.languageService.keyToValue('joined_user_message');
    chatMessage.body = `${name}${body}`;
    chatMessage.createdAt = Date.now().toString();
    return chatMessage;
  }


  private randomWelcomeIndex(amount : number): number {
    return Math.max(1, Math.round(Math.random() * amount));
  }
}

import {VideoService} from '../services/video.service';

export interface IVideoPlayerApi {


  id: string;
  name: string;
  script: string;
  iframe: HTMLElement;
  player: any;
  videoService: VideoService;

  setVideoService(videoService: VideoService);

  init();

  setVisible(value: boolean);

  isHidden(): boolean;

  playVideo();

  pauseVideo(): void;

  stopVideo(): void;

  seekTo(seconds: number, allowSeekAhead?: boolean);

  setVolume(value: number);

  isMuted(): boolean;

  getVideoDuration(): number;

  getCurrentTime(): number;

  getPlayerState(): number

  getAvailableQualityLevels(): string[];

  getPlaybackQuality(): string;

  getAvailablePlaybackRates(): Array<number>;

  setCurrentPlaybackRate(rate: number);

  getCurrentPlaybackRate(): number;

  loadVideoById(videoId: string, timestamp?: number);

  cueVideoById(videoId: string, timestamp?: number);

  enablePiP(option?: string);

  disablePiP(option?: string);

  getVideoData(): any

  toFullScreen();
}

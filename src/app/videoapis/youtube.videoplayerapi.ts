import {IVideoPlayerApi} from './ivideoplayerapi';
import {VideoService} from '../services/video.service';
import {Logger} from '../helper/logger';
import {StoreService} from '../services/store.service';

export class YoutubeVideoPlayer implements IVideoPlayerApi {


  id: string;
  name: string;
  script: string;
  iframe: HTMLElement;
  player: any;
  videoService: VideoService;

  setVideoService(videoService: VideoService) {
    this.videoService = videoService;
  }

  init() {
    this.initScript();
    this.initPlayer();
  }

  setVisible(value: boolean) {
    this.iframe.hidden = !value;
  }

  isHidden(): boolean {
    return this.iframe.hidden;
  }

  playVideo() {
    this.player.playVideo();
  }

  pauseVideo(): void {
    this.player.pauseVideo();
  }

  stopVideo(): void {
    this.player.stopVideo();
  }

  seekTo(seconds: number, allowSeekAhead?: Boolean) {
    this.player.seekTo(seconds, true)
  }

  setVolume(value: number) {
    throw new Error("Method not implemented.");
  }

  isMuted(): boolean {
    return this.player.isMuted()
  }

  getVideoDuration(): number {
    return this.player.getDuration();
  }

  getCurrentTime(): number {
    return this.player.getCurrentTime();
  }

  getPlayerState(): number {
    return this.player.getPlayerState();
  }

  getAvailableQualityLevels(): string[] {
    throw new Error("Method not implemented.");
  }

  getPlaybackQuality(): string {
    throw new Error("Method not implemented.");
  }

  getAvailablePlaybackRates(): number[] {
    throw new Error("Method not implemented.");
  }

  setCurrentPlaybackRate(rate: number) {
    this.player.setPlaybackRate(rate);
  }

  getCurrentPlaybackRate(): number {
    return this.player.getPlaybackRate();
  }

  loadVideoById(videoId: string, timestamp?: number) {
    this.player.loadVideoById({
      videoId: videoId,
      startSeconds: timestamp
    });
  }

  cueVideoById(videoId: string, timestamp: number) {
    this.player.cueVideoById({
      videoId: videoId,
      startSeconds: timestamp
    })
  }


  constructor(id: string, name: string, script: string) {
    this.id = id;
    this.name = name;
    this.script = script;
  }

  initScript() {
    let tag = document.createElement("script");
    tag.src = this.script;
    let firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  initPlayer() {
    let that = this;
    let videoPlayer: any;
    let YT: any;
    window["onYouTubeIframeAPIReady"] = e => {
      YT = window["YT"];
      videoPlayer = new window["YT"].Player(that.id + 'player', {
        width: '100%',
        height: 700,
        videoId: '',
        wmode: 'transparent',
        host: 'https://www.youtube-nocookie.com',
        playerVars: {
          autoplay: 0,
          controls: 1,
          rel: 0,
          fs: 1
        },
        events: {
          'onStateChange': that.delegateHandlePlayerEvent,
          'onPlaybackRateChange': that.delegateHandlePlaybackRateChange,
          'onReady': e => {
            that.iframe = e.target.f;
            that.player = videoPlayer;
            that.onVideoPlayerReady(e);
          },
          'onError': that.delegateHandlePlayerError
        }
      });
    };
  }


  delegateHandlePlayerError = (event) => {
    this.videoService.handlePlayerError(event);
  };

  delegateHandlePlayerEvent = (event) => {
    this.videoService.handlePlayerEvent(event.data);
  };

  delegateHandlePlaybackRateChange = (event) => {
    this.videoService.handlePlaybackRateChange(event.data);
  };

  onVideoPlayerReady(event) {
    this.setVisible(false);
    this.videoService.isReady(this.id);
    this.player.mute();
  }

  enablePiP(option?: string) {
    option = option || '';
    if (this.iframe) {
      this.iframe.className = 'video-iframe ' + option;
    }
  }

  disablePiP(option?: string) {
    option = option || '';
    if (this.iframe) {
      this.iframe.className = 'video-iframe ' + option;
    }
  }

  getVideoData() :any {
    return this.player.getVideoData();
  }

  toFullScreen() {
    this.iframe.requestFullscreen();
  }


}

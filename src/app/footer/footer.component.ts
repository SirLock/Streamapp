import { Component, OnInit } from '@angular/core';
import { AppManagerService } from '../services/appmanager.service';
import { StoreService } from '../services/store.service';
import {OverlayManagerService} from "../overlay-manager.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private overlayManager: OverlayManagerService,
              private appManager: AppManagerService,
              private store: StoreService) { }

  ngOnInit() {  }

  isDsgvoDisplayed() {
    return this.overlayManager.isDsgvoDisplayed();
  }

  isPhilosophyDisplayed() {
    return this.overlayManager.isPhilosophyDisplayed();
  }

  isCookieDetailsDisplayed() {
    return this.overlayManager.isCookieDetailsDisplayed();
  }

  isImpressumDisplayed() {
    return this.overlayManager.isImpressumDisplayed();
  }

  isTermsOfUseDisplayed() {
    return this.overlayManager.isTermsOfUseDisplayed();
  }

  isCookiePolicyDisplayed() {
    return this.overlayManager.isCookiePolicyDisplayed();
  }

  closeAdditionalInfo() {
    this.overlayManager.closeCurrentOverlay();
  }

  openDsgvo(){
    this.overlayManager.openDsgvo();
  }
  openCookieDetails(){
    this.overlayManager.openCookieDetails();
  }
  openImpressum(){
    this.overlayManager.openImpressum();
  }
  openTermsOfUse(){
    this.overlayManager.openTermsOfUse();
  }
  openPhilosophy(){
    this.overlayManager.openPhilosophy();
  }
  openCookiePolicy(){
    this.overlayManager.openCookiePolicy();
  }

}

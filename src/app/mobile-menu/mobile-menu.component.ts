import { Component, OnInit } from '@angular/core';
import { AppManagerService } from "../services/appmanager.service";
import { Theme } from '../models/theme.model';
import { ClipboardService } from 'ngx-clipboard';
import { Constants } from '../helper/constants';
import { Language } from '../models/language.model';
import { LanguageService } from '../language.service';
import { CookieManagerService } from '../services/cookiemanager.service';
@Component({
  selector: 'app-mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.scss'],
})
export class MobileMenuComponent implements OnInit {


  themeColorPickerOpen = false;

  constructor(private languageService: LanguageService,
    private appManager: AppManagerService,
    private clipboardService: ClipboardService,
    private cookieManager: CookieManagerService
    ) { }

  ngOnInit(): void {
        this.setInitalCreateNewRoomLink();
  }

  get selectedThemeColor() {
    return this.currentTheme ? this.currentTheme.color : '';
  }

  private themeColors_: string[] = [Constants.GREEN_THEME, Constants.BLUE_THEME, Constants.RED_THEME];

  get themeColors(): string[] {
    return this.themeColors_;
  }

  get languages(): Language[] {
    return this.languageService.Languages.filter(lang => lang.id !== this.selectedLanguageKey);
  }

  get filteredThemeColors() {
    return this.themeColors.filter(color => color !== this.selectedThemeColor);
  }

  get currentTheme(): Theme {
    return this.appManager.currentTheme;
  }

  languagePickerOpen = false;

  get selectedLanguageKey() {
    return this.languageService.selectedLanguageKey;
  }

  set selectedLanguageKey(val: string) {
    this.languageService.selectedLanguageKey = val;
  }

  get selectedLanguageSelect(): Language {
    return this.languageService.selectedLanguageSelect;
  }

  closeSideNav() {
    this.appManager.closeSideNav()
  }

  newRoomLink: string = "localhost:4200";

  setInitalCreateNewRoomLink() {
    const end: number = document.URL.lastIndexOf("/");
    this.newRoomLink = document.URL.substring(0, end);
  }

  setAsSelectedLanguage(language: Language) {
    this.selectedLanguageKey = language.id;
    this.cookieManager.updateCookie(this.currentTheme);
  }

  copyUrlToClipboard() {
    this.clipboardService.copyFromContent(document.URL);
  }

  createNewRoom() {
    this.appManager.s2gComponent.disconnect();
    window.location.href = this.newRoomLink;
  }


  toggleLanguagePicker() {
    this.languagePickerOpen = !this.languagePickerOpen;
  }

  clickOutsideLanguagePicker(event) {
    this.languagePickerOpen = false;
  }

  resolveDayMode(): string {
    return this.currentTheme.mode + '-mode';
  }

  toggleDayMode() {
    if (this.currentTheme.mode === Constants.LIGHT_MODE) {
      this.currentTheme.mode = Constants.DARK_MODE;
      this.cookieManager.updateCookie(this.currentTheme);
      return;
    }

    if (this.currentTheme.mode === Constants.DARK_MODE) {
      this.currentTheme.mode = Constants.LIGHT_MODE;
      this.cookieManager.updateCookie(this.currentTheme);
      return;
    }
  }

  toggleThemeColorPicker() {
    this.themeColorPickerOpen = !this.themeColorPickerOpen;
  }

  setAsSelectedThemeColor(color: string) {
    this.currentTheme.color = color;
    this.cookieManager.updateCookie(this.currentTheme);
  }

  clickOutsideThemeColorPicker(event) {
    this.themeColorPickerOpen = false;
  }

}

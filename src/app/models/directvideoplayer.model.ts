import { IVideoPlayerApi } from '../videoapis/ivideoplayerapi';
import { VideoService } from '../services/video.service';
import { Logger } from '../helper/logger';
import { Constants } from '../helper/constants';

export class DirectVideoPlayer implements IVideoPlayerApi {


    id: string;
    name: string;
    script: string;
    iframe: HTMLVideoElement;
    player: any;
    videoService: VideoService;


    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    addEventListeners(videoPlayer: HTMLVideoElement) {
        videoPlayer.addEventListener("ratechange", this.delegateHandlePlaybackRateChange);

        videoPlayer.addEventListener("error", this.delegateHandlePlayerError);

        videoPlayer.addEventListener("loadeddata", function () {
            Logger.success("LOADEDDATA")
        });

        videoPlayer.addEventListener("play", this.delegateHandlePlayerEvent);

        videoPlayer.addEventListener("pause", this.delegateHandlePlayerEvent);

        videoPlayer.addEventListener("ended", this.delegateHandlePlayerEvent);

        videoPlayer.addEventListener('contextmenu', event => {
          //event.preventDefault();
      })


    }

    setVideoService(videoService: VideoService) {
        this.videoService = videoService;
    }
    init() {
        this.iframe = <HTMLVideoElement>document.getElementById(this.id + "player");
        this.player = this.iframe;
        this.player.muted = true;
        this.addEventListeners(this.player);

    }
    setVisible(value: boolean) {
        this.iframe.hidden = !value;
    }
    isHidden(): boolean {
        throw new Error("Method not implemented.");
    }
    playVideo() {
        this.player.play();
    }
    pauseVideo(): void {
        this.player.pause();
    }
    stopVideo(): void {
        throw new Error("Method not implemented.");
    }
    seekTo(seconds: number, allowSeekAhead?: boolean) {
        this.player.currentTime = seconds;
    }
    setVolume(value: number) {
        throw new Error("Method not implemented.");
    }
    isMuted(): boolean {
        return this.player.muted;
    }
    getVideoDuration(): number {
        return this.player.duration;
    }
    getCurrentTime(): number {
        return this.player.currentTime;
    }
    getPlayerState(): number {
        return (this.player.ended) ? Constants.FINISHED : ((this.player.paused) ? Constants.PAUSED : Constants.PLAYING)
    }
    getAvailableQualityLevels(): string[] {
        return this.player.qualities;
    }
    getPlaybackQuality(): string {
        throw new Error("Method not implemented.");
    }
    getAvailablePlaybackRates(): number[] {
        return [2, 1.75, 1.5, 1.25, 1, 0.75, 0.5, 0.25];
    }
    setCurrentPlaybackRate(rate: number) {
        this.player.playbackRate = rate;
    }
    getCurrentPlaybackRate(): number {
        return this.player.playbackRate;
    }
    loadVideoById(videoId: string, timestamp?: number) {
        this.iframe.src = videoId;
        if (timestamp && timestamp > 0) {
            this.seekTo(timestamp);
        }
    }
    cueVideoById(videoId: string) {
        this.loadVideoById(videoId);
    }

    delegateHandlePlayerError = (event) => {
        this.videoService.handlePlayerError(event);
    };

    delegateHandlePlayerEvent = (event) => {
        let state: string = event.type;
        let convertedState: number;
        if (state == "play") {
            convertedState = Constants.PLAYING;
        } else if (state == "pause") {
            convertedState = Constants.PAUSED;
        } else if (state == "ended") {
            convertedState = Constants.FINISHED;
        } else {
            convertedState = Constants.BUFFERING;
        }

        this.videoService.handlePlayerEvent(convertedState);
    };

    delegateHandlePlaybackRateChange = (event) => {
        const rate = this.getCurrentPlaybackRate();
        this.videoService.handlePlaybackRateChange(rate);
    };

    onVideoPlayerReady(event) {
        this.setVisible(false);
        this.videoService.isReady(this.id);
        this.player.mute();
    }

    enablePiP(option?: string) {
      option = option || '';
      if (this.iframe) {
            this.iframe.className ='video-iframe ' + option;
        }
    }

    disablePiP(option?: string) {
      option = option || '';
      if (this.iframe) {
            this.iframe.className = 'video-iframe ' + option;
        }
    }

  getVideoData() :any {
    return this.player.getVideoData();
  }

  toFullScreen() {
    this.iframe.requestFullscreen();
  }


}

import {VideoPlayerAction} from './videoplayeraction.model';
import {VideoPlayerSettings} from './videoplayersettings.model';
import {Constants} from '../helper/constants';
import {User} from './user.model';
import {Util} from '../helper/util';
import {VideoService} from '../services/video.service';
import {YoutubeVideoPlayer} from '../videoapis/youtube.videoplayerapi';
import {DirectVideoPlayer} from './directvideoplayer.model';

export class VideoPlayerFactory {

  /**
   * builds all video players from config data
   *
   * @param apis
   */
  public static buildVideoPlayers(apis: any[], videoService: VideoService) {
    let videoContainer: HTMLElement = document.getElementById('videocontainer');
    apis.forEach(api => {
      switch (api.id) {
        case 'youtube': {
          Util.insertHTMLElement(videoContainer, 'div', api.id + 'player', 'video-iframe')
          let youtubeplayer = new YoutubeVideoPlayer(api.id, api.name, api.script);
          youtubeplayer.setVideoService(videoService);
          youtubeplayer.init();
          videoService.players.set(api.id, youtubeplayer);
        }
          break;
        case Constants.DIRECT_VIDEO_ID: {
          let videoElem: HTMLVideoElement = <HTMLVideoElement>Util.insertHTMLElement(videoContainer, 'video', api.id + 'player', 'video-iframe');
          videoElem.controls = true;
          let directVideoPlayer = new DirectVideoPlayer(api.id, api.name);
          directVideoPlayer.setVideoService(videoService);
          directVideoPlayer.init();
          videoElem.muted = true;
          videoService.players.set(api.id, directVideoPlayer);
          videoService.isReady(api.id);
          directVideoPlayer.setVisible(false);
        }
          break;
      }
    })
  }

  /**
   * builds the videoplayeraction needed for the sync when joining the room
   */
  public static buildInitialSyncVideoPlayerAction(localUser: User) {
    let videoPlayerAction: VideoPlayerAction = new VideoPlayerAction();
    videoPlayerAction.from = localUser.id;
    let videoPlayerSettings = new VideoPlayerSettings();
    videoPlayerSettings.state = Constants.REQUEST_SYNC;
    videoPlayerAction.videoPlayerSettings = videoPlayerSettings;
    return videoPlayerAction;
  }


  /**
   *  build videoplayeraction
   */
  public static buildVideoPlayerAction(playerState: number, timestamp: number, playbackRate: number, localUser: User, selectedPlayerId: string, currentVideoPlayerSettings: VideoPlayerSettings) {
    let videoPlayerAction: VideoPlayerAction = new VideoPlayerAction();
    videoPlayerAction.from = localUser.id;
    let videoPlayerSettings = new VideoPlayerSettings();
    videoPlayerSettings.state = playerState;
    videoPlayerSettings.timestamp = timestamp;
    videoPlayerSettings.playbackRate = playbackRate;
    videoPlayerSettings.api = selectedPlayerId;
    videoPlayerSettings.video = currentVideoPlayerSettings.video;
    videoPlayerSettings.usePlaylist = currentVideoPlayerSettings.usePlaylist;
    videoPlayerAction.videoPlayerSettings = videoPlayerSettings;
    return videoPlayerAction;
  }

}

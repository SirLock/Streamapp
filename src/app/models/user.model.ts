import { connect } from 'net';

export class User {

    id: string;
    name: string;
    iconUrl: string;
    onlineSince: string;
    designated: boolean;
    schemaString: string;

}

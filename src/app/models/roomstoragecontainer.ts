import {ChatMessage} from './chatmessage.model';

export class RoomStorageContainer {
  roomId: string;
  chatMessages: ChatMessage[];
  history: any;
  searchResultsHistory: string;

}

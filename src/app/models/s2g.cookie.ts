import { Theme } from './theme.model';
import { User } from './user.model';

export class Cookie{
    user: User;
    theme: Theme;
    langKey: string;
}

export class Language {


    id: string;
    short: string;

    constructor(id: string, short: string) {
        this.id = id;
        this.short = short;
    }



}
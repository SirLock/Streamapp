export class ChatMessage {
    id: string;
    from: string;
    body: string;
    createdAt: string;
}
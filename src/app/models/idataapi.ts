import { Observable } from 'rxjs';
import { SearchResult } from './searchresult.model';

export interface IDataApi {
    id: string;
    name: string;
    iconUrl: string;
    additionalFileTypeInfo:string;
    //apiKey: string;
    //searchEndPointBaseUrl: string;
    //videoEndPointBaseUrl: string;
    //playlistEndPointBaseUrl: string;

    handleResponse(response: any, searchInput?: string);
    sendRequest(requestParams: Map<string, string>) : Observable<any>;
    //sendSearchRequest(query: string, nextPageToken?: string): Observable<any>;
    mapToSearchResult(origin: any): SearchResult;
    mapRequestedPlaylistItemToSearchResult(origin: any): SearchResult
    //sendPlaylistRequest(playlistId: string, nextPageToken?: string): Observable<any>;

    sendPlaylistRequestToBackend(playlistId: string, nextPageToken?: string): Observable<any>;
    sendSearchRequestToBackend(query: string, nextPageToken?: string): Observable<any>;
    sendSingleVideoRequestToBackend(videoId: string): Observable<any>;
    supportedOnly(response): boolean;
    handleVideoLinkFallback(videoId: string): SearchResult[];
    handleResponseFallback(searchResults: SearchResult[], searchInput: string);


}

export class Theme {

    mode: string;
    color: string;

    constructor(mode?: string, color?: string) {
        this.mode = mode;
        this.color = color;
    }

    copy() {
        let theme = new Theme();
        theme.color = this.color;
        theme.mode = this.mode;
        return theme;
    }
}
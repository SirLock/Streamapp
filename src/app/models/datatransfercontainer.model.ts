import { User } from './user.model';
import { ChatMessage } from './chatmessage.model';

export class DataTransferContainer {
    from: string;
    purpose: number;
    user: User;
    users: User[];
    roomId: string;
    chatMessage: ChatMessage;
    fallbackMode: boolean;
}

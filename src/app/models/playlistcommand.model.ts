import { Video } from './video.model';
import { PlaylistState } from './playliststate.model';

export class PlaylistCommand {

    from: string;
    action: string;
    video: Video;
    playlist: Video[];
    index: number;
    previousIndex: number;
    fallbackMode: boolean;
}

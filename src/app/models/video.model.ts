export class Video {

    api: string;
    id: string;
    videoId: string;
    title: string;
    iconUrl:string;
    createdAt:string;
    channelTitle:string;
}

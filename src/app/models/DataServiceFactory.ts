import { IDataApi } from './idataapi';
import { YouTubeDataApi } from './youtubedataapi';
import { DirectVideoDataApi } from './directvideodataapi.model';
import { Logger } from '../helper/logger';
import { Constants } from '../helper/constants';
import { DataApiManagerService } from '../services/data-api-manager.service';

export class DataServiceFactory {

    public static buildDataApis(apis: any[], dataApiManager: DataApiManagerService) {
        let dataApis = new Map<string, IDataApi>();
        for (let api of apis) {
          switch (api.id) {
            case 'youtube':
              let ytDataApi = new YouTubeDataApi(api);
              ytDataApi.setDataService(dataApiManager);
              dataApis.set(api.id, ytDataApi);
              break;

            case Constants.DIRECT_VIDEO_ID:
              let directVideoDataApi = new DirectVideoDataApi(api);
              directVideoDataApi.setDataService(dataApiManager);
              dataApis.set(api.id, directVideoDataApi);
              break;

            default:
              break;
          }
        }
        Logger.success("BUILD DATA APIs: " + dataApis.size);
        dataApiManager.dataApis = dataApis;
        dataApiManager.selectedDataApiId = Constants.YOUTUBE_ID;
      }


}

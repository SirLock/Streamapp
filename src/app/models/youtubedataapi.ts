import {IDataApi} from './idataapi';
import {RestService} from '../services/rest.service';
import {Constants} from '../helper/constants';
import {Observable} from 'rxjs';
import {DataApiManagerService} from '../services/data-api-manager.service';
import {SearchResult} from './searchresult.model';
import {Data} from '@angular/router';
import {Util} from '../helper/util';
import {Logger} from '../helper/logger';
import {NextPageTokenObject} from './nextpagetokenobject.model';
import {RequestParam} from './requestparam.model';
import {AppManagerService} from '../services/appmanager.service';

export class YouTubeDataApi implements IDataApi {


  YOUTUBE_BACKEND_URL_PREFIX: string = '/data-service/api/';

  dataApiManager: DataApiManagerService;
  id: string;
  name: string;
  iconUrl: string;
  additionalFileTypeInfo: string = '';

  get restService(): RestService {
    return this.dataApiManager.RestService;
  }

  get urlPrefixWithAuthentication(): string {
    return 'roomId/'
      + this.dataApiManager.roomId + '/userId/'
      + this.dataApiManager.localUser.id
      + this.YOUTUBE_BACKEND_URL_PREFIX
      + this.id + '/type/';
  }

  constructor(api: any) {
    this.name = api.name;
    this.id = api.id;
    this.iconUrl = api.iconUrl;
  }

  setDataService(dataSerivce: DataApiManagerService) {
    this.dataApiManager = dataSerivce;
  }

  private clearInputIfLink(searchInput: string): any {
    searchInput = Util.removeURLPrefixes(searchInput);
    if (searchInput.startsWith('youtu.be/')) {
      return {searchInput: searchInput.substr(9), idOnly: true};
    } else if (searchInput.startsWith('youtube.com/watch?')) {
      return {searchInput: searchInput.substr(18), idOnly: false};
    }
    return null;
  }

  private getParametersIfValidYouTubeLink(clearedInputObj: any): Map<string, string> {
    let apiParameters = new Map<string, string>();

    if (clearedInputObj.idOnly) {
      apiParameters.set(Constants.YOUTUBE_VIDEO_ID_KEY, clearedInputObj.searchInput);
      return apiParameters;
    }

    let rawParameters = clearedInputObj.searchInput.split('&');
    for (let i = 0; i < rawParameters.length; i++) {
      let keyValue = rawParameters[i].split('=');
      apiParameters.set(keyValue[0], keyValue[1]);

    }
    if (apiParameters.has(Constants.YOUTUBE_PLAYLIST_KEY) || apiParameters.has(Constants.YOUTUBE_VIDEO_ID_KEY)) {
      return apiParameters;
    }
    return null;
  }

  public getValidYouTubeParams(searchInput: string): Map<string, string> {
    let clearedInput = this.clearInputIfLink(searchInput);
    if (clearedInput) {
      return this.getParametersIfValidYouTubeLink(clearedInput);
    }
    return null;
  }

  public sendRequest(apiParameters: Map<string, string>): Observable<any> {
    if (apiParameters.get(Constants.YOUTUBE_PLAYLIST_KEY)) {
      return this.sendPlaylistRequestToBackend(apiParameters.get(Constants.YOUTUBE_PLAYLIST_KEY));
    } else {
      return this.sendSingleVideoRequestToBackend(apiParameters.get(Constants.YOUTUBE_VIDEO_ID_KEY));
    }
  }

  public sendSearchRequestToBackend(query: string, nextPageToken?: string): Observable<any> {
    let requestParams: RequestParam[] = [];
    Util.addParam(requestParams, 'q', query);
    Util.addNexPageTokenParamIfPresent(requestParams, nextPageToken);
    return this.restService.POST(this.urlPrefixWithAuthentication + Constants.REQUEST_TYPE_SEARCH, requestParams);
  }


  public sendPlaylistRequestToBackend(playlistId: string, nextPageToken?: string): Observable<any> {
    let requestParams: RequestParam[] = [];
    Util.addParam(requestParams, 'playlistId', playlistId);
    Util.addNexPageTokenParamIfPresent(requestParams, nextPageToken);
    return this.restService.POST(this.urlPrefixWithAuthentication + Constants.REQUEST_TYPE_PLAYLIST, requestParams);
  }

  public sendSingleVideoRequestToBackend(videoId: string): Observable<any> {
    let requestParams: RequestParam[] = [];
    Util.addParam(requestParams, 'id', videoId);
    return this.restService.POST(this.urlPrefixWithAuthentication + Constants.REQUEST_TYPE_VIDEO, requestParams);
  }

  public handleResponse(response: any, searchInput: string) {
    if(!response) {
      return;
    }

    if (response.kind === 'youtube#playlistItemListResponse') {
      this.dataApiManager.displayPlaylistImportOptions = true;
      this.dataApiManager.nextPageTokenObj = null;
      if (response.nextPageToken) {
        this.sendPlaylistRequestToBackend(response.items[0].snippet.playlistId, response.nextPageToken)
          .subscribe(this.handleResponse.bind(this));
      }
      this.dataApiManager.searchResults = this.updatedImportedPlaylist(response.items);
    } else {
      this.dataApiManager.displayPlaylistImportOptions = false;
      this.dataApiManager.displaySearchResultOverlay = false;
      const updatedSearchResults = this.updatedSearchResults(response.items);
      this.dataApiManager.searchResults = updatedSearchResults;
      this.dataApiManager.addSearchResultsToHistory(updatedSearchResults, searchInput);
      this.dataApiManager.nextPageTokenObj = null;
    }
  }

  updatedImportedPlaylist(items: any) {
    return [
      ...this.dataApiManager.searchResults, ...items
        .map(item => this.mapRequestedPlaylistItemToSearchResult(item))]
  }

  updatedSearchResults(items: any): SearchResult[] {
    return [
      ...this.dataApiManager.searchResults, ...items
      .filter(response => this.supportedOnly(response))
      .map(item => this.mapToSearchResult(item))]
  }

  mapToSearchResult(origin: any): SearchResult {
    let searchResult = new SearchResult();
    searchResult.api = this.id;
    if ('string' == typeof origin.id) {
      searchResult.videoId = origin.id;
    } else if (origin.id.playlistId) {
      searchResult.playlistId = origin.id.playlistId;
    } else if (origin.id.videoId) {
      searchResult.videoId = origin.id.videoId;
    }

    searchResult.title = origin.snippet.title;
    searchResult.iconUrl = origin.snippet.thumbnails.high.url || origin.snippet.thumbnails.default.url;
    searchResult.channelTitle = origin.snippet.channelTitle;
    searchResult.createdAt = origin.snippet.publishedAt;
    return searchResult;
  }

  mapRequestedPlaylistItemToSearchResult(origin: any): SearchResult {
    let searchResult = new SearchResult();
    searchResult.api = this.id;
    searchResult.videoId = origin.snippet.resourceId.videoId;
    searchResult.title = origin.snippet.title;
    searchResult.iconUrl = origin.snippet.thumbnails ? origin.snippet.thumbnails.high.url || origin.snippet.thumbnails.default.url : 'assets/directvid.png';
    searchResult.channelTitle = origin.snippet.channelTitle;
    searchResult.createdAt = origin.snippet.publishedAt;
    return searchResult;
  }


  supportedOnly(response): boolean {
    return 'string' === typeof response.id || response.id.videoId || response.id.playlistId;
  }

  handleVideoLinkFallback(videoId: string): SearchResult[] {
    const searchResult: SearchResult = new SearchResult();
    searchResult.api = this.id;
    searchResult.iconUrl = 'http://i.ytimg.com/vi/' + videoId + '/sddefault.jpg';
    searchResult.videoId = videoId;
    searchResult.title = videoId;
    return [searchResult];
  }

  handleResponseFallback(searchResults, searchInput) {
    this.dataApiManager.searchResults = searchResults;
  }


}

export class NextPageTokenObject {

    nextPageToken: string;
    apiId: string;
}
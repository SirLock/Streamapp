import { Video } from './video.model';

export class VideoPlayerSettings {
    api: string;
    playbackRate: number;
    state: number;
    timestamp: number;
    video: Video;
    usePlaylist: boolean;
}

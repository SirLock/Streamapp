import { RGB } from './rgb.model';

export class Cell {
    color: RGB;
    x: number;
    y: number;
    size: number;
    leftClicked: boolean;

    clone(): Cell {
        let clone = new Cell();
        clone.color = this.color;
        clone.x = this.x;
        clone.y = this.y;
        clone.size = this.size;
        clone.leftClicked = this.leftClicked;
        return clone;
    }
}
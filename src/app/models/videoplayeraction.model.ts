import {VideoPlayerSettings} from './videoplayersettings.model';

export class VideoPlayerAction {
  from: string;
  videoPlayerSettings: VideoPlayerSettings;
  additionalAction: number;
  fallbackMode: boolean;

}

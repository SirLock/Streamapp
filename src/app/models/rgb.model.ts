export class RGB {

    r: number;
    g: number;
    b: number;

    constructor(r: number, g: number, b: number) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    equals(other: RGB): boolean {
        return other && this.r === other.r && this.g === other.g && this.b === other.b;
    }

    get htmlColor(): string {
        return 'rgb(' + this.r + ',' + this.g + ',' + this.b + ')';
    }

    get stripped(): string {
        return this.r + ',' + this.g + ',' + this.b;
    }

}
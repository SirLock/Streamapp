import {IDataApi} from './idataapi';
import {Observable} from 'rxjs';
import {DataApiManagerService} from '../services/data-api-manager.service';
import {SearchResult} from './searchresult.model';
import {Util} from '../helper/util';

export class DirectVideoDataApi implements IDataApi {

  id: string;
  name: string;
  iconUrl: string;
  apiKey: string;
  searchEndPointBaseUrl: string;
  videoEndPointBaseUrl: string;
  playlistEndPointBaseUrl: string;
  dataApiManager: DataApiManagerService;
  additionalFileTypeInfo: string = '.mp4, .webm, .flv';

  constructor(api: DirectVideoDataApi) {
    this.apiKey = api.apiKey;
    this.name = api.name;
    this.id = api.id;
    this.iconUrl = api.iconUrl;
  }

  setDataService(dataSerivce: DataApiManagerService) {
    this.dataApiManager = dataSerivce;
  }


  handleResponse(results, searchInput: string) {
    this.dataApiManager.searchResults = results;
  }

  sendSearchRequestToBackend(query: string, nextPageToken?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  sendRequest(requestParams: Map<string, string>): Observable<any> {
    throw new Error('Method not implemented.');
  }

  sendSearchRequest(query: string, nextPageToken?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  mapToSearchResult(origin: any): SearchResult {
    throw new Error('Method not implemented.');
  }

  mapRequestedPlaylistItemToSearchResult(origin: any): SearchResult {
    throw new Error('Method not implemented.');
  }

  sendPlaylistRequest(playlistId: string, nextPageToken?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  sendPlaylistRequestToBackend(playlistId: string, nextPageToken?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  sendSingleVideoRequestToBackend(videoId: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  supportedOnly(response): boolean {
    return true;
  }

  handleVideoLinkFallback(videoId: string): SearchResult[] {
    let searchResult: SearchResult = new SearchResult();
    if (Util.checkForUrlPrefixes(videoId)) {
      if (videoId.endsWith('.webm') || videoId.endsWith('.mp4') || videoId.endsWith('.flv')) {
        searchResult.api = this.id;
        searchResult.iconUrl = 'assets/video-player.svg';
        searchResult.videoId = videoId;
        let splitted = videoId.split('/');
        searchResult.title = splitted[splitted.length - 1];
        return [searchResult];
      }
    }
    return null;
  }

  handleResponseFallback(searchResults, searchInput) {
    this.dataApiManager.searchResults = searchResults;
  }


}

import {Component, OnInit, isDevMode} from '@angular/core';
import {FormControl} from '@angular/forms';
import {DataApiManagerService} from '../services/data-api-manager.service';
import {StoreService} from '../services/store.service';
import {User} from '../models/user.model';
import {IDataApi} from '../models/idataapi';
import {ClipboardService} from 'ngx-clipboard';
import {ActivatedRoute, Router} from '@angular/router';
import {AppManagerService} from '../services/appmanager.service';
import {LanguageService} from '../language.service';
import {Language} from '../models/language.model';
import {Constants} from '../helper/constants';
import {Theme} from '../models/theme.model';
import {CookieManagerService} from '../services/cookiemanager.service';
import {gsap, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin, DrawSVGPlugin, TimelineLite} from 'gsap/all';
import {WebStorageService} from '../web-storage.service';

gsap.registerPlugin(MorphSVGPlugin, MotionPathPlugin, DrawSVGPlugin);

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  displaySearchInput: boolean = true;
  searchInput = new FormControl();
  themeColorPickerOpen = false;

  tlCopy = gsap.timeline({paused: true});
  tl2 = gsap.timeline({paused: true, defaults: {duration: 0.2, ease: 'bounce.out'}});

  ngOnInit() {

    MorphSVGPlugin.convertToPath('line');
    this.tlCopy.fromTo('#path1', 0.4, {drawSVG: '0% 100%'}, {drawSVG: '100% 100%', opacity: 0.5}, 0)
      .to('#path1', 0, {drawSVG: '0% 0%', rotation: 40, transformOrigin: '50% 50%', opacity: 1}, 0.4)
      .to('#path1', 0.3, {drawSVG: '0% 100%'}, 0.4)
      .to('#path1', 0.4, {rotation: 0, ease: 'bounce.out'}, 0.4);
    this.tlCopy.fromTo('#path2', 0.4, {drawSVG: '0% 100%'}, {drawSVG: '100% 100%', opacity: 0.5}, 0)
      .to('#path2', 0, {drawSVG: '0% 0%', rotation: -40, transformOrigin: '50% 50%', opacity: 1}, 0.4)
      .to('#path2', 0.3, {drawSVG: '0% 100%'}, 0.4)
      .to('#path2', 0.4, {rotation: 0, ease: 'bounce.out'}, 0.4);

    this.tlCopy.fromTo('.dot', 0.1, {scale: 0, opacity: 0, transformOrigin: '50% 50%'}, {scale: 1, opacity: 1}, 0.5)
      .to('.dot', 0.5, {scale: 0, opacity: 0}, 0.7);
  }

  tabTogglePlaylist() {
    this.tl2.play(0);
  }

  get selectedThemeColor() {
    return this.currentTheme ? this.currentTheme.color : '';
  }

  private themeColors_: string[] = [Constants.GREEN_THEME, Constants.BLUE_THEME, Constants.RED_THEME];

  get themeColors(): string[] {
    return this.themeColors_;
  }

  get filteredThemeColors() {
    return this.themeColors.filter(color => color !== this.selectedThemeColor);
  }

  get currentTheme(): Theme {
    return this.appManager.currentTheme;
  }

  get topbarDocked(): boolean {
    return this.appManager.topbarDocked;
  }

  get languages(): Language[] {
    return this.languageService.Languages.filter(lang => lang.id !== this.selectedLanguageKey);
  }

  get apis(): any[] {
    return this.dataApiManager.dataApis ? [...this.dataApiManager.dataApis.values()] : [];
  }

  get apiSelection(): any[] {
    return this.apis.filter(api => api.id !== this.selectedDataApiId);
  }

  get localUser(): User {
    return this.store.localUser;
  }

  set localUser(val: User) {
    this.store.localUser = val;
  }

  get selectedDataApiId(): string {
    return this.dataApiManager.selectedDataApiId;
  }

  set selectedDataApiId(val: string) {
    this.dataApiManager.selectedDataApiId = val;
  }

  get selectedDataApi(): IDataApi {
    return this.dataApiManager.selectedDataApi;
  }

  // private getRoomUrl(): string {
  //   let pathId = this.store.roomId;
  //     return 'localhost:4200/' + pathId;
  //   return Constants.FRONTEND_URL_POSTFIX + pathId;
  // }

  constructor(private languageService: LanguageService,
              private appManager: AppManagerService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private dataApiManager: DataApiManagerService,
              private store: StoreService,
              private clipboardService: ClipboardService,
              private cookieManager: CookieManagerService,
              private localStorage: WebStorageService
  ) {
    this.initKeyBoardHandling();
    this.setInitalCreateNewRoomLink();
  }

  initKeyBoardHandling() {
    this.dataApiManager.handleKeyboardInput(this.prepareKeyboardInput());
  }

  setInitalCreateNewRoomLink() {
    const end: number = document.URL.lastIndexOf('/');
    this.newRoomLink = document.URL.substring(0, end);
  }

  newRoomLink: string = 'localhost:4200';

  prepareKeyboardInput = () => {
    return this.searchInput.valueChanges;
  };

  toSelectedDataApi(api) {
    this.selectedDataApiId = api.id;
  }

  copyUrlToClipboard() {
    this.clipboardService.copyFromContent(document.URL);
    this.tlCopy.play(0);
  }

  createNewRoom() {
    window.location.href = this.newRoomLink;
  }

  languagePickerOpen = false;

  get selectedLanguageKey() {
    return this.languageService.selectedLanguageKey;
  }

  set selectedLanguageKey(val: string) {
    this.languageService.selectedLanguageKey = val;
  }

  get selectedLanguageSelect(): Language {
    return this.languageService.selectedLanguageSelect;
  }

  toggleLanguagePicker() {
    this.languagePickerOpen = !this.languagePickerOpen;
  }

  clickOutsideLanguagePicker(event) {
    this.languagePickerOpen = false;
  }

  setAsSelectedLanguage(language: Language) {
    this.selectedLanguageKey = language.id;
    this.cookieManager.updateCookie(this.currentTheme);
  }

  setAsSelectedThemeColor(color: string) {
    this.currentTheme.color = color;
    this.cookieManager.updateCookie(this.currentTheme);
  }

  toggleThemeColorPicker() {
    this.themeColorPickerOpen = !this.themeColorPickerOpen;
  }

  resolveDayMode(): string {
    return this.currentTheme.mode + '-mode';
  }

  toggleDayMode() {
    if (this.currentTheme.mode === Constants.LIGHT_MODE) {
      this.currentTheme.mode = Constants.DARK_MODE;
      this.cookieManager.updateCookie(this.currentTheme);
      return;
    }

    if (this.currentTheme.mode === Constants.DARK_MODE) {
      this.currentTheme.mode = Constants.LIGHT_MODE;
      this.cookieManager.updateCookie(this.currentTheme);
      return;
    }
  }

  clickOutsideThemeColorPicker(event) {
    this.themeColorPickerOpen = false;
  }

  inFocus: boolean = false;

  focusFunction() {
    this.inFocus = true;
  }

  focusOutFunction() {
    this.inFocus = false;
  }

}

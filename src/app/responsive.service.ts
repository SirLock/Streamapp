import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {Observable} from "rxjs";

@Injectable()
export class ResponsiveService {

  public _mobile: string = "mobile";
  public _medium: string = "tablet";
  public _widescreen: string = "desktop";


  private _resolvedScreenMode: string = 'desktop';

  get resolvedScreenMode(): string {
    return this._resolvedScreenMode;
  }

  set resolvedScreenMode(val: string) {
    this._resolvedScreenMode = val;
  }

  private _screenMode = new Subject();
  public screenWidth: number;


  constructor() {
  }

  onScreenModeChange(status: string) {
    this._screenMode.next(status);
  }

  getMobileStatus(): Observable<any> {
    return this._screenMode.asObservable();
  }

  public checkWidth() {
    const width = window.innerWidth;
    if (width <= 768) {
      this.screenWidth = width
      this.onScreenModeChange(this._mobile);
    } else if (width > 768 && width <= 1440) {
      this.screenWidth = width;
      this.onScreenModeChange(this._medium);
    } else {
      this.screenWidth = width;
      this.onScreenModeChange(this._widescreen);
    }
  }

  isMobile() : boolean {
    return this.resolvedScreenMode === this._mobile;
  }

  get dragDelay(): number {
    return this.isMobile() ? 200 : 0;
  }
}

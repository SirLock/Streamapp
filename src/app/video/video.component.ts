import { Component, OnInit } from '@angular/core';
import { VideoService } from '../services/video.service';
import { AppManagerService } from '../services/appmanager.service';
import { StoreService } from '../services/store.service';
import { Constants } from '../helper/constants';
import { Logger } from '../helper/logger';
import { Video } from '../models/video.model';
import { CustomAnimations } from "../customanimations";
import { PlaylistService } from "../playlist.service";
import { ResponsiveService } from '../responsive.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  animations: [
    CustomAnimations.opOutMedium
  ]
})
export class VideoComponent implements OnInit {

  constructor(private appManager: AppManagerService, private responsiveService: ResponsiveService, private videoService: VideoService, private store: StoreService, private playlistService: PlaylistService) { }

  get pipActive(): boolean {
    return this.appManager.pipActive;
  }

  get nextVideo(): Video {
    return this.videoService.nextVideo;
  }

  get displayNextVideoPreview(): boolean {
    return this.appManager.displayNextVideoPreview;
  }

  get chatToggle(): boolean {
    return this.appManager.chatToggle;
  }

  get leftTime(): number {
    return this.appManager.leftTime;
  }

  get initLeftTime(): number {
    return this.appManager.initLeftTime;
  }

  private screenMode_: string;

  get screenMode(): string {
    return this.responsiveService.resolvedScreenMode;
  }



  triggerStartNextVideo() {
    this.appManager.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_NEXT, null, -1);
    this.appManager.displayNextVideoPreview = false;
  }

  ngOnInit() {
    this.videoService.initVideoPlayers();
  }

  scrollBackToVideoContainer() {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

}

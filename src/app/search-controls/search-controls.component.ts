import { Component, OnInit } from '@angular/core';
import {NextPageTokenObject} from "../models/nextpagetokenobject.model";
import {ResponsiveService} from "../responsive.service";
import {AppManagerService} from "../services/appmanager.service";
import {StoreService} from "../services/store.service";
import {DataApiManagerService} from "../services/data-api-manager.service";
import {Util} from "../helper/util";
import {SearchResult} from "../models/searchresult.model";
import {Constants} from '../helper/constants';

@Component({
  selector: 'app-search-controls',
  templateUrl: './search-controls.component.html',
  styleUrls: ['./search-controls.component.scss']
})
export class SearchControlsComponent implements OnInit {

  constructor(private responsiveService: ResponsiveService,
              private appManager: AppManagerService,
              private store: StoreService,
              private dataApiManager: DataApiManagerService) { }

  ngOnInit(): void {
  }

  get selectedDataApiId(): string {
    return this.dataApiManager.selectedDataApiId;
  }

  set selectedDataApiId(val: string) {
    this.dataApiManager.selectedDataApiId = val;
  }

  get currentDisplayPlaylistName(): string {
    return this.appManager.currentDisplayPlaylistName;
  }

  set currentDisplayPlaylistName(value: string) {
    this.appManager.currentDisplayPlaylistName = value;
  }

  get displayPlaylistImportOptions(): boolean {
    return this.dataApiManager.displayPlaylistImportOptions;
  }

  set displayPlaylistImportOptions(val: boolean) {
    this.dataApiManager.displayPlaylistImportOptions = val;
  }

  get nextPageTokenObj(): NextPageTokenObject {
    return this.dataApiManager.nextPageTokenObj;
  }

  get displaySearchResultOverlay(): boolean {
    return this.appManager.displaySearchResultOverlay;
  }

  get hasPreviousSearchResults() : boolean {
    return this.dataApiManager.hasPreviousSearchResults;
  }

  get hasNextSearchResults() : boolean {
    return this.dataApiManager.hasNextSearchResults;
  }

  get currentSearchInputResolved() : string {
    return this.currentSearchInput || this.store.searchResultsHistoryKeys[this.appManager.currentSearchResultsIndex];
  }

  get currentSearchInput(): string {
    return this.appManager.currentSearchInput;
  }

  set currentSearchInput(value: string) {
    this.appManager.currentSearchInput = value;
  }

  get searchResults(): SearchResult[] {
    return this.dataApiManager.searchResults;
  }

  get directVideoId(): string {
    return Constants.DIRECT_VIDEO_ID;
  }


  toPreviousSearchResults() {
    this.displayPlaylistImportOptions = false;
    this.currentSearchInput = null;
    this.dataApiManager.toPreviousSearchResults();
  }

  backToSearchResults() {
    this.displayPlaylistImportOptions = false;
    //this.dataApiManager.displayLatestSearchResults();
    this.dataApiManager.toLatestSearchResults();
  }

  toNextSearchResults() {
    this.dataApiManager.toNextSearchResults();
  }

  triggerImportPlaylist() {
    if (this.displayPlaylistImportOptions) {
      this.appManager.playlistService.sendImportPlaylistCommand(this.searchResults.map(searchRes => Util.mapToVideo(searchRes)));
    }
  }



}

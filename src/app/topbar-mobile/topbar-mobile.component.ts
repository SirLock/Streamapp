import { Component, OnInit } from '@angular/core';
import { AppManagerService } from "../services/appmanager.service";
import { StoreService } from "../services/store.service";
import { FormControl } from "@angular/forms";
import { DataApiManagerService } from "../services/data-api-manager.service";

@Component({
  selector: 'app-topbar-mobile',
  templateUrl: './topbar-mobile.component.html',
  styleUrls: ['./topbar-mobile.component.scss']
})
export class TopbarMobileComponent implements OnInit {

  constructor(private appManager: AppManagerService, private dataApiManager: DataApiManagerService, private store: StoreService) { }

  ngOnInit(): void {
  }

  openSideNav() {
    this.appManager.openSideNav();
  }


}

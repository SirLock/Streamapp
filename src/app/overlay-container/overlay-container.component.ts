import { Component, OnInit } from '@angular/core';
import {OverlayManagerService} from "../overlay-manager.service";
import {CookieManagerService} from "../services/cookiemanager.service";

@Component({
  selector: 'app-overlay-container',
  templateUrl: './overlay-container.component.html',
  styleUrls: ['./overlay-container.component.scss']
})
export class OverlayContainerComponent implements OnInit {

  constructor(private overlayManager: OverlayManagerService, private cookieManager: CookieManagerService) { }

  get dsgvoCookieConfirmed(): boolean {
    return this.cookieManager.dsgvoCookieConfirmed;
  }

  set overlayIndex(value: number) {
    this.overlayManager.overlayIndex = value;
  }

  get overlayIndex(): number {
    return this.overlayManager.overlayIndex;
  }

  ngOnInit(): void {
  }

  isDsgvoDisplayed() {
    return this.overlayManager.isDsgvoDisplayed();
  }

  isPhilosophyDisplayed() {
    return this.overlayManager.isPhilosophyDisplayed();
  }

  isCookieDetailsDisplayed() {
    return this.overlayManager.isCookieDetailsDisplayed();
  }

  isCookiePolicyDisplayed() {
    return this.overlayManager.isCookiePolicyDisplayed();
  }

  isImpressumDisplayed() {
    return this.overlayManager.isImpressumDisplayed();
  }

  isTermsOfUseDisplayed() {
    return this.overlayManager.isTermsOfUseDisplayed();
  }

  close() {
    this.overlayManager.closeCurrentOverlay();
  }

}

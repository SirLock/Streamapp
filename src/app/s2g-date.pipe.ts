import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 's2gDate'
})
export class S2gDatePipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {

  }

  transform(date: string): any {

    let now: Date =  new Date();
    let date_: Date = new Date(date);
    if (this.areDifferentDays(now, date_)) {
      return this.datePipe.transform(date, 'dd.MM.yyyy HH:mm');
    }
    return this.datePipe.transform(date, 'HH:mm');
  }

  private areDifferentDays(date1: Date, date2: Date): boolean {
    return date1.getDate() != date2.getDate();
  }

}

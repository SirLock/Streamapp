import {SearchResult} from '../models/searchresult.model';
import {Video} from '../models/video.model';
import {ChatMessage} from '../models/chatmessage.model';
import {VideoPlayerSettings} from '../models/videoplayersettings.model';
import {Theme} from '../models/theme.model';
import {Constants} from './constants';
import {RGB} from '../models/rgb.model';
import {RequestParam} from '../models/requestparam.model';
import {User} from "../models/user.model";
import * as UUID from 'uuid';

export class Util {


  public static loggingActive: boolean = true;


  static NOTSTARTED: number = -1;
  static FINISHED: number = 0;
  static PLAYING: number = 1;
  static PAUSED: number = 2;
  static BUFFERING: number = 3;
  static PLACED: number = 5;

  private static DEFAULT_THEME_: Theme = new Theme(Constants.DARK_MODE, Constants.GREEN_THEME);

  public static DEFAULT_THEME(): Theme {
    return this.DEFAULT_THEME_.copy();
  }

  public static toString(playerState: number): string {
    if (playerState === Util.NOTSTARTED)
      return 'not started'

    if (playerState === Util.FINISHED)
      return 'video finished'

    if (playerState === Util.PLAYING)
      return 'started playing'

    if (playerState === Util.PAUSED)
      return 'paused'

    if (playerState === Util.BUFFERING)
      return 'buffering video'
  }

  public static generateLeftRoomChatMessage(userName: string): ChatMessage {
    let chatMessage: ChatMessage = new ChatMessage();
    chatMessage.id = "SYSTEM_ID-" + Date.now().toString();
    chatMessage.createdAt = new Date(Date.now()).toString();
    chatMessage.body = userName + " has left the room";
    return chatMessage;
  }


  public static clone(videoPlayerSettings: VideoPlayerSettings): VideoPlayerSettings {
    let clonedVideoPlayerSettings = new VideoPlayerSettings();
    clonedVideoPlayerSettings.api = videoPlayerSettings.api;
    clonedVideoPlayerSettings.playbackRate = videoPlayerSettings.playbackRate;
    clonedVideoPlayerSettings.state = videoPlayerSettings.state;
    clonedVideoPlayerSettings.timestamp = videoPlayerSettings.timestamp;
    clonedVideoPlayerSettings.video = videoPlayerSettings.video;
    return clonedVideoPlayerSettings;
  }

  public static removeURLPrefixes(URL: String) {
    return URL.replace(/^(https?:\/\/)?(www\.)?/i, "")
  }

  public static checkForUrlPrefixes(URL: String) {
    return URL.match(/^(https?:\/\/)?(www\.)?/i);
  }

  public static getCookieDomain(url: string) {
    let domain = Util.removeURLPrefixes(url);
    const end: number = domain.indexOf("/");
    domain = domain.substring(0, end);
    if(domain.indexOf(':') > -1) {
      domain = domain.split(':')[0];
    }
    return domain;
  }

  public static insertHTMLElement(parent: HTMLElement, tag: string, id: string, className: string): HTMLElement {
    let htmlElem: HTMLElement = document.createElement(tag);
    htmlElem.id = id;
    htmlElem.className = className;
    parent.appendChild(htmlElem);
    return htmlElem;
  }

  public static mapToVideo(searchResult: SearchResult): Video {
    let video = new Video();
    video.api = searchResult.api;
    video.videoId = searchResult.videoId;
    video.title = searchResult.title;
    video.iconUrl = searchResult.iconUrl;
    video.createdAt = searchResult.createdAt;
    video.channelTitle = searchResult.channelTitle;
    return video;
  }

  public static toRGB(arr: string[]): RGB {
    return new RGB(parseInt(arr[0]), parseInt(arr[1]), parseInt(arr[2]));
  }

  public static addParam(params: RequestParam[], key: string, value: string) {
    let param = new RequestParam();
    param.key = key;
    param.value = value;
    params.push(param);
  }


  public static addNumberParam(params: RequestParam[], key: string, intValue: number) {
    let param = new RequestParam();
    param.key = key;
    param.intValue = intValue;
    params.push(param);
  }

  public static addNexPageTokenParamIfPresent(params: RequestParam[], nextPageToken: string) {
    if (nextPageToken) {
      this.addParam(params, "pageToken", nextPageToken)
    }
  }

  public static cloneUser(user: User): User {
    let clone_: User = new User();
    clone_.id = user.id;
    clone_.name = user.name;
    clone_.iconUrl = user.iconUrl;
    clone_.onlineSince = user.onlineSince;
    clone_.designated = user.designated;
    return clone_;
  }

  public static getDelimiterChatMessage(): ChatMessage {
    const borderMessage = new ChatMessage();
    borderMessage.id = UUID.v4();
    borderMessage.from = 'system';
    borderMessage.body = 'border';
    borderMessage.createdAt = Date.now().toString();
    return borderMessage;
  }
}

import { IndividualConfig } from 'ngx-toastr';

export class ToastrConfig  {
    disableTimeOut?: boolean | "timeOut" | "extendedTimeOut";    
    timeOut?: number;
    closeButton?: boolean;
    extendedTimeOut?: number;
    progressBar?: boolean;
    progressAnimation?: import("ngx-toastr").ProgressAnimationType;
    enableHtml?: boolean;
    toastClass?: string;
    positionClass?: string;
    titleClass?: string;
    messageClass?: string;
    easing?: string;
    easeTime?: string | number;
    tapToDismiss?: boolean;
    toastComponent?: import("ngx-toastr").ComponentType<any>;
    onActivateTick?: boolean;


}
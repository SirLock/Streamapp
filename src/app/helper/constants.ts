import {Injectable} from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
export class Constants {


  //COOKIE KEYS
  public static DSGVO_COOKIE_KEY: string = 'dsgvo_s9_curview';
  public static USER_COOKIE_KEY: string = 'user_settings_cookie';


  public static LOCALHOST_FRONTEND_POSTFIX: string = "/";

  public static CONFIG: any = null;
  public static configLoaded: boolean = false;

  //
  public static FRONTEND_URL_POSTFIX: string = "/";

  //GLOBAL LOGGING FLAG
  public static loggingActive: boolean = true;

  public static NEXT_VIDEO_TOASTR_TIME_THRESHOLD_MAX: number = 15; // in sec

  //FOOTER STATES
  public static IMPRESSUM: number = 1;
  public static CONTACT: number = 2;
  public static SAFETY_POLICY = 3;
  public static FOOTER_HIDDEN = 0;

  // DSGVO FLAGS
  public static OVERLAY_COOKIE_POLICY_INDEX = 0;
  public static OVERLAY_DSGVO_INDEX = 1;
  public static OVERLAY_COOKIE_DETAILS_INDEX = 2;
  public static OVERLAY_PHILOSOPHY_INDEX = 3;
  public static OVERLAY_TERMS_OF_USE_INDEX = 4;
  public static OVERLAY_IMPRESSUM_INDEX = 5;


  //PLAYLIST COMMANDS
  public static PL_CMD_APPEND: string = "append";
  public static PL_CMD_INSERT_AT: string = "insert-at";
  public static PL_CMD_DELETE: string = "delete";
  public static PL_CMD_START_IMMEDIATELY: string = "start-immediately";
  public static PL_CMD_START_NEXT: string = "start-next";
  public static PL_CMD_REPEAT_ALL: string = "repeat-all";
  public static PL_CMD_REPEAT_ONE: string = "repeat-one";
  public static PL_CMD_NO_REPEAT: string = "no-repeat";
  public static PL_CMD_SEQUENCE_ORDER: string = "sequence-order";
  public static PL_CMD_RANDOM_ORDER: string = "random-order";
  public static PL_CMD_IMPORT_PLAYLIST: string = "import-playlist";
  public static PL_CMD_CHANGE_POSITION: string = "change-position";
  public static PL_CMD_CLEAR_LIST: string = "clear-list";
  public static PL_CMD_START_CLICKED_VIDEO: string = "start-clicked-video";
  public static PL_CMD_START_PREVIOUS: string = "start-previous";

  //PURPOSES
  public static PURPOSE_DISCONNECTED_CLIENT: number = 501
  public static PURPOSE_SEND_CHATMESSAGE: number = 502;
  public static PURPOSE_USER_JOINED: number = 503;
  public static PURPOSE_ASSIGN_TO_DESIGNATED: number = 504;
  public static PURPOSE_UPDATE_USERNAME: number = 505;
  public static PURPOSE_UPDATE_USERICON: number = 506;

  //INTERVALS
  public static USER_COOKIE_REFRESH_DELAY: number = 7; //1 / 288;


  //URLS
  public static BACKEND_BASE_URL: string = "http://localhost:8080";


  public static WEBSOCKET_URL_POSTFIX: string = "/backend/ws/"
  public static REST_URL_POSTFIX: string = "/backend/rest/"
  public static REST_ENDPOINT_CREATE_ROOM = "create-room";
  public static REST_ENDPOINT_JOIN_ROOM = "join-room/";
  public static topBarDockingThreshold: number = 50;
  static MAX_DIRECT_VIDEO_QUERY_LENGTH: number = 30;

  static get WEBSOCKET_BASE_URL(): string {
    return this.BACKEND_BASE_URL + this.WEBSOCKET_URL_POSTFIX;
  }

  static get REST_BASE_URL(): string {
    return this.BACKEND_BASE_URL + this.REST_URL_POSTFIX;
  }

  //PLAYERSTATES
  public static NOTSTARTED: number = -1;
  public static FINISHED: number = 0;
  public static PLAYING: number = 1;
  public static PAUSED: number = 2;
  public static BUFFERING: number = 3;
  public static PLACED: number = 5;
  public static REQUEST_SYNC: number = 100;
  public static SYNC_REQUEST_RESPONSE: number = 200;


  // API IDs
  public static YOUTUBE_ID = 'youtube';
  public static NOAPI_ID = 'NOAPI_ID';
  public static DIRECT_VIDEO_ID = 'direct-video';

  // Request param keys
  public static YOUTUBE_PLAYLIST_KEY: string = 'list';
  public static YOUTUBE_VIDEO_ID_KEY: string = 'v';
  public static YOUTUBE_MAX_RESULTS: number = 50;
  public static MIN_QUERY_LENGTH = 3;
  public static MAX_QUERY_LENGTH = 250;

  public static SEARCH_RESULT_COUNT_LIMIT: number = 100;

  public static SEARCH_RESULTS_HISTORY_LIMIT: number = 10;

  public static LOCALSTORAGE_KEYS_LIMIT: number = 10;
  public static LOCALSTORAGE_KEY_PREFIX: string = 'localstorage_slot_';
  public static LOCALSTORAGE_ITEM_STRING_DELIMITER: string = '#';
  public static LOCALSTORAGE_ROOM_ID_INDEX = 0;
  public static LOCALSTORAGE_DATE_INDEX = 1;
  public static LOCALSTORAGE_ITEM_INDEX = 2;


  //Themes

  public static LIGHT_MODE = "light";
  public static DARK_MODE = "dark";

  public static GREEN_THEME = "green";
  public static BLUE_THEME = "blue";
  public static RED_THEME = "red";


  //ADDITIONAL VIDEO PLAYER ACTIONS
  public static ACTION_ADD_TO_HISTORY: number = 700;

  //ICON EDITOR

  public static TILE_SIZE: number = 5;
  public static ICON_SIZE: number = 50;
  public static DISPLAYED_EDITOR_TILE_SIZE = 20;

  //Icon Editor
  public static ROW_SEPARATOR: string = "#";
  public static CELL_SEPARATOR: string = ";";
  public static VALUE_SEPARATOR: string = ",";
  public static COLOR_SEPARATOR: string = "§";
  public static START_SEPARATOR: string = ">";

  public static TILE_ICON_CODE: number = 0;
  public static BG_ICON_CODE: number = 1;

  //DATA SERVICE
  public static REQUEST_TYPE_VIDEO: string = "ds-video";
  public static REQUEST_TYPE_PLAYLIST: string = "ds-playlist";
  public static REQUEST_TYPE_SEARCH: string = "ds-search";

}

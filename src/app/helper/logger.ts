import { ConfigService } from '../services/config.service';
import { Util } from './util';


export class Logger {

  constructor() { }

  public static log(value: any) {
    if (Util.loggingActive) {
      console.log(value)
    }
  }

  public static success(value: any) {
    if (Util.loggingActive) {
      console.log("%c"+value, 'color: #bada55')
    }
  }

  public static warn(value: any) {
    if (Util.loggingActive) {
      console.warn(value)
    }
  }

  public static error(value: any) {
    if (Util.loggingActive) {
      console.error(value)
    }
  }

  public static info(value: any) {
    if (Util.loggingActive) {
      console.info(value)
    }
  }


}

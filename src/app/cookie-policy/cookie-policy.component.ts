import {Component, OnInit} from '@angular/core';
import {AppManagerService} from "../services/appmanager.service";
import {CookieManagerService} from "../services/cookiemanager.service";
import {Router} from "@angular/router";
import {Constants} from "../helper/constants";
import {OverlayManagerService} from "../overlay-manager.service";

@Component({
  selector: 'app-cookie-policy',
  templateUrl: './cookie-policy.component.html',
  styleUrls: ['./cookie-policy.component.scss']
})
export class CookiePolicyComponent implements OnInit {

  newRoomLink: string;

  constructor(private appManager: AppManagerService,
              private cookieManager: CookieManagerService,
              private overlayManager: OverlayManagerService) {
    this.setInitalCreateNewRoomLink();
  }

  ngOnInit(): void {
  }

  get dsgvoCookieConfirmed(): boolean {
    return this.cookieManager.dsgvoCookieConfirmed;
  }

  confirmToPolicy() {
    this.cookieManager.setConfirmDSGVO();
    this.cookieManager.checkAndUpdateDsgvoCookieStatus();
    this.closeCookiePolicy();
    this.appManager.s2gComponent.startApp();
  }

  discardFromPolicy() {
    this.cookieManager.setDiscardDSGVO();
    this.cookieManager.deleteAll(this.newRoomLink)
    this.backToStartPage();
  }

  backToStartPage() {
    this.appManager.s2gComponent.disconnect();
    window.location.href = this.newRoomLink;
  }

  setInitalCreateNewRoomLink() {
    const end: number = document.URL.lastIndexOf("/");
    this.newRoomLink = document.URL.substring(0, end);
  }

  openDsgvo() {
    this.overlayManager.openDsgvo();
  }

  openCookieDetails() {
    this.overlayManager.openCookieDetails();
  }

  openImpressum() {
    this.overlayManager.openImpressum();
  }

  openTermsOfUse() {
    this.overlayManager.openTermsOfUse();
  }

  openPhilosophy() {
    this.overlayManager.openPhilosophy();
  }

  closeCookiePolicy() {
    this.overlayManager.closeCurrentOverlay()
  }
}

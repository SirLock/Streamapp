import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import { StoreService } from '../services/store.service';
import { AppManagerService } from '../services/appmanager.service';
import { Video } from '../models/video.model';
import { PlaylistService } from '../playlist.service';
import { SearchResult } from '../models/searchresult.model';
import { Constants } from '../helper/constants';
import { PlaylistState } from '../models/playliststate.model';
import { Logger } from '../helper/logger';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import {CustomAnimations} from "../customanimations";
import {ResponsiveService} from "../responsive.service";

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    CustomAnimations.opOut
  ]
})
export class PlaylistComponent implements OnInit {

  @Input() displayedPlaylist: Video[];

  get screenMode(): string {
    return this.responsiveService.resolvedScreenMode;
  }

  get dragDelay(): number {
    return this.responsiveService.dragDelay;
  }

  get playlist(): Video[] {
    return this.store.playlist;
  }
  set playlist(val: Video[]) {
    this.store.playlist = val;
  }

  dropSearchResult(event) {
    let originList: SearchResult[] = event.previousContainer.data;
    let targetList: Video[] = event.container.data;

    if(event.isPointerOverContainer === false) {
      let video: Video = this.playlist[event.currentIndex];
      if(video && originList === targetList) {
        this.triggerDeleteClickedVideo(video);
      }
      return;
    }

    let choosenItem = originList[event.previousIndex];
    if (choosenItem && choosenItem.playlistId) {
      this.appManager.importPlaylistFromPreview(choosenItem);
      return;
    }

    if (originList === targetList) {
      if (event.previousIndex != event.currentIndex) {
        let video = targetList[event.previousIndex];
        this.triggerChangeVideoPositionTo(event.currentIndex, event.previousIndex, video);
      }
    } else {

      let video = originList[event.previousIndex];
      this.triggerInsertVideoAt(video, event.currentIndex);
    }
  }

  isCurrentVideo(video: Video): boolean {
    return this.appManager.videoService.currentVideoPlayerSettings ? this.appManager.videoService.currentVideoPlayerSettings.video.id === video.id : false;
  }

  constructor(private appManager: AppManagerService,
              private store: StoreService,
              private playlistService: PlaylistService,
              private ref: ChangeDetectorRef,
              private responsiveService: ResponsiveService) { }

  ngOnInit() {
  }

  triggerChangeVideoPositionTo(index: number, previousIndex: number, video: Video) {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_CHANGE_POSITION, video, index, previousIndex);
  }

  triggerInsertVideoAt(video: Video, index:number) {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_INSERT_AT, video, index);
  }

  triggerStartClickedVideo(video: Video) {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_CLICKED_VIDEO, video, -1);
  }

  triggerDeleteClickedVideo(video: Video) {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_DELETE, video, -1);
  }

}

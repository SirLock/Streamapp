import { TestBed } from '@angular/core/testing';

import { S9toastrService } from './s9toastr.service';

describe('S9toastrService', () => {
  let service: S9toastrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(S9toastrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

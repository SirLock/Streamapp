import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import { StoreService } from '../services/store.service';
import { AppManagerService } from '../services/appmanager.service';
import { Video } from '../models/video.model';
import {Constants} from "../helper/constants";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryComponent implements OnInit {
  @Input() displayedHistory: Video[];

  constructor(private appManager: AppManagerService, private store: StoreService) { }

  ngOnInit() {
    console.log(this.displayedHistory)
  }


  triggerStartImmediately(video: Video) {
    this.scrollBackToVideoContainer();
    this.appManager.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_IMMEDIATELY, video, -1);
  }

  scrollBackToVideoContainer() {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import { Constants } from './helper/constants';

@Pipe({
  name: 'orderControlImg'
})
export class OrderPipe implements PipeTransform {

  transform(order: string): string {
    if(order === Constants.PL_CMD_SEQUENCE_ORDER) {
      return "assets/playlisticons/sequence_order.svg"
    }
    return "assets/playlisticons/random_order.svg"
  }

}

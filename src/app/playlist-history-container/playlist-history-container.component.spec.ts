import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistHistoryContainerComponent } from './playlist-history-container.component';

describe('PlaylistHistoryContainerComponent', () => {
  let component: PlaylistHistoryContainerComponent;
  let fixture: ComponentFixture<PlaylistHistoryContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistHistoryContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistHistoryContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

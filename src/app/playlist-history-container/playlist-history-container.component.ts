import { Component, OnInit } from '@angular/core';
import { AppManagerService } from '../services/appmanager.service';
import { StoreService } from '../services/store.service';
import { PlaylistService } from '../playlist.service';
import { Video } from '../models/video.model';
import { PlaylistState } from '../models/playliststate.model';
import { Constants } from '../helper/constants';
import { gsap, ScrollToPlugin, Draggable, MotionPathPlugin, MorphSVGPlugin, TimelineLite } from "gsap/all";
gsap.registerPlugin(MorphSVGPlugin, MotionPathPlugin);




@Component({
  selector: 'app-playlist-history-container',
  templateUrl: './playlist-history-container.component.html',
  styleUrls: ['./playlist-history-container.component.scss']
})
export class PlaylistHistoryContainerComponent implements OnInit {

  tooltip: boolean = false;
  tabToggle: boolean = true;

  constructor(private appManager: AppManagerService, private store: StoreService, private playlistService: PlaylistService) { }

  get playlist(): Video[] {
    return this.store.playlist;
  }
  set playlist(val: Video[]) {
    this.store.playlist = val;
  }

  get playlistState(): PlaylistState {
    return this.playlistService.playlistState;
  }

  get history(): Video[] {
    return this.store.history;
  }

  tl = gsap.timeline({ paused: true });
  tl2 = gsap.timeline({ paused: true, defaults: { duration: 0.2, ease: "bounce.out" } })

  ngOnInit() {

    this.tl.fromTo("#hours", 1, { rotation: 0, transformOrigin: '0% 100%' }, { rotation: 1440 }, 0)
      .fromTo("#minutes", 1, { rotation: 0 }, { rotation: 360 }, 0)
      .fromTo("#circle", 0.8, { rotation: 0, transformOrigin: '15px 11px' }, { rotation: -360 }, 0);


    this.tl2.to("#path11", { y: -4, ease: "power1.out" }, 0.1)
      .to("#path11", { y: 0, ease: "power1.out" }, 0.3)
      .to("#text", { y: -4, ease: "power1.out" }, 0.1)
      .to("#text", { y: 0, ease: "power1.out" }, 0.3)
      .to("#path12", { morphSVG: "#path22" }, 0.2)
      .to("#path12", { morphSVG: "#path12" }, 0.4)
      .to("#path13", { morphSVG: "#path23" }, 0.3)
      .to("#path13", { morphSVG: "#path13" }, 0.5);
  }

  tabTogglePlaylist() {
    this.tl2.play(0);
    this.tabToggle = true;
  }

  tabToggleHistory() {
    this.tl.play(0);
    this.tabToggle = false;
  }

  agreePlaylistClearing() {
    this.triggerClearPlaylist();
    this.tooltip = false;
  }

  disagreePlaylistClearing() {
    this.tooltip = false;
  }

  toggletooltip() {
    if (this.playlist.length > 0)
      this.tooltip = !this.tooltip;
  }

  clickOutsideToolTip(event) {
    if (event.target.id !== "delete-button-id") {
      this.tooltip = false;
    }
  }

  triggerTogglePlaylistRepetition() {
    if (this.playlistState.repeat === Constants.PL_CMD_NO_REPEAT) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_REPEAT_ALL, null, -1);
      return;
    }

    if (this.playlistState.repeat === Constants.PL_CMD_REPEAT_ALL) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_REPEAT_ONE, null, -1);
      return;
    }

    if (this.playlistState.repeat === Constants.PL_CMD_REPEAT_ONE) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_NO_REPEAT, null, -1);
      return;
    }
  }

  triggerClearPlaylist() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_CLEAR_LIST, null, -1);
  }

  triggerStartNextVideo() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_NEXT, null, -1);
  }

  triggerStartPreviousVideo() {
    this.playlistService.sendPlaylistCommand(Constants.PL_CMD_START_PREVIOUS, null, -1);
  }

  triggerTogglePlaylistOrder() {
    if (this.playlistState.order === Constants.PL_CMD_SEQUENCE_ORDER) {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_RANDOM_ORDER, null, -1);
    } else {
      this.playlistService.sendPlaylistCommand(Constants.PL_CMD_SEQUENCE_ORDER, null, -1);
    }
  }

}



import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ChatMessage} from "../models/chatmessage.model";
import {StoreService} from "../services/store.service";
import {AppManagerService} from "../services/appmanager.service";
import {User} from "../models/user.model";
import {DataTransferContainer} from "../models/datatransfercontainer.model";
import {WebsocketService} from "../services/websocket.service";
import {Video} from "../models/video.model";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent implements OnInit {

  @Input() displayedChatMessages: ChatMessage[];

  private chatMessageInputText_: string;

  get chatMessages(): ChatMessage[] {
    return this.store.chatMessages;
  }

  set chatMessages(val: ChatMessage[]) {
    this.store.chatMessages = val;
  }

  get localUser(): User {
    return this.store.localUser;
  }
  set localUser(val: User) {
    this.store.localUser = val;
  }

  get chatMessageInputText(): string {
    return this.chatMessageInputText_;
  }

  set chatMessageInputText(val: string) {
    this.chatMessageInputText_ = val;
  }

  constructor(private store: StoreService, private appManager: AppManagerService, private websocket: WebsocketService) { }

  ngOnInit(): void {
  }


  triggerSendChatMessage() {
    if (this.chatMessageInputText.length > 0) {
      let dtc: DataTransferContainer = new DataTransferContainer();
      let chatMessage: ChatMessage = new ChatMessage();
      chatMessage.body = this.chatMessageInputText;
      chatMessage.from = this.localUser.id;

      dtc.from = this.localUser.id;
      dtc.chatMessage = chatMessage;

      this.websocket.sendWebsocketRequest("roomId/" + this.store.roomId + "/chatmessage", dtc);
      this.chatMessageInputText = "";
    }
  }

  isBorderMessage(chatMessage: ChatMessage) {
    return chatMessage.from === 'system' && chatMessage.body === 'border';
  }

  isIncomingMessage(chatMessage: ChatMessage) {
    if(this.isSystemMessage(chatMessage)) {
      return false;
    }
    return chatMessage && chatMessage.from && chatMessage.from != this.localUser.id;
  }

  isOutgoingMessage(chatMessage: ChatMessage) {
    if(this.isSystemMessage(chatMessage)) {
      return false;
    }
    return chatMessage && chatMessage.from === this.localUser?.id;
  }

  isSystemMessage(chatMessage: ChatMessage) {
    return chatMessage?.from === 'system';
  }

  getChatMessageClass(chatMessage: ChatMessage) : string {
    if(this.isSystemMessage(chatMessage)) {
      return 'system';
    }

    if(this.isOutgoingMessage(chatMessage)) {
      return 'outgoing';
    }

    /*
    if(this.isIncomingMessage(chatMessage)) {
      return 'incoming';
    }*/

    return 'incoming';
  }


}
